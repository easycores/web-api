###
##### 업데이트 : 2022.05.01

---

# [ 현대카드 Universe WEB API 서버 ] 

#### - 기존 QuadMax 를 Spring Boot API 서버로 재구축 하기 위한 프로젝트.
#### - 프론트엔드 소스가 포함되지 않습니다.


---


### API 작업 가이드 

* intellij 프로그램으로 작업할 것을 권장합니다. 
	- eclipse 에서도 동작하는 것을 확인하였지만 현대카드에서는 intellij 로 해당 프로젝트를 진행하기 때문에 가능하면 동일한 환경에서 작업이 이루어지면 좋겠습니다.  
	  
* pom.xml 에 MAVEN DEPENDENCY 의 수정이 필요한 경우 현대카드에서의 해당 DEPENDENCY 사용가능 여부 확인 필요 ( 문의 : 배은식, 박지윤 )


---


### intelliJ 프로그램 사용시 설정 팁

* 파일 저장시 불필요한 import 문을 자동으로 제거
	- https://youtu.be/fi6KaYMp5ak?t=71
	
* show context action ( 관련된 소스를 자동 리팩토링 ) 
	- https://youtu.be/05TugQ7RMoM
	
* code usages 힌트
	- https://youtu.be/1hv8aQOD68M

---

### API 개발현황 (2022-06-28 업데이트)
[WC-0360] 진행중단
[WC-0370] 진행중단
[WC-0460] 정한별 완료
[WC-0470] 김성은 완료
[WC-0490] 김성은 완료
[WC-0370] 정한별 완료
[WC-0370] 정한별 완료



package com.ecs;

import com.ecs.domain.rest.test.controller.TestController;
import com.ecs.domain.rest.test.service.TestService;
import com.ecs.global.constant.Const;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.contains;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@WebMvcTest(TestController.class)
public class TestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TestService testService;

    @Test
    public void list_MVC_테스트() throws Exception {
        mockMvc.perform(
                 get(Const.REST_API_URL_ROOT + "/test/list")
                    .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("jsonView"))
            .andExpect(model().attributeExists("list"))
            .andDo(print());    // 출력
    }

    @Test
    public void list_page_MVC_테스트() throws Exception {
        mockMvc.perform(
                 get(Const.REST_API_URL_ROOT + "/test/list_page")
                    .contentType(MediaType.APPLICATION_JSON)
                    .param("page_size", "2")
                    .param("page_number", "1")
            )
            .andExpect(status().isOk())
            .andExpect(view().name("jsonView"))
            .andExpect(model().attributeExists("list"))
            .andExpect(model().attributeExists("page_number"))
            .andExpect(model().attributeExists("page_size"))
            .andExpect(model().attributeExists("total_page"))
            .andExpect(model().attributeExists("count"))
            .andDo(print());    // 출력
    }
}

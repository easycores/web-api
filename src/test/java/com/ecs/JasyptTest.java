package com.ecs;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.spring3.properties.EncryptablePropertyPlaceholderConfigurer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Ignore
public class JasyptTest {

	@Test
	public void 프로퍼티_암호화_테스트() {
		Security.addProvider(new BouncyCastleProvider());
		
		StandardPBEStringEncryptor jasypt = new StandardPBEStringEncryptor();
		jasypt.setPassword("SMART_QUADMAX");
		jasypt.setAlgorithm("PBEWITHSHA256AND256BITAES-CBC-BC");
		jasypt.setProviderName("BC");

		String encStr = jasypt.encrypt("biadm");
		String decStr = jasypt.decrypt(encStr);
		System.out.println("=====================================");
		System.out.println("enc : " + encStr);
		System.out.println("dec : " + decStr);
		System.out.println("=====================================");
	}
}
package com.ecs.infrastructure.dialect;

import java.util.List;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;

import com.github.miemiedev.mybatis.paginator.dialect.Dialect;
import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

/**
 * Created by kimsangyong on 2016. 5. 27..
 */
public class SQLServerDialect extends Dialect {
	public SQLServerDialect(MappedStatement mappedStatement, Object parameterObject, PageBounds pageBounds) {
		super(mappedStatement, parameterObject, pageBounds);
	}


	static int getAfterSelectInsertPoint(String sql) {
		int selectIndex = sql.toLowerCase().indexOf( "select" );
		final int selectDistinctIndex = sql.toLowerCase().indexOf( "select distinct" );
		return selectIndex + ( selectDistinctIndex == selectIndex ? 15 : 6 );
	}

	@Override
	protected String getLimitString(String sql, String offsetName,int offset, String limitName, int limit) {

		setPageParameter(limitName, limit, Integer.class);
		if (parameterMappings != null) {
			for (int i = parameterMappings.size() - 1; i >= 0; i--) {
				if ("__limit".equals(parameterMappings.get(i).getProperty())) {
					parameterMappings.remove(i);
				}
			}
		}


		StringBuilder retSqlSb = new StringBuilder();
		if (offset == 0) {
			retSqlSb.append( sql )
					.insert( getAfterSelectInsertPoint( sql ), " top " + limit );
		} else {
			retSqlSb.append("with t1 as (")
					.append(sql).insert( getAfterSelectInsertPoint( sql ), " top " + (offset + limit) )
					.append("), t2 as (")
					.append( sql ).insert( getAfterSelectInsertPoint( sql ), " top " + offset )
					.append(") select * from t1 except select * from t2");

			if(null != this.pageBounds) {
				List<Order> list = this.pageBounds.getOrders();
				if(null != list) {
					for(int i = 0; i < list.size(); i++) {
						Order order = list.get(i);
						if(i == 0) {
							retSqlSb.append(" ORDER BY ");
						} else {
							retSqlSb.append(" , ");
						}
						retSqlSb.append(order.toString());
					}
				}
			}

			if(null != parameterMappings && !parameterMappings.isEmpty()) {
				int iMapping = parameterMappings.size();

				for (int i = 0; i < iMapping; i++) {
					ParameterMapping parameterMapping = parameterMappings.get(i);
					ParameterMapping copyParameterMapping = new ParameterMapping.Builder(mappedStatement.getConfiguration(), parameterMapping.getProperty(), parameterMapping.getJavaType()).build();

					parameterMappings.add(copyParameterMapping);
				}
			}
		}
		return retSqlSb.toString();
	}
}

package com.ecs.infrastructure.dialect;

import com.github.miemiedev.mybatis.paginator.dialect.Dialect;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.mapping.MappedStatement;

/**
 * Created by kimsangyong on 2016. 6. 7..
 */
public class DB2Dialect extends Dialect {
	public DB2Dialect(MappedStatement mappedStatement, Object parameterObject, PageBounds pageBounds) {
		super(mappedStatement, parameterObject, pageBounds);
	}

	@Override
	protected String getLimitString(String sql, String offsetName,int offset, String limitName, int limit) {
		setPageParameter(limitName, limit, Integer.class);
		if (parameterMappings != null) {
			for (int i = parameterMappings.size() - 1; i >= 0; i--) {
				if ("__limit".equals(parameterMappings.get(i).getProperty())) {
					parameterMappings.remove(i);
				}
			}
		}

		StringBuilder retSqlSb = new StringBuilder();
		if (offset == 0) {
			retSqlSb.append( sql )
					.append( " FETCH FIRST ").append(limit).append(" ROWS ONLY ");
		} else {
			retSqlSb.append("with t1 as (")
					.append(sql).append( " FETCH FIRST " + (offset + limit) + " ROWS ONLY " )
					.append("), t2 as (")
					.append(sql).append( " FETCH FIRST " + offset + " ROWS ONLY " )
					.append(") select * from t1 except select * from t2");
		}
		return retSqlSb.toString();
	}
}

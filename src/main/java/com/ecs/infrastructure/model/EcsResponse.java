package com.ecs.infrastructure.model;

import com.ecs.global.util.PagingUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author bes
 */
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL) // 필드값이 NULL 인 경우 Response 결과에서 제외
public class EcsResponse<T>{

    // 데이터
    private T data;
    // 상태
    private HttpStatus status;
    // 에러 메세지
    private EcsResponseError error;
    /**
     * 페이징 처리 필드
     * JsonInclude.Include.NON_NULL 처리가 가능한 Integer 로 구성
     * ( NON_DEFAULT 사용하여 row 갯수 0 인 데이터 페이징 처리시 문제소지 )
     */
    private Integer page_number;
    private Integer page_size;
    private Integer total_page;
    private Integer count;

    // HTTP STATUS (HEADER) 리턴
    public EcsResponse(HttpStatus status) {
        this.status = status;
    }

    // 데이터, HTTP STATUS (HEADER) 리턴
    public EcsResponse(T data, HttpStatus status) {
        this.data = data;
        this.status = status;
    }

    // 에러메세지, HTTP STATUS (HEADER) 리턴
    public EcsResponse(Exception error, HttpStatus status) {
        this.error = new EcsResponseError(error.getMessage());
        this.status = status;
    }

    // 데이터, 페이징 처리 필드, HTTP STATUS (HEADER) 리턴(페이징 유틸로 페이징 처리 필드 생성)
    public EcsResponse(T data, PageList pageList, HttpStatus status) {
        this.data = data;
        if (pageList != null && pageList.getPaginator() != null) {
            this.page_number = Integer.valueOf(pageList.getPaginator().getPage());
            this.page_size = Integer.valueOf(pageList.getPaginator().getLimit());
            this.total_page = Integer.valueOf(pageList.getPaginator().getTotalPages());
            this.count = Integer.valueOf(pageList.getPaginator().getTotalCount());
        }
        this.status = status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    // EcsResponse 를 오류 응답용 객체로 사용시 필드로 사용
    @Getter
    @AllArgsConstructor
    public class EcsResponseError {
        private String message;
    }

}

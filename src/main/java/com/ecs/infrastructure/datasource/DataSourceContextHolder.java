package com.ecs.infrastructure.datasource;

import org.springframework.util.Assert;

public class DataSourceContextHolder {
	
	private DataSourceContextHolder() {
	    throw new IllegalStateException("Utility class");
	}

	private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();
	
	public static void setDbmsId(String dbmsId) {
		String id = dbmsId == null ? "" : dbmsId;
		Assert.notNull(id, "dbmsId cannot be null");
		CONTEXT_HOLDER.set(id);
	}

	public static String getDbmsId() {
		return CONTEXT_HOLDER.get();
	}

	public static void clearDbmsId() {
		CONTEXT_HOLDER.remove();
	}

}

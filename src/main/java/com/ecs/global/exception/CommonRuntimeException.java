package com.ecs.global.exception;

/**
 * CommonRuntimeException
 *
 */
public class CommonRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 */
	public CommonRuntimeException() {
		super();
	}

	/**
	 * Constructor
	 * @param message String
	 * @param cause String
	 * @param enableSuppression boolean
	 * @param writableStackTrace boolean
	 */
	public CommonRuntimeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructor
	 * @param message String
	 * @param cause Throwable
	 */
	public CommonRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor
	 * @param message String
	 */
	public CommonRuntimeException(String message) {
		super(message);
	}

	/**
	 * Constructor
	 * @param cause Throwable
	 */
	public CommonRuntimeException(Throwable cause) {
		super(cause);
	}
	
	
}

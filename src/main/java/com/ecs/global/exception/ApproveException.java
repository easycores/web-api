package com.ecs.global.exception;

public class ApproveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7118713061365718420L;
	
	public ApproveException(String message) {
		super(message);
	}
}

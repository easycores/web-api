package com.ecs.global.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class QuadMaxException extends BaseException {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QuadMaxException.class);

	private static final long serialVersionUID = -6489240352111650188L;
	
	public QuadMaxException(String message) {
		super(message);
	}
	
	public QuadMaxException(MessageSource messageSource, String messageKey) {
		this(messageSource, messageKey, null, null, LocaleContextHolder.getLocale(), null);
	}
	
	public QuadMaxException(MessageSource messageSource, String messageKey, Object[] messageParameters) {
		this(messageSource, messageKey, messageParameters, null, LocaleContextHolder.getLocale(), null);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Exception wrappedException) {
		this(messageSource, messageKey, null, null, LocaleContextHolder.getLocale(), wrappedException);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Locale locale, Exception wrappedException) {
		this(messageSource, messageKey, null, null, locale, wrappedException);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Object[] messageParameters, Locale locale,
	        Exception wrappedException) {
		this(messageSource, messageKey, messageParameters, null, locale, wrappedException);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Object[] messageParameters,
	        Exception wrappedException) {
		this(messageSource, messageKey, messageParameters, null, LocaleContextHolder.getLocale(), wrappedException);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Object[] messageParameters,
	        String defaultMessage, Exception wrappedException) {
		this(messageSource, messageKey, messageParameters, defaultMessage, LocaleContextHolder.getLocale(), wrappedException);
	}

	public QuadMaxException(MessageSource messageSource, String messageKey, Object[] messageParameters,
	        String defaultMessage, Locale locale, Exception wrappedException) {
		
		try {
			String tempDefaultMessage = defaultMessage; 
			if(null == defaultMessage || "".equals(defaultMessage)) {
				tempDefaultMessage = messageKey;
			}
			
			LOGGER.debug("messageParameters : {}", messageParameters);
			LOGGER.debug("messageParameters length: {}", messageParameters.length);
			this.messageKey = messageKey;
			this.messageParameters = new Object[messageParameters.length]; 
			int len = messageParameters.length;
			for(int i = 0; i < len; i++) {
				this.messageParameters[i] = messageParameters[i];
			}
			this.message = messageSource.getMessage(messageKey, messageParameters, tempDefaultMessage, locale);
			this.wrappedException = wrappedException;
		} catch(Exception e) {
			LOGGER.error("QuadMaxException :", e);
		}
		
	}
}

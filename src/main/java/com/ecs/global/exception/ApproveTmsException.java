package com.ecs.global.exception;

public class ApproveTmsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2094580230664293005L;

	public ApproveTmsException(String message) {
		super(message);
	}
}

package com.ecs.global.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
	
	private static final Locale LOCALE_COUNTRY = Locale.KOREA;
	
	private DateUtil() {
	    throw new IllegalStateException("Utility class");
	}
			
	/**
	 * Date형 날짜를 포맷에 맞는 문자열로 변환
	 *
	 * @param dt : 문자열로 변경할 날짜
	 * @param format : 날짜포맷(yyyy-MM-dd HH:mm:ss)
	 * @return String : 포맷형식으로 변환된 시간
	 */
	public static final String getDateToString(Date dt, String format) {
		DateFormat df = new SimpleDateFormat(format, Locale.KOREA);
		return df.format(dt);
	}

	/**
	 * Date를 타임존에 해당하는 시간으로 포맷에 맞게 변환하여 리턴하는 함수
	 * @param timeZone : 타임존(Asia/Seoul, Greenwich, America/Los_Angeles, America/New_York 등)
	 * @param format : 날짜포맷(yyyy-MM-dd HH:mm:ss)
	 * @param date : 변환할 Date
	 * @return String : 타임존의 시간
	 */
	public static final String getDateTimeOfGMT(String timeZone, String format, Date date) {
		DateFormat df = new SimpleDateFormat(format, Locale.KOREA);
		TimeZone tz = TimeZone.getTimeZone(timeZone);
		df.setTimeZone(tz);

		return df.format(date);
	}


	public static final Date addMinute(Date dt, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.MINUTE, minutes);
		return cal.getTime();
	}

	public static final Date addSecond(Date dt, int seconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.SECOND, seconds);
		return cal.getTime();
	}

	public static final Date addDay(Date dt, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}


	public static final String getYYYYMMDD(Date dt) {
		DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		return df.format(dt);
	}



	public static Date getAddMonth(Date dt, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.MONTH, amount);
		return cal.getTime();
	}


	public static Date getAddYear(Date dt, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.YEAR, amount);
		return cal.getTime();
	}

	public static Date getAddHour(Date dt, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.HOUR_OF_DAY, amount);
		return cal.getTime();
	}


	public static Date getAddDate(Date dt, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DATE, amount);
		return cal.getTime();
	}

	public static int getYear(Date dt){
		Calendar cal = Calendar.getInstance(Locale.FRANCE);
		cal.setTime(dt);
		return cal.get(Calendar.YEAR);
	}

	public static int getMonth(Date dt){
		Calendar cal = Calendar.getInstance(Locale.FRANCE);
		cal.setTime(dt);
		return cal.get(Calendar.MONTH);
	}

	@SuppressWarnings("deprecation")
	public static Date getLastDayOfMon(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.set(dt.getYear(), dt.getMonth(), 1);
		dt.setDate(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return dt;
	}

	public static final  int REAL = 1;
	public static final  int FIRST = 2;
	public static final  int LAST  = 3;

	/**
	 * <p>입력 날짜와 시각을  DBFormat 형태로 변환 후 return.
	 *
	 * @param dt
	 * @param calcMethod [1:실제값, 2:첫번째일, 3:마지막일]
	 * @param intervalType [년도기준:yyyy,월:m,일자:d,시:h ,10분:10mi,15분:15mi, 30분:30mi]
	 * @param interval
	 * @param outputDBFormat
	 * @return EvaluationValue
	 *
	 * <pre>
	 *  - 사용 예
	 * String strValue = DateUtil.getDateParamEvaluation(now(),"1:실제값","년도기준:yyyy",-1,"YYYYMMDD")
	 * </pre>
	 */
	public static final String getDateParamEvaluation(Date dt,String calcMethod,String intervalType,int interval,String outputDBFormat) throws ParseException{
		Date toDate = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);

		int intervalUnit =  Calendar.DATE;
		int intervalCount = 1;

		switch(intervalType){
		case "년도기준:yyyy":
			intervalUnit = Calendar.YEAR;
			intervalCount = 1;
			break;
		case "월:m":
			intervalUnit = Calendar.MONTH;
			intervalCount = 1;
			break;
		case "일자:d":
			intervalUnit = Calendar.DATE;
			intervalCount = 1;
			break;
		case "시:h":
			intervalUnit = Calendar.HOUR;
			intervalCount = 1;
			break;
		case "10분:10mi":
			intervalUnit = Calendar.MINUTE;
			intervalCount = 10;
			break;
		case "1분:1mi":
			intervalUnit = Calendar.MINUTE;
			intervalCount = 1;
			break;
		case "15분:15mi":
			intervalUnit = Calendar.MINUTE;
			intervalCount = 15;
			break;
		case "30분:30mi":
			intervalUnit = Calendar.MINUTE;
			intervalCount = 30;
			break;
		default:
			break;
		}

		cal.add(intervalUnit, interval*intervalCount);
		toDate = cal.getTime();

		if(calcMethod.equals("2:첫번째일")){
			switch(intervalUnit){
			case Calendar.YEAR:
				toDate = getToDate(getToString(toDate,"YYYY"),"YYYY");
				break;
			case Calendar.MONTH:
				toDate = getToDate(getToString(toDate,"YYYYMM"),"YYYYMM");
				break;
			case Calendar.DATE:
				toDate = getToDate(getToString(toDate,"YYYYMMDD"),"YYYYMMDD");
				break;
			case Calendar.HOUR:
				toDate = getToDate(getToString(toDate,"YYYYMMDDHH"),"YYYYMMDDHH");
				break;
			case Calendar.MINUTE:
				toDate = getToDate(getToString(toDate,"YYYYMMDDHHMI"),"YYYYMMDDHHMI");
				break;
			default:
				break;
			}
			cal.setTime(toDate);
			while(cal.get(intervalUnit) % intervalCount != 0 ){
				cal.add(intervalUnit, -1);
			}
			toDate = cal.getTime();
		}
		else if(calcMethod.equals("3:마지막일")){
			switch(intervalUnit){
			case Calendar.YEAR:
				toDate = getToDate(getToString(toDate,"YYYY")+"1231235959","YYYYMMDDHHMISS");
				break;
			case Calendar.MONTH:
				toDate = getToDate(getToString(toDate,"YYYYMM")+"01235959","YYYYMMDDHHMISS");
				break;
			case Calendar.DATE:
				toDate = getToDate(getToString(toDate,"YYYYMMDD")+"235959","YYYYMMDDHHMISS");
				break;
			case Calendar.HOUR:
				toDate = getToDate(getToString(toDate,"YYYYMMDDHH")+"5959","YYYYMMDDHHMISS");
				break;
			case Calendar.MINUTE:
				toDate = getToDate(getToString(toDate,"YYYYMMDDHHMI")+"59","YYYYMMDDHHMISS");
				break;
			default:
				break;
			}
			cal.setTime(toDate);
			if(cal.get(intervalUnit)+1 % intervalCount != 0){
				cal.add(intervalUnit, +1);
				while(cal.get(intervalUnit) % intervalCount != 0 ){
					cal.add(intervalUnit, +1);
				}
				cal.add(intervalUnit, -1);
			}
			if(intervalUnit ==  Calendar.MONTH){
				cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			toDate = cal.getTime();
		}

		return getToString(toDate,outputDBFormat);
	}


	/**
	 * <p>입력 날짜와 시각을  DBFormat 형태로 변환 후 return.
	 *
	 * @param dt
	 * @param dBFormat
	 * @return strDate
	 *
	 * <pre>
	 *  - 사용 예
	 * String strDate = DateUtil.getToString(now(),"YYYY/MM/DD HH:MMISS")
	 * </pre>
	 */
	public static final String getToString(Date dt, String dBFormat) {
		String toFormat ;
		toFormat = dBFormat;
		toFormat = toFormat.replace("YYYY", "yyyy");
		toFormat = toFormat.replace("MM", "MM");
		toFormat = toFormat.replace("DD", "dd");
		toFormat = toFormat.replace("HH", "HH");
		toFormat = toFormat.replace("MI", "mm");
		toFormat = toFormat.replace("SS", "ss");

		DateFormat df = new SimpleDateFormat(toFormat, Locale.KOREA);
		return df.format(dt);
	}

	/**
	 * <p>입력 날짜와 시각을  DBFormat 형태로 변환 후 return.
	 *
	 * @param dBFormat
	 * @param stringDate
	 * @return Date
	 *
	 * <pre>
	 *  - 사용 예
	 * String strDate = DateUtil.getToDate("20150101012110","YYYYMMDDHHMISS")
	 * </pre>
	 * @throws ParseException
	 */
	public static final Date getToDate(String stringDate, String dBFormat) throws ParseException {
		Date dt;
		String toFormat ;
		toFormat = dBFormat;
		toFormat = toFormat.replace("YYYY", "yyyy");
		toFormat = toFormat.replace("MM", "MM");
		toFormat = toFormat.replace("DD", "dd");
		toFormat = toFormat.replace("HH", "HH");
		toFormat = toFormat.replace("MI", "mm");
		toFormat = toFormat.replace("SS", "ss");
		DateFormat df = new SimpleDateFormat(toFormat, Locale.KOREA);

		String stringDateCopy = stringDate;
		if(stringDateCopy.length() > dBFormat.length())
			stringDateCopy = stringDateCopy.substring(0, dBFormat.length()-1);

		dt = df.parse(stringDateCopy);
		return dt;
	}

	/**
	 * <p>현재 날짜와 시각을  yyyyMMdd 형태로 변환 후 return.
	 *
	 * @param cal
	 * @return yyyyMMdd
	 *
	 * <pre>
	 *  - 사용 예
	 * String date = DateUtil.getYyyymmdd()
	 * </pre>
	 */
	public static String getYYYYMMDD(Calendar cal) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		return formatter.format(cal.getTime());
	}

	/**
	 * <p>GregorianCalendar 객체를 반환함.
	 *
	 * @param yyyymmdd 날짜 인수
	 * @return GregorianCalendar
	 * @see Calendar
	 * @see GregorianCalendar
	 * <p><pre>
	 *  - 사용 예
	 * Calendar cal = DateUtil.getGregorianCalendar(DateUtil.getCurrentYyyymmdd())
	 * </pre>
	 */
	public static GregorianCalendar getGregorianCalendar(String yyyymmdd) {

		int yyyy = Integer.parseInt(yyyymmdd.substring(0, 4));
		int mm = Integer.parseInt(yyyymmdd.substring(4, 6));
		int dd = Integer.parseInt(yyyymmdd.substring(6));

		return new GregorianCalendar(yyyy, mm - 1, dd, 0, 0, 0);

	}

	/**
	 * <p>현재 날짜와 시각을  yyyyMMddhhmmss 형태로 변환 후 return.
	 *
	 * @return yyyyMMddhhmmss
	 * @see Date
	 * @see Locale
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.getCurrentDateTime()
	 * </pre>
	 */
	public static String getCurrentDateTime() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "yyyyMMddHHmmss";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);
	}

	/**
	 *
	 * @return
	 */
	public static String getCurrentYear() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);
	}
	
	public static String getCurrentYearMonth() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "yyyyMM";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);
	}

	/**
	 * 현재년도 int로 추출.
	 * @param year +-년도 0이면 현재년도
	 * @return int
	 */
	public static int getCurrentYear(final int year) {
		Calendar now = Calendar.getInstance();
		return now.get(Calendar.YEAR) + year;
	}

	/**
	 *
	 * @return
	 */
	public static String getCurrentMonth() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "MM";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);
	}

	/**
	 *
	 * @return
	 */
	public static String getCurrentDay() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "dd";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);
	}


	/**
	 * <p>현재  시각을  hhmmss 형태로 변환 후 return.
	 *
	 * @return hhmmss
	 * @see Date
	 * @see Locale
	 * <p><pre>
	 *  - 사용 예
	 *  String date = DateUtil.getCurrentDateTime()
	 * </pre>
	 */
	public static String getCurrentTime() {
		Date today = new Date();
		Locale currentLocale = new Locale("KOREAN", "KOREA");
		String pattern = "HHmmss";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, currentLocale);
		return formatter.format(today);

	}

	/**
	 * <p>현재 날짜를 yyyyMMdd 형태로 변환 후 return.
	 *
	 * @return yyyyMMdd *
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.getCurrentYyyymmdd()
	 * </pre>
	 */
	public static String getCurrentYyyymmdd() {
		return getCurrentDateTime().substring(0, 8);
	}

	/**
	 * <p>주로 일자를 구하는 메소드.
	 *
	 * @param yyyymm 년월
	 * @param week 몇번째 주
	 * @param pattern 리턴되는 날짜패턴 (ex:yyyyMMdd)
	 * @return 연산된 날짜
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.getWeekToDay("200801" , 1, "yyyyMMdd")
	 * </pre>
	 */
	public static String getWeekToDay(String yyyymm, int week, String pattern) {
		
		int weekCopy = week;

		Calendar cal = Calendar.getInstance(Locale.FRANCE);

		int newYy = Integer.parseInt(yyyymm.substring(0,4));
		int newMm = Integer.parseInt(yyyymm.substring(4,6));
		int newDd = 1;

		cal.set(newYy,newMm-1,newDd);

		// 임시 코드
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			weekCopy = weekCopy - 1;
		}

		cal.add(Calendar.DATE, (weekCopy-1)*7+(cal.getFirstDayOfWeek()-cal.get(Calendar.DAY_OF_WEEK)));

		SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.FRANCE);

		return formatter.format(cal.getTime());

	}

	/**
	 * <p>지정된 플래그에 따라 연도 , 월 , 일자를 연산한다.
	 *
	 * @param field 연산 필드
	 * @param amount 더할 수
	 * @param date 연산 대상 날짜
	 * @return 연산된 날짜
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.getOpDate(java.util.Calendar.DATE , 1, "20080101")
	 * </pre>
	 */
	public static String getOpDate(int field, int amount, String date) {

		Calendar calDate = getGregorianCalendar(date);

		if (field == Calendar.YEAR) {
			calDate.add(Calendar.YEAR, amount);
		} else if (field == Calendar.MONTH) {
			calDate.add(Calendar.MONTH, amount);
		} else {
			calDate.add(Calendar.DATE, amount);
		}

		return getYYYYMMDD(calDate);

	}

	/**
	 * <p>입력된 년월의 마지막 일수를 return 한다.
	 *
	 * @param yyyymmdd
	 * @param addDay
	 * @return 마지막 일수
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * int date = DateUtil.getLastDayOfMon(2008 , 1)
	 * </pre>
	 */
	public static int getWeek(String yyyymmdd, int addDay){ 
		Calendar cal = Calendar.getInstance(Locale.FRANCE);
		int newYy = Integer.parseInt(yyyymmdd.substring(0,4));
		int newMm = Integer.parseInt(yyyymmdd.substring(4,6));
		int newDd = Integer.parseInt(yyyymmdd.substring(6,8));

		cal.set(newYy,newMm-1,newDd);
		cal.add(Calendar.DATE, addDay);

		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * <p>입력된 년월의 마지막 일수를 return 한다.
	 *
	 * @param year
	 * @param month
	 * @return 마지막 일수
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * int date = DateUtil.getLastDayOfMon(2008 , 1)
	 * </pre>
	 */
	public static int getLastDayOfMon(int year, int month) {

		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);

	}

	/**
	 * <p>입력된 년월의 마지막 일수를 return한다
	 *
	 * @param yyyymm
	 * @return 마지막 일수
	 * <p><pre>
	 *  - 사용 예
	 * int date = DateUtil.getLastDayOfMon("2008")
	 * </pre>
	 */
	public static int getLastDayOfMon(String yyyymm) {

		Calendar cal = Calendar.getInstance();
		int yyyy = Integer.parseInt(yyyymm.substring(0, 4));
		int mm = Integer.parseInt(yyyymm.substring(4)) - 1;

		cal.set(yyyy, mm, 1);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * <p>입력된 날자가 올바른지 확인합니다.
	 *
	 * @param yyyymmdd
	 * @return boolean
	 * <p><pre>
	 *  - 사용 예
	 * boolean b = DateUtil.isCorrect("20080101")
	 * </pre>
	 */
	public static boolean isCorrect(String yyyymmdd) {
		boolean flag  =  false;
		if(yyyymmdd.length() < 8 ) {
			return false;
		} else {
			try {
				int yyyy = Integer.parseInt(yyyymmdd.substring(0, 4));
				int mm = Integer.parseInt(yyyymmdd.substring(4, 6));
				int dd = Integer.parseInt(yyyymmdd.substring(6));
				flag = isCorrect(yyyy, mm, dd);
			} catch(Exception ex) {
				LOGGER.error(ex.getMessage(), ex);
				return false;
			}
		}
		return flag;
	}

	/**
	 * <p>입력된 날자가 올바른 날자인지 확인합니다.
	 *
	 * @param yyyy
	 * @param mm
	 * @param dd
	 * @return boolean
	 * <p><pre>
	 *  - 사용 예
	 * boolean b = DateUtil.isCorrect(2008,1,1)
	 * </pre>
	 */
	public static boolean isCorrect(int  yyyy, int mm, int dd) {
		if(yyyy < 0 || mm < 0 || dd < 0) return false;
		if(mm > 12 || dd > 31) return false;

		String year    = String.valueOf(yyyy);
		String month    = "00" + mm;
		String yearStr = year + month.substring(month.length() - 2);
		int endday      = getLastDayOfMon(yearStr);

		return dd <= endday;
	}

	/**
	 * <p>현재 일자를 입력된 type의 날짜로 반환합니다.
	 *
	 * @param type
	 * @return String
	 * @see DateFormat
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.getThisDay("yyyymmddhhmmss")
	 * </pre>
	 */
	public static String getThisDay(String type) {
		
		String typeCopy = type;
		
		Date date = new Date();
		SimpleDateFormat sdf = null;
		typeCopy = typeCopy.toLowerCase();

		try{ 
			if(typeCopy.equals("yyyymmdd")) { 
				sdf = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
				return sdf.format(date);
			}
			if(typeCopy.equals("yyyymmddhh")) { 
				sdf = new SimpleDateFormat("yyyyMMddHH", Locale.KOREA);
				return sdf.format(date);
			}
			if(typeCopy.equals("yyyymmddhhmm")) { 
				sdf = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
				return sdf.format(date);
			}
			if(typeCopy.equals("yyyymmddhhmmss")) { 
				sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
				return sdf.format(date);
			}
			if(typeCopy.equals("yyyymmddhhmmssms")) { 
				sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.KOREA);
				return sdf.format(date);
			} else { 
				sdf = new SimpleDateFormat(typeCopy, Locale.KOREA);
				return sdf.format(date);
			}
		} catch (Exception e) {
			LOGGER.error("[ ERROR ]: parameter must be 'YYYYMMDD', 'YYYYMMDDHH', 'YYYYMMDDHHSS'or 'YYYYMMDDHHSSMS' ", e);
			return "[ ERROR ]: parameter must be 'YYYYMMDD', 'YYYYMMDDHH', 'YYYYMMDDHHSS'or 'YYYYMMDDHHSSMS'";
		}
	}

	/**
	 * <p>입력된 일자를 '9999년 99월 99일' 형태로 변환하여 반환한다.
	 *
	 * @param yyyymmdd
	 * @return String
	 * <p><pre>
	 *  - 사용 예
	 * String date = DateUtil.changeDateFormat("20080101")
	 * </pre>
	 */
	public static String changeDateFormat(String yyyymmdd) {
		String rtnDate=null;

		String yyyy = yyyymmdd.substring(0, 4);
		String mm  = yyyymmdd.substring(4,6);
		String dd  = yyyymmdd.substring(6,8);
		rtnDate=yyyy+" 년 "+mm + " 월 "+dd + " 일";

		return rtnDate;

	}

	/**
	 * <p>두 날짜간의 날짜수를 반환(윤년을 감안함)
	 *
	 * @param sd 시작 날짜
	 * @param ed 끝 날짜
	 * @return 날수
	 * @see GregorianCalendar
	 * <p><pre>
	 *  - 사용 예
	 * long date = DateUtil.getDifferDays("20080101","20080202")
	 * </pre>
	 */
	public static long getDifferDays(String sd, String ed) {

		GregorianCalendar startDate = getGregorianCalendar(sd);
		GregorianCalendar endDate = getGregorianCalendar(ed);
		return (endDate.getTime().getTime() - startDate.getTime().getTime()) / 86400000;

	}

	/**
	 * <p>현재의 요일을 구한다.
	 *
	 * @param
	 * @return 요일
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * int day = DateUtil.getDayOfWeek()
	 *  SUNDAY    = 1
	 *  MONDAY    = 2
	 *  TUESDAY  = 3
	 *  WEDNESDAY = 4
	 *  THURSDAY  = 5
	 *  FRIDAY    = 6
	 * </pre>
	 */
	public static int getDayOfWeek(){
		return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * <p>현재주가 올해 전체의 몇째주에 해당되는지 계산한다.
	 *
	 * @param
	 * @return 요일
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * int day = DateUtil.getWeekOfYear()
	 * </pre>
	 */
	public static int getWeekOfYear(){
		Calendar rightNow = Calendar.getInstance(LOCALE_COUNTRY);
		return rightNow.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * <p>현재주가 현재월에 몇째주에 해당되는지 계산한다.
	 *
	 * @param
	 * @return 요일
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * int day = DateUtil.getWeekOfMonth()
	 * </pre>
	 */
	public static int getWeekOfMonth(){
		Calendar rightNow = Calendar.getInstance(LOCALE_COUNTRY);
		return rightNow.get(Calendar.WEEK_OF_MONTH);
	}

	/**
	 * <p>해당 p_date날짜에 Calendar 객체를 반환함.
	 *
	 * @param pDate
	 * @return Calendar
	 * @see Calendar
	 * <p><pre>
	 *  - 사용 예
	 * Calendar cal = DateUtil.getCalendarInstance(DateUtil.getCurrentYyyymmdd())
	 * </pre>
	 */
	public static Calendar getCalendarInstance(String pDate){
		Calendar retCal = Calendar.getInstance(LOCALE_COUNTRY);

		if (pDate != null && pDate.length() == 8) {
			int year  = Integer.parseInt(pDate.substring(0,4));
			int month = Integer.parseInt(pDate.substring(4,6))-1;
			int date  = Integer.parseInt(pDate.substring(6));

			retCal.set(year, month, date);
		}
		return retCal;
	}

	/**
	 * 현재 날짜에서 일을 더한 만큼의 날자를 돌려준다
	 * @param amount
	 * @return
	 */
	public static String getAddDate( int amount ){
		Calendar cal = Calendar.getInstance(Locale.getDefault());
		cal.add(Calendar.DATE, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(month).append(day);
		return buf.toString();
	}

	/**
	 * 현재 날짜에서 일을 더한 만큼의 날자를 돌려준다
	 * @param amount
	 * @return
	 */
	public static String getAddMonth( int amount ){
		Calendar cal = Calendar.getInstance(Locale.getDefault());
		cal.add(Calendar.MONTH, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(month).append(day);
		return buf.toString();
	}



	/**
	 * 특정날짜에서 달을 더한 날짜를 돌려준다
	 * @param date
	 * @param amount
	 * @return
	 */
	public static String getAddDate(String date, int amount )  throws ParseException{
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date dt =  fommatter.parse(date);

		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DATE, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(month).append(day);
		return buf.toString();
	}

	/**
	 * 특정날짜에서 달을 더한 날짜를 돌려준다
	 * @param date
	 * @param amount
	 * @return
	 */
	public static String getAddMonth(String date, int amount )  throws ParseException{
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date dt =  fommatter.parse(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.MONTH, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(month).append(day);
		return buf.toString();
	}

	public static String getAddMonth(String date, int amount, String delimiter )  throws ParseException{
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date dt =  fommatter.parse(date.replaceAll(delimiter, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.MONTH, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(delimiter+month)
				.append(delimiter+day);
		return buf.toString();
	}

	/**
	 * 특정날짜에서 달을 더한 날짜를 돌려준다
	 * @param date
	 * @param amount
	 * @return
	 */
	public static String getAddDate(String date, int amount, String delimiter )  throws ParseException{
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date dt =  fommatter.parse(date.replaceAll(delimiter, ""));

		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DATE, amount);
		StringBuilder buf = new StringBuilder();
		buf.append(Integer.toString(cal.get(1)));
		String month = Integer.toString(cal.get(2) + 1);
		if(month.length() == 1)
			month = "0" + month;
		String day = Integer.toString(cal.get(5));
		if(day.length() == 1)
			day = "0" + day;
		buf.append(delimiter+month)
				.append(delimiter+day);
		return buf.toString();
	}
	/**
	 * 현재날자의 다음달을 되돌려준다
	 * @return 20110113
	 */
	public static String getNowNextMonth(){
		SimpleDateFormat fommatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH,1);
		return fommatter.format(calendar.getTime());
	}

	/**
	 * 현재 날자의 밀리세컨드 날자를  가지고 온다
	 * @return
	 */
	public static long getCurTimeMili() {
		Calendar	cal					=	Calendar.getInstance();
		return	cal.getTimeInMillis();
	}

	/**
	 * 날자를 받아 밀리세컨드 값을 가지고 온다
	 * @param datetime
	 * @return
	 * @throws ParseException
	 */
	public static long getTimeMili(String datetime) throws ParseException {
		long value=0;
		if (datetime != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
			Date date = sdf.parse(datetime);
			value = date.getTime();
		} else {
			return	0;
		}
		return	value;
	}

	/**
	 * 스트링 timestamp 값을 받아 원하는 포맷 형태로 변화하여 리턴한다
	 * @param date
	 * @return
	 */
	public static String getConvertDate(String date,String type) {
		return getConvertDate(date,type,"-");
	}

	public static String getConvertDate(String date,String type,String delimeter) {
		String dateCopy = date;
		if(dateCopy == null) {
			return	"";
		}
		dateCopy							=	dateCopy.trim();

		if(dateCopy.length() == 6 && type.equals("5")) {
			return	dateCopy.substring(0, 4) + delimeter + dateCopy.substring(4, 6);
		}

		if(type.equals("1")){
			return	dateCopy.substring(0, 4) + delimeter + dateCopy.substring(4, 6) + delimeter + dateCopy.substring(6, 8);
		}else if(type.equals("2")){
			return	dateCopy.substring(0, 4) + delimeter + dateCopy.substring(4, 6) + delimeter + dateCopy.substring(6, 8) + " " + dateCopy.substring(8, 10);
		}else if(type.equals("3")){
			return	dateCopy.substring(0, 4) + delimeter + dateCopy.substring(4, 6) + delimeter + dateCopy.substring(6, 8) + " " + dateCopy.substring(8, 10) + ":" + dateCopy.substring(10, 12);
		}else if(type.equals("4")){
			return	dateCopy.substring(0, 4) + delimeter + dateCopy.substring(4, 6) + delimeter + dateCopy.substring(6, 8) + " " + dateCopy.substring(8, 10) + ":" + dateCopy.substring(10, 12) + ":" + dateCopy.substring(12, 14);
		}else{
			return  dateCopy;
		}
	}

	/**
	 * 시작일부터 종료일까지 사이의 월을 배열에 담아 리턴
	 * ( 시작일과 종료일을 모두 포함한다 )
	 * @param fromDate yyyyMMdd 형식의 시작일
	 * @param toDate yyyyMMdd 형식의 종료일
	 * @return yyyyMMdd 형식의 날짜가 담긴 배열
	 * @throws ParseException
	 */
	public static String[] getDiffMonth(String fromDate, String toDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM", Locale.KOREA);
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(fromDate));

		int count = getDiffDayCount(fromDate, toDate);
		// 시작일부터
		cal.add(Calendar.MONTH, -1);
		// 데이터 저장
		List<String> list = new ArrayList<>();
		for (int i = 0; i <= count; i++) {
			cal.add(Calendar.MONTH, 1);
			list.add(sdf.format(cal.getTime()));
		}
		String[] result = new String[list.size()];
		list.toArray(result);

		return result;
	}

	/**
	 * 두날짜 사이의 일수를 리턴
	 * @param fromDate yyyyMMdd 형식의 시작일
	 * @param toDate yyyyMMdd 형식의 종료일
	 * @return 두날짜 사이의 일수
	 * @throws ParseException
	 */
	public static int getDiffDayCount(String fromDate, String toDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		return (int) ((sdf.parse(toDate).getTime() - sdf.parse(fromDate).getTime()) / 1000 / 60 / 60 / 24);
	}

	/**
	 * String 을 Date 타입으로 변환(quartz용)
	 * @param strDate
	 * @return Date
	 * @throws ParseException
	 */
	public static Date stringToDate(String strDate) throws ParseException{
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S", Locale.KOREA).parse(strDate);
	}

	/**
	 * 현재시간을 백분의 1초까지 반환해준다.
	 * @return
	 */
	public static String currentTimeMillDate(){
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.KOREA);
		return dayTime.format(new Date(time));
	}

	/**
	 * 시간 문자열을 날짜만을 가져와서 웹상에서 표시할 형식으로 변환<BR>
	 * 길이가 맞지 않거나 null일때 원래 스트링을 리턴함<BR>
	 * strFormat = "/"
	 * Ex) "20011014123020" --> "2001/10/14"
	 * @param strDate 시간 문자열
	 * @return 날짜만의 문자열
	 */
	public static String getFormattedDate(String strDate , String strFormat) {
		if(strDate == null || strDate.equals("") || !(strDate.length() == 8 || strDate.length() == 14)) {
			return strDate;
		}

		// 날짜를 가져온다.
		StringBuilder sbufferFormattedDay = new StringBuilder("");

		sbufferFormattedDay.append(strDate.substring(0, 4))
				.append(strFormat);

		String strMonth = strDate.substring(4, 6);
		sbufferFormattedDay.append(strMonth)
				.append(strFormat);

		String strDay = strDate.substring(6, 8);
		sbufferFormattedDay.append(strDay);

		return sbufferFormattedDay.toString();
	}

	/**
	 * 주어진 000000시간을 strFormat delimiter를 적용하여 추출.
	 * @param strTime
	 * @param strFormat
	 * @return
	 */
	public static String getFormattedTime(String strTime, String strFormat) {
		StringBuilder str = new StringBuilder();

		if(strTime!=null && !strTime.equals("")) {
			str.append(strTime.substring(0, 2));
			str.append(strFormat);
			str.append(strTime.substring(2, 4));
			str.append(strFormat);
			str.append(strTime.substring(4, 6));
		}

		return str.toString();
	}
}

package com.ecs.global.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.infrastructure.model.ResultMap;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.FastArrayList;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.ContextLoader;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

/**
 * 공통 유틸
 *
 */
public class CmmnUtil {

	static final Logger LOGGER = LoggerFactory.getLogger(CmmnUtil.class);

	static Runtime r = Runtime.getRuntime();
	static long free1;
	
	private CmmnUtil() {
		throw new IllegalStateException("Utility class");
	}

	static ModelMapper modelMapper = new ModelMapper();

	/**
	 * 메모리 사용상태를 확인한다.
	 *
	 * @return 메모리 사용상태
	 */
	public static String showMemory() {

		//JVM이 현재 시스템에 요구 가능한 최대 메모리량, 이 값을 넘으면 OutOfMemory 오류가 발생 합니다.
		long max = r.maxMemory();

		//JVM이 현재 시스템에 얻어 쓴 메모리의 총량
		long total = r.totalMemory();
		//JVM이 현재 시스템에 청구하여 사용중인 최대 메모리(total)중에서 사용 가능한 메모리
		long free = r.freeMemory();
		long usage=0l;
		if(0!=free1) {
			usage = free1-free;
		}
		free1=free;

		String usageStr;
		if(usage<0) {
			usageStr = "GC:"+FileUtils.byteCountToDisplaySize(Math.abs(usage));
		}else{
			usageStr = FileUtils.byteCountToDisplaySize(usage);
		}

		double percentage = 1d*free/total*100;
		return String.format("Max:%s, Total:%s [%.2f%%], Free:%s, Usage:%s", FileUtils.byteCountToDisplaySize(max),FileUtils.byteCountToDisplaySize(total),percentage,FileUtils.byteCountToDisplaySize(free)
				,usageStr);
	}

	/**
	 * web root 부터의 절대경로
	 * @param path
	 * @return 경로
	 */
	public static String getRealPath(String path) {

		return System.getProperty("webRoot") + path;
	}

	/**
	 * Spring Bean을 가져온다.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object getBean(Class beanType) {
		try {
			return ContextLoader.getCurrentWebApplicationContext().getBean(beanType);
		}catch (NoSuchBeanDefinitionException e){
			LOGGER.warn(e.getMessage());
		}
		return null;
	}

	/**
	 * Spring Bean을 가져온다.
	 */
	public static Object getBean(String beanName) {
		return ContextLoader.getCurrentWebApplicationContext().getBean(beanName);
	}

	/**
	 * 카멜케이스 변환
	 * @param str - 변환대상 문자
	 * @param firstUpper - 첫문자 대문자 여부
	 * @return 변환된 문자열
	 */
	public static String toCamelCase(String str, boolean firstUpper) {
		StringBuilder camelCaseString = new StringBuilder();

		String[] parts = StringUtils.split(StringUtils.lowerCase(str), "_");
		for (int i=0; i<parts.length; i++){
			if (i == 0 && !firstUpper) {
				camelCaseString.append(parts[i]);
			} else {
				camelCaseString.append(StringUtils.capitalize(parts[i]));
			}
		}

		return camelCaseString.toString();
	}

	/**
	 * 카멜케이스 변환
	 * @param str - 변환대상 문자
	 * @return 변환된 문자열
	 */
	public static String toCamelCase(String str) {
		return toCamelCase(str, false);
	}

	/**
	 * 카멜케이스 문자 역변환
	 * @param str - 변환대상 카멜케이스 문자
	 * @return 변환된 문자열
	 */
	public static String fromCamelCase(String str) {
		return fromCamelCase(str, "_", false);
	}

	/**
	 * 카멜케이스 문자 역변환
	 * @param str - 변환대상 카멜케이스 문자
	 * @param isLower - 소문자 변환 여부
	 * @return 변환된 문자열
	 */
	public static String fromCamelCase(String str, boolean isLower) {
		return fromCamelCase(str, "_", isLower);
	}

	/**
	 * 카멜케이스 문자 역변환
	 * @param str - 변환대상 카멜케이스 문자
	 * @param delimiter - 변환 구분자
	 * @param isLower - 소문자 변환 여부
	 * @return 변환된 문자열
	 */
	public static String fromCamelCase(String str, String delimiter, boolean isLower) {
		StringBuilder replaceStringSb = new StringBuilder();

		String[] strs = StringUtils.splitByCharacterTypeCamelCase(str);
		for (String s : strs) {
			if (replaceStringSb.length() > 0 && !StringUtils.isNumeric(s)) {
				replaceStringSb.append(delimiter);
			}
			String replaceString = isLower ? StringUtils.lowerCase(s) : StringUtils.upperCase(s);
			replaceStringSb.append(replaceString);
		}

		return replaceStringSb.toString();
	}

	public static <T,S> List<S> deepCopyVoList(List<T> originList, Class<S> voClass) {
		List<S> voList = new ArrayList<>();
		for (T source : originList) {
			voList.add(modelMapper.map(source, voClass));
		}
		return voList;
	}
	public static Map<String, Object> convertToMap(Object obj)
			throws IllegalAccessException, InstantiationException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		if (obj == null) {
			return Collections.emptyMap();
		}
		Map<String, Object> convertMap = new HashMap<>();
		Field[] fields = obj.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			convertMap.put(field.getName(), field.get(obj));
		}
		return convertMap;
	}
	public static List<Map<String, Object>> convertToMapList(List<?> list)
		throws IllegalAccessException, InstantiationException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		if (list == null || list.isEmpty()) {
			return Collections.emptyList();
		}
		List<Map<String, Object>> convertList = new ArrayList<>();
		for (Object obj : list) {
			convertList.add(convertToMap(obj));
		}
		return convertList;
	}

	/**
	 * ResultMap을 다른 객체로 변환
	 * @param orig ResultMap
	 * @param dest 변환할 객체
	 */
	public static void convertResultMap(ResultMap orig, Object dest) {
		try {
			Field[] dataFields = dest.getClass().getDeclaredFields();
			Object data;
			Class<?> dataType;
			Class<?> fieldType;
			for (Field f : dataFields) {
				if (f.getModifiers() == 2) { // only private field
					data = MapUtils.getObject(orig, f.getName());
					if (data != null) {
						dataType = data.getClass();
						fieldType = f.getType();
						f.setAccessible(true);

						if (Clob.class.equals(dataType)) {
							try (Reader r = ((Clob) data).getCharacterStream();
									BufferedReader br = new BufferedReader(r);) {
								f.set(dest, IOUtils.toString(br));
							}
						} else if (Blob.class.equals(dataType)) {
							f.set(dest, data);
						} else if (Integer.class.equals(fieldType) || Integer.TYPE.equals(fieldType)) {
							f.set(dest, NumberUtils.toInt(String.valueOf(data)));
						} else if (Long.class.equals(fieldType) || Long.TYPE.equals(fieldType)) {
							f.set(dest, NumberUtils.toLong(String.valueOf(data)));
						} else if (Double.class.equals(fieldType) || Double.TYPE.equals(fieldType)) {
							f.set(dest, NumberUtils.toDouble(String.valueOf(data)));
						} else if (Float.class.equals(fieldType) || Float.TYPE.equals(fieldType)) {
							f.set(dest, NumberUtils.toFloat(String.valueOf(data)));
						} else if (Short.class.equals(fieldType) || Short.TYPE.equals(fieldType)) {
							f.set(dest, NumberUtils.toShort(String.valueOf(data)));
						} else if (Boolean.class.equals(fieldType) || Boolean.TYPE.equals(fieldType)) {
							f.set(dest, "TRUE".equalsIgnoreCase(String.valueOf(data)) || "Y".equalsIgnoreCase(String.valueOf(data)));
						} else if (BigDecimal.class.equals(fieldType)) {
							f.set(dest, new BigDecimal(String.valueOf(data)));
						} else if (Date.class.equals(fieldType)) {
							if (Timestamp.class.equals(dataType)) {
								f.set(dest, new Date(((Timestamp) data).getTime()));
							}
						} else if (Timestamp.class.equals(fieldType)) {
							if (Timestamp.class.equals(dataType)) {
								f.set(dest, data);
							}
						} else {
							f.set(dest, String.valueOf(data));
						}
					}
				}
			}
		} catch (SQLException | IOException | IllegalAccessException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * ResultMap을 다른 객체로 생성하여 변환
	 * @param orig ResultMap
	 * @param voClass 변환할 객체 클래스
	 * @param <T>
	 * @return
	 */
	public static <T> T convertResultMap(ResultMap orig, Class<T> voClass) {
		T clazz = null;

		//값이 있을 경우만 입력함.
		if(null != orig) {
			try {
				clazz = voClass.newInstance();
				convertResultMap(orig, clazz);
			} catch (InstantiationException | IllegalAccessException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return clazz;
	}

	/**
	 * List&lt;ResultMap&gt;를 다른 객체리스트로 변환
	 * @param mapList ResultMap List
	 * @param voClass 변환할 객체
	 * @param <T>
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> List<T> convertResultMapList(List<ResultMap> mapList, Class<?> voClass) {
		T clazz;
		List<T> dataList = null;
		List<T> voList = new ArrayList<>();

		if (mapList != null) {
			for (ResultMap rm : mapList) {
				try {
					clazz = ((Class<T>) voClass).newInstance();
					convertResultMap(rm, clazz);
					voList.add(clazz);
				} catch (Exception e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
			if (mapList instanceof PageList) {
				dataList = new PageList<>(voList, ((PageList) mapList).getPaginator());
			} else {
				dataList = voList;
			}
		}
		return dataList;
	}

	public static String convertQueryUserVariable(String query) {
		String userId = "";
		String deptId = "";
		Pattern pattern = null;
		Matcher matcher = null;

		if(QuadMaxUserDetailsHelper.isAuthenticated().booleanValue()) {
			UserVO userVO = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();

			if (userVO != null) {
				userId = userVO.getEmpId();
				deptId = userVO.getDeptId();
			}
		}

		String queryCopy = query;
		pattern = Pattern.compile("%%USER_ID%%");
		matcher = pattern.matcher(queryCopy);
		queryCopy = matcher.replaceAll(userId);

		pattern = Pattern.compile("%%DEPT_ID%%");
		matcher = pattern.matcher(queryCopy);
		queryCopy = matcher.replaceAll(deptId);

		return queryCopy;
	}

	/**
	 * 여러 파일을 하나로 합친다.
	 * @param destination
	 * @param sources
	 * @throws IOException
	 */
	public static void joinFiles(File destination, File[] sources) throws IOException{
		
		if (sources != null) {
			try (
					FileOutputStream foutput = new FileOutputStream(destination,true);
					OutputStream output= new FileOutputStream(destination,true);
					){
				File reSource = null;
				for (File source : sources){
					if (source != null) {
						reSource = Util.renameFilterFile(source);
						if (reSource != null) {
							appendFIle(output, reSource);
						}
					}
				}
				foutput.flush();
				output.flush();
			}
		}
	}

	public static void appendFIle(OutputStream output, File source) throws IOException{
		try (
			FileInputStream finput = new FileInputStream(source);
			InputStream input = new BufferedInputStream(finput);	
		){
			
			IOUtils.copy(input, output);
		}
	}

	public static String methodName(String name) {
		String methodName = StringUtils.capitalize(name);

		if (Character.isUpperCase(methodName.charAt(1)) && Character.isUpperCase(methodName.charAt(0))){
			methodName = StringUtils.uncapitalize(methodName);
		}
		return methodName;
	}

	/**
	 *
	 * get  local ip
	 * @return
	 */
	public static String getLocalIp(){
		String ip = "";
		try{
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while( networkInterfaces.hasMoreElements()){
				NetworkInterface networkInterface = networkInterfaces.nextElement();
				Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
				while(inetAddresses.hasMoreElements()){
					InetAddress inetAddress = inetAddresses.nextElement();
					if(null!=inetAddress.getHostAddress() && inetAddress.getHostAddress().indexOf(".")!=-1){
						byte[] address = inetAddress.getAddress();
						if(address[0]!=127)  {
							ip = inetAddress.getHostAddress();
							break;
						}
					}
				}
				if(ip.length() > 0){
					break;
				}
			}
		} catch (SocketException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return ip;
	}
	
	public static String maskingId(String str) {
		if (str != null && !str.equals("")) {
			return str.replaceAll(".{1}.{1}.{1}$", "***");
		} else {
			return str;
		}
	}
	
	public static String maskingEmail(String str) {
		if (str != null && !str.equals("") && str.contains("@")) {
			String[] strArr = str.split("@");
			return strArr[0].replaceAll(".{1}.{1}.{1}$", "***") + "@" + strArr[1];
		} else {
			return str;
		}
	}
	
	public static String maskingName(String str) {
		if (str != null && !str.equals("")) {
			return str.replaceAll(".{1}$", "*");
		} else {
			return str;
		}
	}
	
	public static String maskingPhone(String str) {
		return str.replaceAll("([0-9]{2,3})(\\-?)([0-9]{3,4})(\\-?)([0-9]{4})", "$1$2****$4$5");
	}
	
	public static String maskingAddr(String str) {
		return str;
	}

	public static String maskingIp(String str) {
		if (str != null && !str.equals("")) {
			if (str.contains(".")) {
				return str.replaceAll("(\\d+)[.](\\d+)[.](\\d+)[.](\\d+)", "$1.$2.***.$3");
			} else {
				return str.replaceAll("([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4}):([\\d\\w]{1,4})", "$1:$2:$3:$4:$5:$6:***:$8");
			}
		} else {
			return str;
		}
	}
	
	public static String replacePath(String path) {
		if (path == null) {
			return null;
		}
		return path.replace("..", "")
				   .replace("&", "")
				   .replace("\"", "")
				   .replace("\'", "")
				   .replaceAll("(\\r\\n|\\n\\r)", "")
				   .replaceAll("(\\n)", "")
				   .replaceAll("(\\r)", "")
				   .replaceAll("(\\s)", "")
				   .replaceAll("(?i)null", "")
				   ;
	}
	public static String replaceFilename(String filename) {
		if (filename == null) {
			return null;
		}
		return filename.replaceAll("\\s", "")
				.replaceAll("[\\(\\)\\[\\]\\{\\}]", "")
				.replaceAll("./", "")
				.replaceAll("(\r\n)", "")
				.replaceAll("(\n\r)", "")
				.replaceAll("(\n)", "")
				.replaceAll("(\r)", "")
				.replaceAll("(?i)null", "")
				.replace("..", "")
				.replace("/", "")
				.replace("\\", "")
				.replace("&", "")
				;
	}
	
	public static int toInt(Object value) {
		int retVal = 0;
		if (value instanceof BigDecimal) {
			retVal = ((BigDecimal) value).intValue();
		} else if (value instanceof String){
			retVal = Integer.parseInt((String) value);
		} else {
			retVal = (int) value;
		}
		return retVal;
	}
	
	public static String printValue(ResultMap map, String key, String defVal) {
		String str = null;

		if (map == null)
			return defVal;

		Object obj = map.get(key);

		if (obj == null)
			return defVal;

		if (obj instanceof String) {
			str = String.valueOf(obj);
		} else if (obj instanceof Integer) {
			str = String.format("%,d", (Integer) obj);
		} else if (obj instanceof Long) {
			str = String.format("%,d", (Long) obj);
		} else if (obj instanceof BigDecimal) {
			str = String.format("%,d", ((BigDecimal) obj).longValue());
		} else if (obj instanceof Float) {
			str = String.format("%,.2f", (Float) obj);
		} else if (obj instanceof Double) {
			str = String.format("%,.2f", (Double) obj);
		} else {
			str = String.valueOf(obj);
		}

		return str;
	}
	
	public static String printValue2(Map<String, Object> map, String key, String defVal) {
		String str = null;
		
		if (map == null)
			return defVal;
		
		Object obj = map.get(key);
		
		if (obj == null)
			return defVal;
		
		if (obj instanceof String) {
			str = String.valueOf(obj);
		} else if (obj instanceof Integer) {
			str = String.format("%,d", (Integer) obj);
		} else if (obj instanceof Long) {
			str = String.format("%,d", (Long) obj);
		} else if (obj instanceof BigDecimal) {
			str = String.format("%,d", ((BigDecimal) obj).longValue());
		} else if (obj instanceof Float) {
			str = String.format("%,.2f", (Float) obj);
		} else if (obj instanceof Double) {
			str = String.format("%,.2f", (Double) obj);
		} else {
			str = String.valueOf(obj);
		}
		
		return str;
	}

	public static String printValue(ResultMap map, String key) {
		return printValue(map, key, "없음");
	}
	
	public static String printValue2(Map<String, Object> map, String key) {
		return printValue2(map, key, "없음");
	}

}

/*
 * Copyright (c) 2011 EASYCORESOLUTIONS
 * All right reserved.
 *
 * This software is the proprietary information of EASYCORESOLUTIONS
 *
 * Revision History
 *
 * Util
 *
 * Version     Date      Author              Description
 * =======  ==========  ========  ==========================================
 *
 */
package com.ecs.global.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.ecs.global.constant.Const;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;

public class Util {
	
	private Util() {
	    throw new IllegalStateException("Utility class");
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);
	
	/** The encoding type. */
	@Value("#{application['application.system.encodingType']}")
	private static String encodingType;

	/** The request string encoding type. */
	@Value("#{application['application.system.reqStrEnctype']}")
	private static String reqStrEnctype;

	/** The request string decoding type. */
	@Value("#{application['application.system.reqStrDectype']}")
	private static String reqStrDectype;
	
	
	public static String getAttribute(HttpServletRequest request, String attribute) {
		return request.getAttribute(attribute) == null ? "" : (String) request.getAttribute(attribute);
	}

	public static String getAttribute(HttpServletRequest request, String attribute, String defstr) {
		return request.getAttribute(attribute) == null ? defstr : (String) request.getAttribute(attribute);
	}

	/**
	 * HttpServletRequest 에서 특정 파라미터(strParameterName)의 데이터를 읽어와 String 형으로
	 * 리턴한다. 기본값이 NULL일 경우 ""을 리턴한다.
	 *
	 * @param request
	 * @param strParameterName
	 * @param strDefault
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 */
	public static String getString(HttpServletRequest request, String strParameterName, String strDefault) throws UnsupportedEncodingException {
		String strTemp = "";

		String str = Util.xssFilter(request.getParameter(strParameterName));
		if (encodingType != null && encodingType.equals("1")) {
			if (str != null) {
				strTemp = Util.trim(new String(str.getBytes(reqStrEnctype), reqStrDectype));
			}
		} else {
			if (str != null) {
				strTemp = Util.trim(Util.xssFilter(request.getParameter(strParameterName)));
			}
		}

		if (strTemp == null || "".equals(strTemp)) {
			strTemp = strDefault == null ? "" : strDefault;
		}

		return strTemp;
	}

	/**
	 * HttpServletRequest 에서 특정 컬럼(strColumnName)의 데이터를 읽어와 String 형으로 리턴 데이터값이
	 * NULL일 경우 공백("")값을 리턴
	 *
	 * @param request
	 * @param strParameterName 읽어올 컬럼명
	 * @return 컬럼의 데이터
	 * @throws UnsupportedEncodingException
	 */
	public static String getString(HttpServletRequest request, String strParameterName) throws UnsupportedEncodingException {
		return getString(request, strParameterName, null);
	}

	/**
	 * getString로 변경된 Encoding Type을 원래대로 변경
	 * @throws UnsupportedEncodingException
	 */
	public static String setString(HttpServletRequest request, String strParameterName) throws UnsupportedEncodingException {
		return setString(request, strParameterName, null);
	}

	/**
	 * getString로 변경된 Encoding Type을 원래대로 변경
	 * @throws UnsupportedEncodingException
	 */
	public static String setString(HttpServletRequest request, String strParameterName, String strDefault) throws UnsupportedEncodingException {
		String strTemp = strDefault == null ? "" : strDefault;

		String str = Util.xssFilter(request.getParameter(strParameterName));
		if (encodingType != null && encodingType.equals("1")) {
			if (str != null) {
				strTemp = Util.trim(new String(str.getBytes(reqStrDectype), reqStrEnctype));
			}
		} else {
			if (str != null) {
				strTemp = Util.trim(str);
			}
		}
		return strTemp;
	}

	/**
	 * HttpServletRequest 에서 특정 컬럼(strColumnName)의 데이터를 읽어와 String 형으로 리턴 데이터값이
	 * NULL일 경우 디폴트값을 리턴
	 *
	 * @param request
	 * @param strParameterName 읽어올 컬럼명
	 * @return 컬럼의 데이터
	 * @throws UnsupportedEncodingException
	 */
	public static String[] getStringArr(HttpServletRequest request, String strParameterName) throws UnsupportedEncodingException {
		return getStringArr(request, strParameterName, null);
	}

	/**
	 * getStringArr로 변경된 Encoding Type을 원래대로 변경
	 * @throws UnsupportedEncodingException
	 */
	public static String[] setStringArr(HttpServletRequest request, String strParameterName) throws UnsupportedEncodingException {
		return setStringArr(request, strParameterName, null);
	}

	/**
	 * HttpServletRequest 에서 특정 컬럼(strColumnName)의 데이터를 읽어와 String 형으로 리턴 데이터값이
	 * NULL일 경우 디폴트값을 리턴
	 *
	 * @param request
	 * @param strParameterName 읽어올 컬럼명
	 * @param strDefault 디폴트값...
	 * @return 컬럼의 데이터
	 * @throws UnsupportedEncodingException
	 */
	public static String[] getStringArr(HttpServletRequest request, String strParameterName, String strDefault) throws UnsupportedEncodingException {
		String[] arrTemp = null;

		if (encodingType != null && encodingType.equals("1")) {

			arrTemp = request.getParameterValues(strParameterName);
			if (arrTemp != null) {
				for (int i = 0; i < arrTemp.length; i++) {
					if (arrTemp[i] != null) {
						arrTemp[i] = new String(arrTemp[i].getBytes(reqStrEnctype), reqStrDectype);
					} else {
						arrTemp[i] = strDefault;
					}
				}
			}
		} else {
			arrTemp = request.getParameterValues(strParameterName);
		}

		return arrTemp;
	}

	/**
	 * getStringArr로 변경된 Encoding Type을 원래대로 변경
	 * @throws UnsupportedEncodingException
	 */
	public static String[] setStringArr(HttpServletRequest request, String strParameterName, String strDefault) throws UnsupportedEncodingException {
		String[] arrTemp = null;

		if (encodingType != null && encodingType.equals("1")) {

			arrTemp = request.getParameterValues(strParameterName);

			if (arrTemp != null) {
				for (int i = 0; i < arrTemp.length; i++) {
					if (arrTemp[i] != null) {
						arrTemp[i] = Util.trim(new String(arrTemp[i].getBytes(reqStrDectype), reqStrEnctype));
					} else {
						arrTemp[i] = strDefault;
					}
				}
			}
		} else {
			arrTemp = request.getParameterValues(strParameterName);
		}

		return arrTemp;
	}

	public static String isNull(String str, String setstr) {
		return isNull(str, setstr, null);
	}
	
	public static boolean isNullObj(Object obj) {
		return obj == null ? true : false;
	}

	/**
	 * String 이 null 일경우 바꿀 대치할 문자열
	 *
	 * @param str 검사할 String
	 * @param setstr str 이 null 일 경우 리턴할 문자열
	 * @return 리턴 String
	 */
	public static String isNull(String str, String setstr, String setstr2) {
		String result = null;

		if (str == null) {
			result = setstr;
		} else {
			if (setstr2 == null) {
				result = str;
			} else {
				result = setstr2;
			}
		}
		return result;
	}

	/**
	 * 현재 날짜를 가져오는 함수
	 *
	 * @return yyyy/mm/dd 형식
	 */
	public static String getFormattedThisDateSlash() {
		Calendar now = Calendar.getInstance();
		int iYear = now.get(Calendar.YEAR);
		int iMonth = now.get(Calendar.MONTH) + 1;
		int iDay = now.get(Calendar.DATE);

		return iYear + "/"
				+ fixTextSize(String.valueOf(iMonth), '0', 2) + "/"
				+ fixTextSize(String.valueOf(iDay), '0', 2);

	}

	/**
	 * 현재 날짜을 가져오는 함수
	 *
	 * @return yyyymmdd 형식
	 */
	public static String getFormattedThisDate() {
		Calendar now = Calendar.getInstance();
		int iYear = now.get(Calendar.YEAR);
		int iMonth = now.get(Calendar.MONTH) + 1;
		int iDay = now.get(Calendar.DATE);

		return iYear
				+ fixTextSize(String.valueOf(iMonth), '0', 2)
				+ fixTextSize(String.valueOf(iDay), '0', 2);

	}

	/**
	 * 현재 날짜를 가져오는 함수
	 *
	 * @return yyyy/mm/dd 형식
	 */
	public static String getFormattedThisDate(String flag) {
		StringBuilder date = new StringBuilder();
		Calendar now = Calendar.getInstance();

		if (flag.equalsIgnoreCase("YEAR")) {
			date.append(String.valueOf(now.get(Calendar.YEAR)));

		} else if (flag.equalsIgnoreCase("MONTH")) {
			date.append(fixTextSize(String.valueOf(now.get(Calendar.MONTH) + 1), '0', 2));

		} else if (flag.equalsIgnoreCase("DAY")) {
			date.append(fixTextSize(String.valueOf(now.get(Calendar.DATE)), '0', 2));

		} else {
			date.append(String.valueOf(now.get(Calendar.YEAR)));
			date.append(fixTextSize(String.valueOf(now.get(Calendar.MONTH) + 1), '0', 2));
			date.append(fixTextSize(String.valueOf(now.get(Calendar.DATE)), '0', 2));
		}

		return date.toString();
	}

	/**
	 * 현재년도의 month월 day일을 구함.
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getFormattedThisDate(int year, int month, int day) {
		Calendar now = Calendar.getInstance();
		int iYear = now.get(Calendar.YEAR) + year;
		int iMonth = month;
		int iDay = day;

		return iYear
				+ fixTextSize(String.valueOf(iMonth), '0', 2)
				+ fixTextSize(String.valueOf(iDay), '0', 2);
	}

	/**
	 * 현재년도의 month월 day일을 구함.
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getFormattedThisDate(int month, int day) {
		Calendar now = Calendar.getInstance();
		int iYear = now.get(Calendar.YEAR);
		int iMonth = month;
		int iDay = day;

		return iYear
				+ fixTextSize(String.valueOf(iMonth), '0', 2)
				+ fixTextSize(String.valueOf(iDay), '0', 2);
	}

	/**
	 * 현재 날짜에서 일을 위 아래 day 만큼 이동
	 * @param day
	 * @return
	 */
	public static String getFormattedThisDate(int day) {
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, day);

		return sd.format(cal.getTime());
	}

	/**
	 * 시간 문자열을 날짜만을 가져와서 웹상에서 표시할 형식으로 변환<BR>
	 * 길이가 맞지 않거나 null일때 원래 스트링을 리턴함<BR>
	 * Ex) "20011014123020" --> "2001/10/14"
	 *
	 * @param strDate 시간 문자열
	 * @return 날짜만의 문자열
	 */
	public static String getFormattedDate(String strDate) {
		if (strDate == null || !(strDate.length() == 8 || strDate.length() == 14)) {
			return strDate;
		}

		// 날짜를 가져온다.
		StringBuilder sbufferFormattedDay = new StringBuilder("");

		sbufferFormattedDay.append(strDate.substring(0, 4))
				.append("/");

		String strMonth = strDate.substring(4, 6);
		sbufferFormattedDay.append(strMonth)
				.append("/");

		String strDay = strDate.substring(6, 8);
		sbufferFormattedDay.append(strDay);

		return sbufferFormattedDay.toString();
	}

	/**
	 * 시간 문자열을 날짜만을 가져와서 웹상에서 표시할 형식으로 변환<BR>
	 * 길이가 맞지 않거나 null일때 원래 스트링을 리턴함<BR>
	 * strFormat = "/" Ex) "20011014123020" --> "2001/10/14"
	 *
	 * @param strDate 시간 문자열
	 * @return 날짜만의 문자열
	 */
	public static String getFormattedDate(String strDate, String strFormat) {
		if (strDate == null || strDate.equals("")
				|| !(strDate.length() == 8 || strDate.length() == 14)) {
			return strDate;
		}

		// 날짜를 가져온다.
		StringBuilder sbufferFormattedDay = new StringBuilder("");

		sbufferFormattedDay.append(strDate.substring(0, 4)).append(strFormat);

		String strMonth = strDate.substring(4, 6);
		sbufferFormattedDay.append(strMonth).append(strFormat);

		String strDay = strDate.substring(6, 8);
		sbufferFormattedDay.append(strDay);

		return sbufferFormattedDay.toString();
	}

	/**
	 * 날짜 문자열을 포맷형식에 맞춰 리턴
	 * @param strDate
	 * @param strFormat
	 * @return
	 */
	public static String getFormattedDate(Date strDate, String strFormat) {
		if (strDate == null) {
			return "";
		}

		// 날짜를 가져온다.
		StringBuilder sbufferFormattedDay = new StringBuilder("");

		sbufferFormattedDay.append(strDate.toString().substring(0, 4)).append(strFormat);

		String strMonth = strDate.toString().substring(5, 7);
		sbufferFormattedDay.append(strMonth).append(strFormat);

		String strDay = strDate.toString().substring(8, 10);
		sbufferFormattedDay.append(strDay);

		return sbufferFormattedDay.toString();
	}

	/**
	 * 날짜 문자열을 한글포맷형식에 맞춰 리턴
	 * @param strTime
	 * @return
	 */
	public static String getFormattedTimeHangul(String strTime) {
		StringBuilder str = new StringBuilder();

		if (!strTime.equals("")) {
			str.append(strTime.substring(0, 2));
			str.append("시");
			str.append(strTime.substring(2, 4));
			str.append("분");
			str.append(strTime.substring(4, 6));
			str.append("초");
		}

		return str.toString();
	}

	/**
	 * 시간 문자열을 포맷형식에 맞춰 리턴
	 * @param strTime
	 * @param strFormat
	 * @return
	 */
	public static String getFormattedTime(String strTime, String strFormat) {
		StringBuilder str = new StringBuilder();

		if (strTime != null && !strTime.equals("")) {
			str.append(strTime.substring(0, 2));
			str.append(strFormat);
			str.append(strTime.substring(2, 4));
			if (strTime.length() > 4) {
				str.append(strFormat);
				str.append(strTime.substring(4, 6));
			}
		}

		return str.toString();
	}

	/**
	 * 파일명을 확장자와 분리해 확장자 리턴
	 *
	 * @param fileName
	 * @return
	 */
	public static String getFileExt(String fileName) {
		if (fileName == null) {
			return null;
		}
		if (fileName.lastIndexOf('.') == -1) {
			return "";
		}
		return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
	}
	
	/**
	 * 파일명을 확장자와 분리해 파일명 리턴
	 *
	 * @param fileName
	 * @return
	 */
	public static String getFileNameWithoutExt(String fileName) {
		if (fileName != null) {
			if (fileName.lastIndexOf('.') == -1) {
				return fileName;
			}
			return fileName.substring(0, fileName.lastIndexOf('.'));
		} else {
			return null;
		}
	}

	/**
	 * 파일확장자 체크
	 *
	 * @param filename
	 * @return
	 */
	public static boolean fileExtCheck(String filename, String[] extArr) {
		boolean check = false;
		
		String[] extArrCopy = extArr == null ? new String[] {} :  extArr.clone();

		if (filename == null || "".equals(filename)) {
			return check;
			
		} else {

			String ext = isNull(getFileExt(filename), "").toLowerCase();

			if (extArr == null || extArr.length == 0) {
				extArrCopy = Const.FILE_EXT_ALLOW.toArray(new String[0]);
			}
			for (String strExt : extArrCopy) {
				if (ext.endsWith(strExt)) {
					check = true;
					break;
				}
			}
		}

		return check;
	}

	public static File renameFilterFile(File srcFile) {
		String srcFileName = null;
		String srcFilePath = null;
		String reFileName = null;
		String fileExt = null;

		File reSrcFile = null;

		if (srcFile != null) {
			srcFileName = srcFile.getName();
			srcFilePath = srcFile.getAbsolutePath();
			if (srcFilePath != null && srcFileName != null) {

				fileExt = Util.isNull(Util.getFileExt(srcFileName), "");
				if (Util.fileExtCheck(fileExt, new String[] {})) {
					reFileName = Util.isNull(Util.getFileNameWithoutExt(srcFileName), "");

					if (reFileName.equals(srcFileName)) {
						reSrcFile = srcFile;
					} else {
						reSrcFile = new File(CmmnUtil.replacePath(srcFilePath), CmmnUtil.replaceFilename(reFileName) + ("".equals(fileExt) ? "" : "." + fileExt));

						if (!srcFile.renameTo(reSrcFile)) {
							LOGGER.error("파일명 변경에 실패하였습니다.");
						}
					}
				}
			}
		}
		return reSrcFile;
	}

	/**
	 * 문자열을 특정 크기로 만듬, 만약 남는 공간이 있으면 왼쪽에서부터 특정문자(cSpace)를 채움. null이 입력되더라도 크기 만큼
	 * 특정문자를 채움
	 *
	 * @param strText String 문자열
	 * @param cSpace char 빈공란에 채울 특정문자
	 * @param iTotalSize int 특정 크기
	 * @return 변경된 문자열
	 */
	public static String fixTextSize(final String strText, final char cSpace, final int iTotalSize) {

		String result;

		if (strText.length() < iTotalSize) {
			// 문자열의 크기가 특정크기보다 작을 때는 특정문자로 채움
			char[] carraySpace = new char[iTotalSize - strText.length()];
			Arrays.fill(carraySpace, cSpace);
			String strSpace = new String(carraySpace);

			result = strSpace + strText;
		} else {
			// 문자열의 크기가 특정크기보다 클때는 앞쪽의 문자열 잘라냄
			result = strText.substring(strText.length() - iTotalSize,
					strText.length());
		}
		return result;
	}

	/**
	 * 주어진 날짜의 iNextMonth 일 후의 날짜를 리턴한다.<BR>
	 * strCurDate : 20020915 iNextMonth : 3 -> return : 20021215 윤년 계산됨
	 * @param strCurDate
	 * @param iNextMonth
	 * @return
	 */
	public static String afterNMonth(String strCurDate, int iNextMonth) {
		// 현재 날짜의 Calendar 객체를 가져온다.
		Calendar calCurDate = Util.convertStringToCal(strCurDate);

		// N개월을 더한다.
		int iMonth = calCurDate.get(Calendar.MONTH) + iNextMonth;

		// 달을 설정한다.
		calCurDate.set(Calendar.MONTH, iMonth);
		String strResult = Util.convertCalToString(calCurDate);

		if (strCurDate.length() == 8) {
			strResult = strResult.substring(0, 8);
		}

		return strResult;
	}

	/**
	 * 주어진 날짜의 3개월 후의 날짜를 리턴한다.<BR>
	 * "20011031" -> "20020131"<BR>
	 * "20011031103040" -> "20020131103040"<BR>
	 * 윤년 계산됨
	 *
	 * @param strCurDate 날짜
	 * @return 3개월 후의 날짜
	 */
	public static String beforeNMonth(String strCurDate, int iNextMonth) {

		// 현재 날짜의 Calendar 객체를 가져온다.
		Calendar calCurDate = Util.convertStringToCal(strCurDate);

		// N개월을 뺀다
		int iMonth = calCurDate.get(Calendar.MONTH) - iNextMonth;

		// 달을 다시 설정한다.
		calCurDate.set(Calendar.MONTH, iMonth);

		String strResult = Util.convertCalToString(calCurDate);
		if (strCurDate.length() == 8) {
			strResult = strResult.substring(0, 8);
		}

		return strResult;
	}

	/**
	 * 문자열을 날짜로 변환<BR>
	 * 길이가 맞지 않거나 null일때 오늘을 리턴함<BR>
	 *
	 * @param strDay String 변환 시킬 문자열
	 * @return 변환된 문자열
	 */
	public static Calendar convertStringToCal(String strDay) {
		if (strDay == null || !(strDay.length() == 8 || strDay.length() == 14)) {
			return new GregorianCalendar();
		}

		// 날짜를 가져온다.
		int iYear = Integer.parseInt(strDay.substring(0, 4));
		int iMonth = Integer.parseInt(strDay.substring(4, 6)) - 1; // 달은 0부터 시작
																	// 한다.
		int iDay = Integer.parseInt(strDay.substring(6, 8));

		// 시간을 가져온다.
		int iHour = 0;
		int iMin = 0;
		int iSecond = 0;
		if (strDay.length() == 14) {
			iHour = Integer.parseInt(strDay.substring(8, 10));
			iMin = Integer.parseInt(strDay.substring(10, 12));
			iSecond = Integer.parseInt(strDay.substring(12, 14));
		}

		// Calendar객체를 생성하고 날짜와 시간을 설정한다.
		Calendar calDay = new GregorianCalendar();
		calDay.set(iYear, iMonth, iDay, iHour, iMin, iSecond);

		return calDay;
	}

	/**
	 * 날짜를 데이터베이스에 입력하기위한 스트링으로 변환하는 함수(년도월일시분초까지)<BR>
	 * 날짜가 null일 경우 공백문자열("")을 리턴시킴
	 *
	 * @param calDay Calendar 변환 시킬 날짜
	 * @return 변환된 문자열
	 */
	public static String convertCalToString(Calendar calDay) {
		if (calDay == null) {
			return "";
		}

		StringBuilder sbufferDay = new StringBuilder("");

		// 날짜를 가져옴
		String strYear = Util.fixTextSize(Integer.toString(calDay.get(Calendar.YEAR)), '0', 4);
		String strMonth = Util.fixTextSize(Integer.toString(calDay.get(Calendar.MONTH) + 1), '0', 2);
		String strDay = Util.fixTextSize(Integer.toString(calDay.get(Calendar.DAY_OF_MONTH)), '0', 2);

		sbufferDay.append(strYear).append(strMonth).append(strDay);

		// 시간을 가져옴
		String strHour = Util.fixTextSize(Integer.toString(calDay.get(Calendar.HOUR_OF_DAY)), '0', 2);
		String strMin = Util.fixTextSize(Integer.toString(calDay.get(Calendar.MINUTE)), '0', 2);
		String strSecond = Util.fixTextSize(Integer.toString(calDay.get(Calendar.SECOND)), '0', 2);

		sbufferDay.append(strHour).append(strMin).append(strSecond);

		return sbufferDay.toString();
	}

	/**
	 * 스트링을 trim함. null일 경우 null을 리턴
	 *
	 * @param strTarget String trim을 할 문자열
	 * @return trim이 된 문자열
	 */
	public static String trim(String strTarget) {
		String strTargetCopy = strTarget;
		if (strTargetCopy != null) {
			strTargetCopy = strTargetCopy.trim();
		}

		return strTargetCopy;
	}

	public static String getMonthActualMaxiumDay(String year, String month, String day) {
		String result = null;

		if (year != null && !year.equals("")
				&& month != null && !month.equals("")) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
			int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

			result = Integer.toString(days);
		}

		return result;
	}
	
	public static String readJSONStringFromRequestBody(HttpServletRequest request) throws IOException {
		BufferedReader reader = null;
		StringBuilder json = new StringBuilder();

		try {
			reader = request.getReader();
			
			String line = reader.readLine();
			while (line != null) {
				json.append(new String(line.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8));
				line = reader.readLine();
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return json.toString();
	}
	
	/**
	 * 문자열을 제한된 글자수로 짜르고, "..."을 추가시킴<BR>
	 * ex) 문자열: "ABCDEFGH", 제한 길이: 3 --&gt "ABC..."
	 *
	 * @param strTarget 문자열
	 * @param iLimit 제한된 글자수
	 * @return 잘려진 문자열
	 */
	public static String stringCut(String strTarget, int iLimit) {
		return stringCut(strTarget, iLimit, "");

	}

	/**
	 * 문자열 자르기
	 * @param strTarget
	 * @param iLimit
	 * @param strReplace
	 * @return
	 */
	public static String stringCut(String strTarget, int iLimit, String strReplace) {
		String replaceString = null;

		if (strTarget == null || strTarget.length() <= iLimit) {
			return strTarget;
		}

		StringBuilder sbufferTarget = new StringBuilder(strTarget);
		sbufferTarget.delete(iLimit, strTarget.length());

		if (strReplace != null) {
			replaceString = "...";
		} else {
			replaceString = null;
		}

		sbufferTarget.append(replaceString);
		return sbufferTarget.toString();
	}

	/**
	 * 숫자를 금액표시법으로 변경 ( 1234560 -> 1,234,560)
	 *
	 * @param iAmount int 숫자
	 * @return 천단위로 콤마가 들어간 문자열
	 */
	public static String putCommaByThousand(int iAmount) {
		DecimalFormat dfAmount = new DecimalFormat("###,###,###,###,###,###,###.####");
		return dfAmount.format(iAmount);
	}

	// -------------------4. 일반 날짜 관련 함수들 끝----------------------

	// -------------------5. 단위 표시 관련 함수들------------------------
	/**
	 * 숫자를 금액표시법으로 변경 ( 1234560 -> 1,234,560)
	 *
	 * @param lAmount long 숫자
	 * @return 천단위로 콤마가 들어간 문자열
	 */
	public static String putCommaByThousand(long lAmount) {
		DecimalFormat dfAmount = new DecimalFormat("###,###,###,###,###,###,###.####");
		return dfAmount.format(lAmount);
	}

	/**
	 * 숫자를 금액표시법으로 변경 ( 1234560 -> 1,234,560)
	 *
	 * @param dAmount long 숫자
	 * @return 천단위로 콤마가 들어간 문자열
	 */
	public static String putCommaByThousand(double dAmount) {
		DecimalFormat dfAmount = new DecimalFormat("###,###,###,###,###,###,###.####");
		return dfAmount.format(dAmount);
	}

	/**
	 * 숫자를 금액표시법으로 변경 ( 1234560 -> 1,234,560)<BR>
	 * 숫자의 문자열이 null이면 공백문자열("")을 리턴
	 *
	 * @param strAmount String 숫자의 문자열
	 * @return 천단위로 콤마가 들어간 문자열
	 */
	public static String putCommaByThousand(String strAmount) {
		if (strAmount == null || strAmount.equals("")) {
			return "";
		}

		if (strAmount.indexOf('.') != -1) {
			double dAmount = Double.parseDouble(strAmount);
			return Util.putCommaByThousand(dAmount);
		} else {
			long lAmount = Long.parseLong(strAmount);
			return Util.putCommaByThousand(lAmount);
		}
	}

	/**
	 * 주어진 문자열이 숫자인지 검사
	 *
	 * @param str 검사할 문자열
	 * @return true 숫자, false 숫자 아님..
	 */
	public static boolean isNumber(String str) {
		boolean result = false;
		// 나머지 글자가 알파벳인지 숫자인지를 알아본다.

		if (str != null) {
			for (int iIndex = 0; iIndex < str.length(); iIndex++) {
				if (Character.isDigit(str.charAt(iIndex))) {
					result = true; // 숫자임.
				}
			}
		}

		return result;
	}

	/**
	 * 문자열 치환
	 * @param strSource
	 * @param strSearch
	 * @param strReplace
	 * @return
	 */
	public static String replaceAll(String strSource, String strSearch, String strReplace) {
		String strSourceCopy = strSource;
		if (strSourceCopy == null) {
			return strSourceCopy;
		}

		int iStart = 0;
		iStart = strSourceCopy.indexOf(strSearch, iStart);

		while (iStart > -1) {
			strSourceCopy = strSourceCopy.substring(0, iStart) + strReplace + strSourceCopy.substring(iStart+strSearch.length());

			iStart += strReplace.length();

			iStart = strSourceCopy.indexOf(strSearch, iStart);
		}

		return strSourceCopy;
	}

	/**
	 * 문자열 치환(대소문자 구분없이)
	 * @param strSource
	 * @param strSearch
	 * @param strReplace
	 * @return
	 */
	public static String replaceAllIgnoreCase(String strSource, String strSearch, String strReplace) {
		String strSourceCopy = strSource;
		if (strSourceCopy == null) {
			return strSourceCopy;
		}

		String strSourceUpper = strSourceCopy.toUpperCase();
		String strSearchUpper = strSearch.toUpperCase();
		int iStart = 0;

		iStart = strSourceUpper.indexOf(strSearchUpper, iStart);

		while (iStart > -1) {
			strSourceCopy = strSourceCopy.substring(0, iStart) + strReplace + strSourceCopy.substring(iStart+strSearch.length());

			iStart += strReplace.length();
			strSourceUpper = strSourceCopy.toUpperCase();

			iStart = strSourceUpper.indexOf(strSearchUpper, iStart);
		}

		return strSourceCopy;
	}

	/**
	 * Xss Filter
	 * @param data
	 * @return
	 */
	public static String xssFilter(String data) {
		if (data == null) {
			return null;
		}
		String parsedData = StringEscapeUtils.escapeHtml4(data);
		if (parsedData != null) {
			parsedData = parsedData.replace("'", "&#39;");
		}

		return parsedData;
	}

	/**
	 * File Name Filter
	 * @param data
	 * @return
	 */
	public static String getFileNameFilter(String data) {
		if (data == null) {
			return null;
		}
		return data.replaceAll("\\s", "")
					.replaceAll("[\\(\\)\\[\\]\\{\\}]", "")
					.replace("/", "")
					.replace("\\\\", "")
					.replaceAll("./", "")
					.replace("&", "")
					.replaceAll("[.]", "")
					.replaceAll("(\r\n)", "")
					.replaceAll("(\n\r)", "")
					.replaceAll("(\n)", "")
					.replaceAll("(\r)", "")
					.replaceAll("(?i)null", "")
					;
	}
	
	/**
	 * File Name Filter
	 * @param data
	 * @return
	 */
	public static String getFileNameFilterWithExt(String data) {
		if (data == null) {
			return null;
		}
		String filename = StringUtils.defaultIfEmpty(getFileNameWithoutExt(data), "");
		String fileExt = StringUtils.defaultIfEmpty(getFileExt(data), "");
		String reFilename = filename.replaceAll("\\s", "")
									.replaceAll("[\\(\\)\\[\\]\\{\\}]", "")
									.replace("/", "")
									.replace("\\\\", "")
									.replaceAll("./", "")
									.replace("&", "")
									.replaceAll("[.]", "")
									.replaceAll("[\r\n]", "");
		return reFilename + (fileExt.length() > 0 ? "." + fileExt : "");
	}

	/**
	 * File Name Filter
	 * @param file
	 * @return
	 */
	public static String getFileNameFilterByFile(File file) {
		String parsedData = null;
		if (file != null) {
			parsedData = Util.getFileNameFilter(Util.getFileNameWithoutExt(file.getName()));
		}
		return parsedData;
	}

	/**
	 * get QuadMax Lang
	 * @return
	 */
	public static String getQuadMaxLanguage() {
		String quadMaxlanguage = null;
		
		/*
		 * Locale.KOREA : ko_KR
		 * Locale.KOREAN : ko
		 * Locale.JAPAN : ja_JP
		 * Locale.JAPANESE : ja
		 * Locale.US : en_US
		 * Locale.ENGLISH : en
		 * Locale.CHINA : zh_CN
		 * Locale.CHINESE : zh
		 */
		Locale locale = LocaleContextHolder.getLocale();
		
		if (locale == null) {
			LocaleContextHolder.setLocale(Locale.KOREA);
		}
		if (locale != null) {
			quadMaxlanguage = locale.toString();

			if (!(String.valueOf(Locale.US)).equals(quadMaxlanguage)	// en_US
					&& !(String.valueOf(Locale.JAPAN)).equals(quadMaxlanguage)	// ja_JP
					&& !(String.valueOf(Locale.CHINA)).equals(quadMaxlanguage)	// zh_CN
					&& !"vi_VN".equals(quadMaxlanguage)) {	// vi_VN
				quadMaxlanguage = String.valueOf(Locale.KOREA);	// ko_KR
			}
		}
		
		if (quadMaxlanguage == null || "".equals(quadMaxlanguage)) {
			quadMaxlanguage = String.valueOf(Locale.KOREA);	// ko_KR
		}

		return quadMaxlanguage;
	}
	
	/**
	 * get QuadMax DW Lang
	 * @return
	 */
	public static String getQuadMaxDwLanguage() {
		String quadMaxDwlanguage = null;
		String quadMaxlanguage = getQuadMaxLanguage();

		// 프로젝트에서 DW 언어코드가 다를 경우 해당 부분 커스트마이징
		
		quadMaxDwlanguage = quadMaxlanguage;

		return quadMaxDwlanguage;
	}
	
	public static String changeStr(String str, String enCode) throws UnsupportedEncodingException {
		return new String(str.getBytes(enCode), StandardCharsets.ISO_8859_1);
	}
	
	public static String changeStrOfEuckr(String str) throws UnsupportedEncodingException {
		return changeStr(str, "EUC-KR");
	}

	public static String changeStrOfUtf8(String str) throws UnsupportedEncodingException {
		return changeStr(str, "UTF-8");
	}
	
	public static String changeStrToEuckr(String str) throws UnsupportedEncodingException {
		return new String(str.getBytes(StandardCharsets.ISO_8859_1), "EUC-KR");
	}
	
	public static boolean checkBytes(String txt, int limitBytes) throws IOException {
		
		if (txt == null || "".equals(txt)) { return false; }
		
        byte [] euckr = txt.getBytes("EUC-KR");
 
        int txtByte = euckr.length;
        return txtByte <= limitBytes;
	}

}

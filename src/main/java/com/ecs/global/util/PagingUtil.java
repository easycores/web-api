package com.ecs.global.util;

import java.util.HashMap;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

/**
 * Created by kimsangyong on 2016. 5. 3..
 */
public class PagingUtil {
	
	private PagingUtil() {
	    throw new IllegalStateException("Utility class");
	}

	/**
	 * Universe Warp 에서 페이징 처리를 위한 RowBounds 반환
	 * @param paramMap
	 * @return
	 */
	public static PageBounds getSimplePageBounds(Map<String, Object> paramMap) {
		String strPageSize = (String) paramMap.get("page_size");
		String strPageNum = (String) paramMap.get("page_number");
		int page = 1;
		int limit = 10;
		int offset = 0;	// page * limit

		// 페이징 처리 데이터 개수 (ex. 10, 25, 50, 100)
		if (strPageSize != null && !"".equals(strPageSize)) {
			limit = Integer.parseInt(strPageSize);
		}
		// 페이지 number
		if (strPageNum != null && !"".equals(strPageNum)) {
			page = Integer.parseInt(strPageNum);
		}

		// 데이터 구할 rownum 처리 시작 번호
		if (limit > 0) {
			offset = limit * (page - 1);
		}

		if (limit == 0) {
			return null;
		} else {
			return new PageBounds(offset / limit + 1, limit);
		}
	}


	/**
	 * DataTable 에서 페이징 처리를 위한 RowBounds 반환
	 * @param paramMap
	 * @return
	 */
	/*public static PageBounds getPageBoundsDataTable(Map<String, Object> paramMap) {
		int offset = 0;
		int limit = 0;
		if (paramMap.get("iDisplayStart") != null) {
			offset = Integer.parseInt((String) paramMap.get("iDisplayStart"));
		}
		if (paramMap.get("iDisplayLength") != null) {
			limit = Integer.parseInt((String) paramMap.get("iDisplayLength"));
		}

		int sortingCols = 0;
		if (paramMap.get("iSortingCols") != null) {
			sortingCols = Integer.parseInt((String) paramMap.get("iSortingCols"));
		}
		StringBuilder sortSb = new StringBuilder();
		for (int i = 0; i < sortingCols; i++) {
			String sortCol = (String) paramMap.get("iSortCol_" + i);
			String sortDir = (String) paramMap.get("sSortDir_" + i);

			if (paramMap.get("mDataProp_" + sortCol) != null) {
				if (i > 0) sortSb.append(",");

				sortSb.append(camelToDbStyle((String) paramMap.get("mDataProp_" + sortCol)) + "." + sortDir);
			}
		}

		if (limit == 0) {
			return null;
		}
		if (!"".equals(sortSb.toString())) {
			return new PageBounds(offset / limit + 1, limit, Order.formString(sortSb.toString()));
		} else {
			return new PageBounds(offset / limit + 1, limit);
		}
	}*/

	/**
	 * jqGrid 에서 페이징 처리를 위한 RowBounds 반환
	 * @param paramMap
	 * @return
	 */
	/*public static PageBounds getPageBoundsJqGrid(Map<String, Object> paramMap) {
		String strRowNum = (String) paramMap.get("rowNum");
		String strPage = (String) paramMap.get("page");
		int page = 1;
		int limit = 10;
		int offset = 0;	// page * limit

		// 페이징 처리 데이터 개수 (10, 25, 50, 100)
		if (strRowNum != null && !"".equals(strRowNum)) {
			limit = Integer.parseInt(strRowNum);
		}
		// 페이지 number
		if (strPage != null && !"".equals(strPage)) {
			page = Integer.parseInt(strPage);
		}

		// 데이터 구할 rownum 처리 시작 번호
		if (limit > 0) {
			offset = limit * (page - 1);
		}

		if (limit == 0) {
			return null;
		} else {
			return new PageBounds(offset / limit + 1, limit);
		}
	}*/

	/**
	 * camelStyle -> CAMEL_STYLE
	 * @param str
	 * @return
	 */
	public static String camelToDbStyle(String str) {
		String regex = "([a-z])([A-Z])";
		String replacement = "$1_$2";
		String value = "";

		value = str.replaceAll(regex, replacement).toUpperCase();

		return value;
	}
	
	/**
	 * pageList get total count
	 * @param pageList
	 * @return
	 */
	public static int getPageTotalCount(@SuppressWarnings("rawtypes") PageList pageList) {
		int totalCount = 0;
		if (pageList != null && pageList.getPaginator() != null) {
			totalCount = pageList.getPaginator().getTotalCount();
		}
		return totalCount;
	}

	/**
	 * pageList get total pages
	 * @param pageList
	 * @return
	 */
	public static int getTotalPages(@SuppressWarnings("rawtypes") PageList pageList) {
		int pages = 0;
		if (pageList != null && pageList.getPaginator() != null) {
			pages = pageList.getPaginator().getTotalPages();
		}
		return pages;
	}

	/**
	 * pageList get page number ( current context )
	 * @param pageList
	 * @return
	 */
	public static int getPageNumber(@SuppressWarnings("rawtypes") PageList pageList) {
		int pageNumber = 0;
		if (pageList != null && pageList.getPaginator() != null) {
			pageNumber = pageList.getPaginator().getPage();
		}
		return pageNumber;
	}

	/**
	 * pageList get page size
	 * @param pageList
	 * @return
	 */
	public static int getPageSize(@SuppressWarnings("rawtypes") PageList pageList) {
		int size = 0;
		if (pageList != null && pageList.getPaginator() != null) {
			size = pageList.getPaginator().getLimit();
		}
		return size;
	}

	/**
	 * 화면에서 필요한 page 정보를 Map 형태로 제공
	 * @param pageList
	 * @return
	 */
	public static Map<String, Object> getSimplePageConditions(@SuppressWarnings("rawtypes") PageList pageList) {
		Map<String, Object> map = new HashMap<>();
		map.put("page_number", getPageNumber(pageList));
		map.put("page_size", getPageSize(pageList));
		map.put("total_page", getTotalPages(pageList));
		map.put("count",getPageTotalCount(pageList));
		return map;
	}

	public static boolean isPageBoundRequired(Map<String, Object> paramMap){
		String strPageSize = (String) paramMap.get("page_size");
		String strPageNum = (String) paramMap.get("page_number");
		if (strPageSize != null && !"".equals(strPageSize)
				&& strPageNum != null && !"".equals(strPageNum)) {
			return true;
		}
		return false;
	}
}

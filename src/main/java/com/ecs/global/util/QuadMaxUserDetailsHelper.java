package com.ecs.global.util;

import java.util.List;

import com.ecs.global.security.service.QuadMaxUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 인증된 유저의 LoginVO, 권한, 인증 여부를 확인 할 수있는 서비스 클래스
 *
 * context-quadmaxdetailshelper.xml 설정 생략 ---------------------------------
 * 	setQuadMaxUserDetailsService 를 public 메소드로 변경 및 @Autowired 를 사용하여
 *	추가적인 빈설정 파일 없이 quadMaxUserDetailsService 를 주입받는다.
 * --------------------------------------------------------------------------
* */
@Component
public class QuadMaxUserDetailsHelper {
	static QuadMaxUserDetailsService quadMaxUserDetailsService;

	public QuadMaxUserDetailsService getQuadMaxUserDetailsService() {
		return quadMaxUserDetailsService;
	}

	@Autowired
	public void setQuadMaxUserDetailsService(QuadMaxUserDetailsService quadMaxUserDetailsService) {
		QuadMaxUserDetailsHelper.quadMaxUserDetailsService = quadMaxUserDetailsService;
	}

	/**
	 * 인증된 사용자객체를 VO형식으로 가져온다.
	 * @return Object - 사용자 ValueObject
	 */
	public static Object getAuthenticatedUser() {
		return quadMaxUserDetailsService.getAuthenticatedUser();
	}

	/**
	 * 인증된 사용자의 권한 정보를 가져온다.
	 * 
	 * @return List - 사용자 권한정보 목록
	 */
	public static List<String> getAuthorities() {
		return quadMaxUserDetailsService.getAuthorities();
	}
	
	/**
	 * 인증된 사용자 여부를 체크한다.
	 * @return Boolean - 인증된 사용자 여부(TRUE / FALSE)	
	 */
	public static Boolean isAuthenticated() {
		System.out.println("quadMaxUserDetailsService : " + quadMaxUserDetailsService.hashCode());
		return quadMaxUserDetailsService.isAuthenticated();
	}

}

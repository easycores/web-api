package com.ecs.global.util;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.ecs.global.exception.CommonRuntimeException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;


import cm.util.AES256Cipher;

public class SecureUtil {
	
	private SecureUtil() {
		throw new IllegalStateException("Utility class");
	}

	private static final String AES_ALGORITHM = "AES256";
	private static final String SHA_ALGORITHM = "SHA-512";
	private static final String BC_ALGORITHM = "PBEWITHSHA256AND256BITAES-CBC-BC";
	private static final String BC_ALGORITHM_PROVIDER = "BC";
	private static final String BC_ALGORITHM_KEY = "SMART_QUADMAX";

	public static String encryptString(String str) {
		return encryptString(str, AES_ALGORITHM, null);
	}

	public static String encryptString(String str, String algorithm, String keyName) {
		String encryptedStr = null;

		try {
			if (AES_ALGORITHM.equals(algorithm)) {
				encryptedStr = AES256Cipher.AES_Encode(str, true);
			} else if (BC_ALGORITHM.equals(algorithm)) {
				encryptedStr = encryptBcAlgorithm(str, keyName);
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
				| IOException e) {
			throw new CommonRuntimeException(e);
		}

		return encryptedStr;
	}

	public static String decryptString(String str) {
		return decryptString(str, AES_ALGORITHM, null);
	}

	public static String decryptString(String str, String algorithm, String keyName) {
		String decryptedStr = null;
		
		try {
			if (AES_ALGORITHM.equals(algorithm)) {
				decryptedStr = AES256Cipher.AES_Decode(str, true);
			} else if (BC_ALGORITHM.equals(algorithm)) {
				decryptedStr = decryptBcAlgorithm(str, keyName);
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
				| IOException e) {
			throw new CommonRuntimeException(e);
		}
		
		return decryptedStr;
	}

	public static String encryptPassword(String plainPassword) {
		return encryptPassword(plainPassword, AES_ALGORITHM);
	}

	public static String encryptPassword(String plainPassword, String algorithm) {
		String encryptedStr = null;
		
		try {
			if (AES_ALGORITHM.equals(algorithm)) {
				encryptedStr = AES256Cipher.AES_Encode(plainPassword, true);
			} else if(SHA_ALGORITHM.equals(algorithm)) {
				encryptedStr = AES256Cipher.SHA_Encode(plainPassword, true);
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
				| IOException e) {
			throw new CommonRuntimeException(e);
		}

		return encryptedStr;
	}

	public static boolean isCheckPassword(String plainPassword, String encryptedPassword) {
		return isCheckPassword(plainPassword, encryptedPassword, AES_ALGORITHM);
	}

	public static boolean isCheckPassword(String plainPassword, String encryptedPassword, String algorithm) {
		boolean isCheckPassword = false;
		
		try {
			if (AES_ALGORITHM.equals(algorithm)) {
				isCheckPassword = AES256Cipher.AES_Encode(plainPassword, true).equals(encryptedPassword);
			} else if(SHA_ALGORITHM.equals(algorithm)) {
				isCheckPassword = AES256Cipher.SHA_Encode(plainPassword, true).equals(encryptedPassword);
			}
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
				| IOException e) {
			throw new CommonRuntimeException(e);
		}

		return isCheckPassword;
	}
	

	public static String encryptBcAlgorithm(String str, String keyName) {
		System.out.println("keyName : " + keyName + " / " + StringUtil.isEmpty(keyName));
		if (StringUtil.isEmpty(keyName)) {
			keyName = BC_ALGORITHM_KEY;
		}
		BouncyCastleProvider bcp = new BouncyCastleProvider();
    	EnvironmentStringPBEConfig esc = new EnvironmentStringPBEConfig(); 
    	esc.setProvider(bcp);
		esc.setProviderName(BC_ALGORITHM_PROVIDER);
		esc.setAlgorithm(BC_ALGORITHM);
//		esc.setPassword(decryptString(keyName));
		esc.setPassword(keyName);

		StandardPBEStringEncryptor spe = new StandardPBEStringEncryptor();
	    spe.setConfig(esc);
	    
	    return spe.encrypt(str);
	}
	
	public static String decryptBcAlgorithm(String str, String keyName) {
		System.out.println("keyName : " + keyName);
		if (StringUtil.isEmpty(keyName)) {
			keyName = BC_ALGORITHM_KEY;
		}
		BouncyCastleProvider bcp = new BouncyCastleProvider();
    	EnvironmentStringPBEConfig esc = new EnvironmentStringPBEConfig(); 
    	esc.setProvider(bcp);
		esc.setProviderName(BC_ALGORITHM_PROVIDER);
		esc.setAlgorithm(BC_ALGORITHM);
//		esc.setPassword(decryptString(keyName));
		esc.setPassword(keyName);

		StandardPBEStringEncryptor spe = new StandardPBEStringEncryptor();
	    spe.setConfig(esc);
	    
	    return spe.decrypt(str);
	}

}

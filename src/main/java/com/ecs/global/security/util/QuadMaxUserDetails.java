package com.ecs.global.security.util;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class QuadMaxUserDetails extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5575152416450996953L;
	
	private Object quadMaxVO;

	/**
	 * User 클래스의 생성자 Override
	 * @param username 사용자계정
	 * @param password 사용자 패스워드
	 * @param enabled 사용자계정 사용여부
	 * @param accountNonExpired
	 * @param credentialsNonExpired
	 * @param accountNonLocked
	 * @param authorities
	 * @param quadMaxVO 사용자 VO객체
	 * @throws IllegalArgumentException
	 */
	public QuadMaxUserDetails(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
			Object quadMaxVO) throws IllegalArgumentException {

		super(username, password, enabled, accountNonExpired,
			credentialsNonExpired, accountNonLocked, authorities);

		this.quadMaxVO = quadMaxVO;
	}

	/**
	 * QuadMaxUserDetails 생성자
	 * @param username
	 * @param password
	 * @param enabled
	 * @param quadMaxVO
	 * @throws IllegalArgumentException
	 */
	public QuadMaxUserDetails(String username, String password, boolean enabled, Object quadMaxVO) throws IllegalArgumentException {
		
		this(username, password, enabled, true, true, true, 
				Arrays.asList(new SimpleGrantedAuthority("HOLDER")), quadMaxVO);
	}

	public Object getQuadMaxUserVO() {
		return quadMaxVO;
	}

	public void setQuadMaxUserVO(Object quadMaxVO) {
		this.quadMaxVO = quadMaxVO;
	}
}

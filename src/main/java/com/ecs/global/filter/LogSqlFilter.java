package com.ecs.global.filter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;


public class LogSqlFilter extends Filter<ILoggingEvent> {
	
	Level level;

    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }
		
        if (event.getLevel().isGreaterOrEqual(level)) {
        	if (event.getMessage().contains("/*==IGNORE_LOG==*/")
                    // TODO ?
        			|| event.getLoggerName().startsWith("com.ecs.domain.rest.quadmax.ecscontainer.campaign.dao.CampaignDAO.insertCampSccssCndtnPrdt")
        			|| event.getLoggerName().startsWith("com.ecs.domain.rest.quadmax.ecscontainer.campaign.dao.RoiDAO.insertRoi")
        			|| event.getLoggerName().startsWith("com.ecs.domain.rest.quadmax.offer.dao.OfferDAO.updateAppCouponEventInfo")
        			|| event.getLoggerName().startsWith("com.ecs.domain.rest.cms.targetlist.dao.TargetListDAO.insertAllTargetListCust")
    				|| event.getLoggerName().startsWith("com.ecs.domain.rest.quadmax.manage.campaign.targettheme.dao.TargetThemeManageDAO.insertAllTargetThemeList")) {
                return FilterReply.DENY;
            }else{
                return FilterReply.ACCEPT;
            }
        } else {
            return FilterReply.DENY;
        }
    }

    public void setLevel(String level) {
        this.level = Level.toLevel(level);
    }

    @Override
    public void start() {
        if (this.level != null) {
            super.start();
        }
    }
}
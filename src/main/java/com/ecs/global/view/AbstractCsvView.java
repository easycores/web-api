/**
 *
 */
package com.ecs.global.view;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecs.global.exception.CommonRuntimeException;
import com.ecs.global.exception.QuadMaxException;
import org.springframework.web.servlet.view.AbstractView;

/**
 * @author hhcho
 * 2017-07-06
 *
 */
public abstract class AbstractCsvView extends AbstractView  {

	@Override
	protected boolean generatesDownloadContent() {
		return true;
	}

	/**
	 * Renders the Csv view, given the specified model.
	 * @throws QuadMaxException 
	 */
	@Override
	protected final void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, CommonRuntimeException, QuadMaxException {
		ByteArrayOutputStream baos = null;
		try {
			baos = createTemporaryOutputStream();
			buildCsvDocument(model, request, response);
			writeToResponse(response, baos);

			if (baos != null)	baos.flush();
		} finally {
			if (baos != null) {
				baos.close();
			}
		}
	}

	protected abstract void buildCsvDocument(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, CommonRuntimeException, QuadMaxException;

}
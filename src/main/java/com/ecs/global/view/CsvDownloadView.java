package com.ecs.global.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecs.global.exception.CommonRuntimeException;
import com.ecs.global.exception.QuadMaxException;
import com.ecs.global.util.CmmnUtil;
import com.ecs.global.util.DateUtil;
import com.ecs.global.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author hhcho
 * CSV 다운로드용 뷰.
 */
public class CsvDownloadView extends AbstractCsvView {

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CsvDownloadView.class);
	
	/** documentRoot path. */
	@Value("#{application['application.cms.documentRoot']}")
	private String documentRoot;

	@SuppressWarnings("rawtypes")
	protected void buildCsvDocument(Map modelMap, HttpServletRequest req, HttpServletResponse res) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, CommonRuntimeException, QuadMaxException {

		File downFile = null;
		byte[] outputByte = null;
		int read = 0;

		List recTitles = (List) modelMap.get("recTitles");
		List recNames = (List) modelMap.get("recNames");
		List records = (List) modelMap.get("records");

		String physicalFileName = CmmnUtil.getRealPath("downloadFile_" + DateUtil.getCurTimeMili() + ".csv");

		String logicalFileName = (String) modelMap.get("fileName");
		if (logicalFileName == null || "".equals(logicalFileName)) {
			logicalFileName = physicalFileName;
		} else {
			logicalFileName += "_" + DateUtil.getCurTimeMili();
		}
		logicalFileName = Util.getFileNameFilter(Util.getFileNameWithoutExt(logicalFileName));
		logicalFileName = logicalFileName.replaceAll("[\\s]", "");
		logicalFileName = URLEncoder.encode(logicalFileName, "UTF-8").replace("+", "%20");
		logicalFileName = new StringBuilder().append(logicalFileName).append(".csv").toString();

		try {

			// file DRM encode 처리
			downFile = createDataTable(physicalFileName, recTitles, recNames, records);

			// RESPONSE header 세팅
			res.setContentType(req.getContentType());
			setHeader(res, logicalFileName);

			try (FileInputStream fis = new FileInputStream(downFile);
					OutputStream out = res.getOutputStream();) {
				
				outputByte = new byte[4096];
				read = fis.read(outputByte);
				while (read != -1) {
					out.write(outputByte, 0, read);
					read = fis.read(outputByte);
				}
				
				if (out != null)	out.flush();
			}


		} finally {
			if (downFile != null && downFile.exists()) {
				Files.delete(downFile.toPath());
			}
		}

	}

	private void setHeader(HttpServletResponse res, String fileName) {
		res.setCharacterEncoding("UTF-8");
		res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		res.setHeader("Pragma", "no-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setHeader("Content-Transfer-Encoding", "binary");
	}

	@SuppressWarnings("rawtypes")
	private File createDataTable(String fileName, List recTitles, List recNames, List records) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, CommonRuntimeException {
		File file = null;
		File md = null;
		
		StringBuilder writeStr = null;
		List methodNames = null;
		String filePath = documentRoot + File.separator + "Temp";

		if (records == null) {
			return null;
		}

		writeStr = new StringBuilder();
		int cnt = 0;
		if (recTitles != null) {
			for (Object obj : recTitles) {
				if (cnt > 0) {
					writeStr.append(",");
				}
				writeStr.append(String.valueOf(obj));
				cnt++;
			}
		}

		if (writeStr != null && writeStr.length() > 0) {
			writeStr.append("\r\n");
		}

		for (Object obj : records) {
			if (obj instanceof Map) {
				setTableRowDataMap(writeStr, (Map) obj, recNames);

			} else {
				if (methodNames == null) {
					methodNames = getMethodNames(recNames);
				}
				setTableRowDataVO(writeStr, obj, methodNames);

			}
			writeStr.append("\r\n");
		}

		md = new File(CmmnUtil.replacePath(filePath));
		if (!md.setExecutable(false, true)) {
			LOGGER.error("setExecutable 실패");
		}
		if (!md.setReadable(true)) {
			LOGGER.error("setReadable 실패");
		}
		if (!md.setWritable(false, true)) {
			LOGGER.error("setWritable 실패");
		}
		if (!md.isDirectory() && !md.mkdirs()) {
			LOGGER.error("디렉토리생성에 실패했습니다.");
		}

		file = new File(CmmnUtil.replacePath(filePath), CmmnUtil.replaceFilename(fileName));
		try (FileOutputStream inStream = new FileOutputStream(file)) {
			
			inStream.write(0xEF);
			inStream.write(0xBB);
			inStream.write(0xBF);
			
			try (OutputStreamWriter outWriter = new OutputStreamWriter(inStream, StandardCharsets.UTF_8);
					BufferedWriter bwriter = new BufferedWriter(outWriter)) {
				
				bwriter.write(writeStr.toString());
				
				bwriter.flush();
				outWriter.flush();
				inStream.flush();
			}
		}


		return file;
	}

	@SuppressWarnings("rawtypes")
	private void setTableRowDataMap(StringBuilder str, Map map, List recNames) {
		int col = 0;
		for (Object key : recNames) {
			if (col > 0) {
				str.append(",");
			}
			str.append(String.valueOf(map.get(key)));

			col++;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List getMethodNames(List recNames) {
		List list = new ArrayList();
		for (String recName : (List<String>) recNames) {
			list.add("get" + (recName.substring(0, 1)).toUpperCase() + recName.substring(1));
		}
		return list;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setTableRowDataVO(StringBuilder str, Object vo, List recNames) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, CommonRuntimeException {
		Class cls = vo.getClass();
		Object val;

		int cnt = 0;
		for (String methodName : (List<String>) recNames) {
			try {
				Method method = cls.getMethod(methodName);
				val = method.invoke(vo);
			} catch (SecurityException|IllegalArgumentException e) {
				throw new CommonRuntimeException(e);
			}

			if (cnt > 0) {
				str.append(",");
			}
			if (val == null) {
				str.append("");
			} else {
				str.append(String.valueOf(val));
			}

			cnt++;
		}
	}

}

package com.ecs.global.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class QuadMaxJsonView extends MappingJackson2JsonView {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuadMaxJsonView.class);
	
	private final ObjectMapper objectMapper;
	private final ObjectMapper excludeObjectMapper;
	
	@SuppressWarnings("rawtypes")
	private List excludePaths;
	
	public QuadMaxJsonView() {
		objectMapper = new ObjectMapper();
		objectMapper.getFactory().setCharacterEscapes(new HTMLCharacterEscapes());
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		excludeObjectMapper = new ObjectMapper();
	}
	
	@SuppressWarnings("rawtypes")
	public void setExcludePaths(List excludePaths) {
		this.excludePaths = new ArrayList<>();
		this.excludePaths.addAll(excludePaths);
	}
	
	
	@SuppressWarnings("rawtypes")
	public List getExcludePaths() {
		return this.excludePaths;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		boolean isExclude = false;
		LOGGER.debug("getExcludePaths : {}", getExcludePaths());
		if (getExcludePaths() != null) {
			String path = request.getRequestURI().substring(request.getContextPath().length());
			PathMatcher pathMatcher = new AntPathMatcher();
			
			for (Object excludePath : getExcludePaths()) {
				String excludePathPattern = String.valueOf(excludePath);
				
				if (excludePathPattern != null && pathMatcher.match(excludePathPattern, path)) {
					isExclude = true;
					break;
				}
			}
		}
		
		LOGGER.debug("jsonView xss escape exclude : {}", isExclude);
		
		if (!isExclude) {
			super.setObjectMapper(this.objectMapper);
		} else {
			super.setObjectMapper(this.excludeObjectMapper);
		}
		
		super.renderMergedOutputModel(model, request, response);
		
	}
	
}

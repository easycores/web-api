package com.ecs.global.handler;

import com.ecs.infrastructure.model.EcsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


@Slf4j
@RestControllerAdvice
public class RestApiControllerAdvice<T> implements ResponseBodyAdvice<T> {

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        // 컨트롤러 클래스를 특정하는 방법 returnType.getExecutable().getDeclaringClass();
        return true; // true => beforeBodyWrite 호출
    }

    @Override
    public T beforeBodyWrite(@Nullable T body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if(body.getClass() == EcsResponse.class) {
            EcsResponse ecsResponse = (EcsResponse) body;
            HttpStatus status = ecsResponse.getStatus();
            response.setStatusCode(status);
            ecsResponse.setStatus(null);
        }
        return body;
    }

    @ExceptionHandler({NoHandlerFoundException.class})
    public EcsResponse handleNoHandlerFoundException(NoHandlerFoundException ex){
        return new EcsResponse(ex,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({Exception.class})
    public EcsResponse handleException(Exception ex) {
        return new EcsResponse(ex,HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

package com.ecs.global.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import com.ecs.global.i18n.QuadMaxMessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author HHCHO
 *
 */
public class Const {
	
	private Const() {
	    throw new IllegalStateException("Utility class");
	}
	
	/*********************************************************************************************
	 * 시스템 설정 정보
	 *********************************************************************************************/
	/**
	 * 세션키 - 유저정보.
	 */
	static WebApplicationContext webAppContext = ContextLoader.getCurrentWebApplicationContext();
	private static QuadMaxMessageSource messageSource = (QuadMaxMessageSource) webAppContext.getBean("quadMaxMessageSource");
	
	static Locale locale = LocaleContextHolder.getLocale();

	/**
	 * 쿼리서버 URL.
	 */
	public static final String REST_API_URL_ROOT = "/cms/api/v1";

	/**
	 * 쿼리서버 URL.
	 */
	public static final String DQS_URL = "DQS_URL";
	/**
	 * 유저정보.
	 */
	public static final String SESSION_USER				= "SESSION_USER";
	/**
	 * 유저정보.
	 */
	public static final String USER_VO					= "userVO";
	

	/**
	 * READONLY MODE
	 */
	public static final String READONLY_MODE_INSERT		= "I";		// 신규등록
	public static final String READONLY_MODE_UPDATE		= "U";		// 수정
	public static final String READONLY_MODE_READONLY	= "R";		// 읽기모드
	
	/**
	 * 캠페인 필드 타입
	 */

	public static final String[] EBM_FIELDTYPE = { "Expression", "String", "int", "long", "double", "boolean", "DateTime" };
	public static final String[] EBM_MESSAGEFIELDTYPE = {"String", "long", "double"};

	/**
	 * EBM 이벤트타입
	 */
	protected static final String[] EBM_EVENTTYPE = {"TCP", "EAI"};
	/**
	 * EBM 전문타입
	 */
	public static final String[] EBM_MESSAGETYPE = {};


	/**
	 * EBM 컨셉타입
	 */
	protected static final String[] EBM_CONCEPTTYPE = {"DB", "Memory"};

	/**
	 * EBM 중복시 옵션
	 */
	public static final String EBM_DUP_OPTION_EX = "E";
	public static final String EBM_DUP_OPTION_RUN = "R";
	public static final String EBM_DUP_OPTION_SELECT = "S";

	public static final String EBM_DUP_OPTION_EX_NM = messageSource.getMessageArgs("WD1368", null, locale);
	public static final String EBM_DUP_OPTION_RUN_NM = messageSource.getMessageArgs("WD1369", null, locale);
	public static final String EBM_DUP_OPTION_SELECT_NM = messageSource.getMessageArgs("WD1370", null, locale);

	/**
	 * Popup캠페인 전문 조건정보
	 */
	public static final List<String> EBM_CON_RELATION = Collections.unmodifiableList(Arrays.asList("AND", "OR"));
	protected static final String[] EBM_BLOCKOPEN = {"Y", "N"};
	protected static final String[] EBM_BLOCKCLOSE = {"Y", "N"};
	public static final List<String> EBM_FIELDOPERATOR = Collections.unmodifiableList(Arrays.asList("==", "!=", "<", ">", "<=", ">="));
	
	/**
	 * 화면 MODE
	 */
	public static final String MODE_INIT				= "INIT";		// 초기화
	public static final String MODE_SUB_MAIN			= "MAIN";		// 메인
	public static final String MODE_SUB_MENU			= "SUBMENU";	// 서브메뉴
	public static final String MODE_SUB_META			= "META";		// DBMS 메타관리
	public static final String MODE_SUB_TABLE			= "TABLE";		// DBMS 메타관리 - 테이블
	public static final String MODE_SUB_TABLELIST		= "TABLELIST";	// DBMS 메타관리 - 테이블리스트
	public static final String MODE_SUB_DBMSTABLE		= "DBMSTABLE";	// DBMS 메타관리 - DBMS 테이블
	public static final String MODE_SUB_TABLETYPE		= "TABLETYPE";	// DBMS 메타관리 - 테이블 유형
	public static final String MODE_SUB_DBMSTREE		= "DBMSTREE";	// DBMS 메타관리 - DBMS 유형 트리
	public static final String MODE_SUB_METATABLE		= "METATABLE";	// DBMS 메타관리 - 메타 테이블
	public static final String MODE_SUB_DBMSID			= "DBMSID";		// DBMS 메타관리 - DBMS ID
	public static final String MODE_SUB_SCHEMA			= "SCHEMA";		// DBMS 메타관리 - 스키마
	
	/**
	 * ACTION MODE
	 */
	public static final String MODE_SELECT				= "SELECT";				// 선택
	public static final String MODE_SEARCH				= "SEARCH";				// 조회
//	public static final String MODE_READ				= "READ";				// 조회(읽기)
//	public static final String MODE_RESET				= "RESET";				// 초기화
//	public static final String MODE_REFRESH				= "REFRESH";			// 새로고침
	public static final String MODE_INSERT				= "INSERT";				// 등록
//	public static final String MODE_INSERT_QUERY		= "INSERT_QUERY";		// 등록
	public static final String MODE_UPDATE				= "UPDATE";				// 수정
//	public static final String MODE_RUN_UPDATE			= "RUNUPDATE";			// 실행 중 수정
	public static final String MODE_DELETE				= "DELETE";				// 삭제
	public static final String MODE_SAVE				= "SAVE";				// 저장
	public static final String MODE_NONE				= "NONE";				// 처리없음
//	public static final String MODE_FILEDELETE			= "FILEDELETE";			// 파일삭제
//	public static final String MODE_LISTDELETE			= "LISTDELETE";			// 리스트삭제
//	public static final String MODE_IMAGEDELETE			= "IMAGEDELETE";		// 이미지삭제
//	public static final String MODE_UPLOAD				= "UPLOAD";				// 업로드
//	public static final String MODE_DATACLEAR			= "DATACLEAR";			// 데이터초기화
//	public static final String MODE_COPY				= "COPY";				// 복사
//	public static final String MODE_BACKUPIMAGEDELETE	= "BACKUPIMAGEDELETE";	// 이미지삭제
//	public static final String MODE_TESTSEND			= "TESTSEND";			// 테스트발송
//	public static final String MODE_REMINDSEND			= "REMINDSEND";			// 리마인드발송
//	public static final String MODE_SUSPEND 			= "SUSPEND";			// 중지
//	public static final String MODE_CANCEL				= "CANCEL";				// 취소
//	public static final String MODE_APRV				= "APRV";				// 승인
	
	/*********************************************************************************************
	 * 캠페인 정보
	 *********************************************************************************************/
	/**
	 * 매스 캠페인 - CAMPAIGN NODE ID (고정)
	 */
	public static final String MASS_CAMP_NODE_ID		= "N000000000000000";
	
	/**
	 * 캠페인 상태 ( CMPMETA.T_CAMP_STATUS )
	 */
	public static final String CSTS_WRITE_K				= "001";	// 기획설계
	public static final String CSTS_APR_REQ_K			= "002";	// 승인요청(전자결재요청)
	public static final String CSTS_APR_EMP_REJ_K		= "004";	// 승인요청(전자결재반송)
	public static final String CSTS_CAN_K				= "005";	// 승인요청취소(전자결재취소-사용안함)
	public static final String CSTS_RUN_WAIT_K			= "006";	// 승인/실행대기
	public static final String CSTS_RUN_K				= "007";	// 실행
	public static final String CSTS_END_K				= "008";	// 완료
	public static final String CSTS_STOP_K				= "009";	// 조기종료
	public static final String CSTS_TEMP_K				= "010";	// 템플릿
	public static final String CSTS_APP_MAKE_K			= "061";	// 제작요청
	public static final String CSTS_APP_MAKE_REJ_K		= "063";	// 제작반려
	public static final String CSTS_APR_REJ_K			= "069";	// 승인반려(본사)
	
	/**
	 * 캠페인 웹/워크플로우 사용모드.
	 */
	public static final String CTYP_WEB 				= "WB";	// 웹
	public static final String CTYP_WORKFLOW 			= "WF";	// 워크플로우
	public static final String CTYP_WF 					= "WF";	// 고급 (워크플로우)
	public static final String CTYP_NM 					= "NM";	// 일반 (웹플로우)
	
	/**
	 * 캠페인 유형 코드.
	 */
	public static final String CAMP_TYPE_CD_OC			= "OC";	// 타겟(오퍼)
	public static final String CAMP_TYPE_CD_PC			= "PC";	// 홍보
	public static final String CAMP_TYPE_CD_MC			= "MC";	// 매스
	public static final String CAMP_TYPE_CD_IC			= "IC";	// 정보성
	
	/**
	 * 템플릿 캠페인 타겟팅 유형
	 */
	public static final String TARGETING_TYPE_SINGLE	= "UT";
	public static final String TARGETING_TYPE_MUTIPLE	= "MT";
	
	public static final String NODE_GP_TYPE_TARGETING	= "TARGETING";
	public static final String NODE_GP_TYPE_CUSTOMER	= "CUSTOMER";
	public static final String NODE_GP_TYPE_CHANNEL		= "CHANNEL";
	
	
	/*********************************************************************************************
	 * 스케줄(실행) 정보
	 *********************************************************************************************/
	/**
	 * 스케줄(JOB) 상태
	 */
	public static final String JOB_STATE_READY			= "1";	// 대기
	public static final String JOB_STATE_DOING			= "2";	// 진행중
	public static final String JOB_STATE_EXIT			= "3";	// 종료
	public static final String JOB_STATE_STOP			= "4";	// 일시정지
	public static final String JOB_STATE_ERROR			= "5";	// 에러
	public static final String JOB_STATE_RUNNING		= "6";	// 실행중
	
	/**
	 * 스케줄 실행 결과 상태
	 */
	public static final String SCH_STATUS_SUCCESS		= "S";	// 성공
	public static final String SCH_STATUS_FAIL			= "F";	// 실패
	public static final String SCH_STATUS_RUNNING		= "R";	// 실행중
	public static final String SCH_STATUS_EXIT			= "K";	// 강제종료
	public static final String SCH_STATUS_NONEXECUTE	= "N";	// 미실행

	/**
	 * 스케줄 실행 타입
	 */
	public static final String SCH_EXECUTE_TYPE_1		= "1";	// 즉시실행(2분후)
	public static final String SCH_EXECUTE_TYPE_2		= "2";	// 10분 뒤
	public static final String SCH_EXECUTE_TYPE_3		= "3";	// 1시간 뒤
	public static final String SCH_EXECUTE_TYPE_4		= "4";	// 날짜지정
	public static final String SCH_EXECUTE_TYPE_5		= "5";	// Workflow스케줄사용

	/**
	 * 스케줄 작업 구분
	 */
	public static final String SCH_RUN_TYPE_ONE			= "0";	// 한번
	public static final String SCH_RUN_TYPE_REPEAT		= "1";	// 되풀이
	
	
	/*********************************************************************************************
	 * 타겟팅 설정 정보
	 *********************************************************************************************/
	/**
	 * 타겟 템플릿 프롬프트 데이터 참조 타입
	 */
	public static final String TARGET_PROMPT_ML				= "ML";
	public static final String TARGET_PROMPT_DS				= "DS";
	public static final String TARGET_PROMPT_LI				= "LI";
	
	/**
	 * 비지니스 쿼리의 기본채널정보를 결정하는 프롬프트
	 */
	public static final String FIX_TARGET_PROMPT_KEYWORD_CHANNEL	= "@@사용채널@@";
	
	public static final String FIX_PROMPT_KEYWORD_MULTISTEPNODEID = "@@멀티스텝노드ID@@";
	public static final String FIX_PROMPT_KEYWORD_CAMPID = "@@캠페인ID@@";
	public static final String FIX_PROMPT_KEYWORD_RESPONSEFLAG = "@@실행방법@@";
	public static final String FIX_PROMPT_KEYWORD_CUSTOMER_ALU = "@@고객군연산@@";
	
	
	
	/*********************************************************************************************
	 * 채널 설정 정보
	 *********************************************************************************************/
	/**
	 *	채널 카테고리
	 */
	public static final String CHANNEL_TYPE_MAIL			= "001";	// MAIL
	public static final String CHANNEL_TYPE_TM 				= "002";	// TM
	public static final String CHANNEL_TYPE_DM 				= "003";	// DM
	public static final String CHANNEL_TYPE_SFA 			= "004";	// SFA
	public static final String CHANNEL_TYPE_SMS 			= "005";	// SMS
	public static final String CHANNEL_TYPE_HOMEPAGE 		= "006";	// HOMEPAGE
	public static final String CHANNEL_TYPE_POS 			= "007";	// POS
	public static final String CHANNEL_TYPE_LMS 			= "008";	// LMS
	public static final String CHANNEL_TYPE_MMS 			= "009";	// MMS
	public static final String CHANNEL_TYPE_LEGACY_SYSTEM 	= "010";	// 운영계
	public static final String CHANNEL_TYPE_ETC				= "011";	// 기타
	public static final String CHANNEL_TYPE_SURVEY			= "012";	// 설문
	public static final String CHANNEL_TYPE_MO				= "013";	// MO
	public static final String CHANNEL_TYPE_WEB_CHANNEL		= "014";	// 웹채널
	public static final String CHANNEL_TYPE_APP				= "015";	// APP
	public static final String CHANNEL_TYPE_NO				= "016";	// NO액션
	public static final String CHANNEL_TYPE_WEB_CONTENTS	= "017";	// Web컨텐츠
	public static final String CHANNEL_TYPE_MMS_COUPON		= "018";	// 바코드MMS
	public static final String CHANNEL_TYPE_KAKAO_NT		= "020";	// 카카오톡(알림톡)
	public static final String CHANNEL_TYPE_KAKAO_FT		= "023";	// 카카오톡(친구톡)
	public static final String CHANNEL_TYPE_CONTROL_GROUP	= "999";	// 대조군
//	public static final String CHANNEL_TYPE_CONTROL_GROUP	= "999";	// 앱푸시TEXT
//	public static final String CHANNEL_TYPE_CONTROL_GROUP	= "999";	// 앱푸시리치


	/*
	 * 채널 발송구분
	 */
	public static final String CHANNEL_SEND_TYPE_R = "R";	// 예약발송
	public static final String CHANNEL_SEND_TYPE_D = "D";	// 당일발송
	public static final String CHANNEL_SEND_TYPE_Y = "Y";	// 즉시발송

	/*********************************************************************************************
	 * 첨부파일 설정 정보
	 *********************************************************************************************/
	/**
	 * 첨부파일 모듈구분
	 */
	public static final String FILE_MODULE_CONTENTS 	= "CONTENTS";	// 채널컨텐츠
	public static final String FILE_MODULE_CAMPAIGN 	= "CAMPAIGN";	// 캠페인
	public static final String FILE_MODULE_NOTICE 		= "NOTICE";		// 공지사항

	/**
	 * 첨부파일 허용 확장자
	 */
	public static final List<String> FILE_EXT_ALLOW = Collections.unmodifiableList(Arrays.asList("txt", "csv", "xls", "xlsx", 
			"ppt", "pptx", "doc", "docx", "hwp", "pdf", 
			"png", "gif", "jpg", ""));

	
	/*********************************************************************************************
	 * DBMS 설정 정보
	 *********************************************************************************************/
	/**
	 * DB 종류 (유형)
	 */
	protected static final String[] DBMS_GUB_K = { 
			"001", "002", "003", "004", "005",
			"006", "007", "008", "009", "010",
			"011", "012"
		};
	protected static final String[] DBMS_GUB_V = {
			"MS-SQL", "ORACLE", "DB2", "Informix", "TeraData",
			"MySQL", "Sybase", "AS400", "SAS", "GreenPlum",
			"ADO"
		};
	
	
	/*********************************************************************************************
	 * 기타 설정 정보
	 *********************************************************************************************/
	/**
	 * 전자결재 승인 상태 코드
	 */
	public static final String APRV_STAT_X 				= "x";	// 요청-전자결재 저장 전
	public static final String APRV_STAT_IMSI 			= "0";	// 요청-전자결재 임시저장
	public static final String APRV_STAT_REQ 			= "1";	// 전자결재 결재중
	public static final String APRV_STAT_EMI_APP 		= "2";	// 전자결재 최종승인
	public static final String APRV_STAT_EMI_RETURN 	= "3";	// 전자결재 반려
	public static final String APRV_STAT_EMI_CANCEL		= "4";	// 전자결재 취소
	public static final String APRV_STAT_SELF 			= "5";	// 자가승인
	public static final String APRV_STAT_RETURN			= "9";  // 반려
	public static final String APRV_STAT_APP			= "7";  // 승인
	public static final String APRV_STAT_APP_REQ		= "61";	// 제작요청
	public static final String APRV_STAT_APP_COMP		= "62";	// 제작요청
	public static final String APRV_STAT_APP_RETURN		= "63";	// 제작요청
	
	/**
	 * 노드 위치 기본값
	 */
	public static final int NODE_LOCATION_DEF_TOP		= 110;	// 상단 위치
	public static final int NODE_LOCATION_DEF_LEFT		= 0;	// 왼쪽 위치
	public static final int NODE_LOCATION_DEF_BOTTOM	= 110;	// 하단 위치
	public static final int NODE_LOCATION_DEF_RIGHT		= 10;	// 하단 위치
	
	public static final String INTERSECTION 			= "교집합";
	public static final String OR 						= "차집합";
	public static final String UNION 					= "합집합";
	
	/*********************************************************************************************
	 * 이마트 추가 설정 정보
	 *********************************************************************************************/
	/**
	 * 본사/점포 계정 구분 코드
	 * ( LOOKUP_TYPE = 'CMP_VSBL_TYPE_CD' )
	 */
	public static final String STORE_CODE_HQ 			= "0000";
	
	public static final String DIV_CD_ALL 				= "01";
	public static final String DIV_CD_HQ 				= "02";
	public static final String DIV_CD_STORE 			= "03";
	
	/**
	 * 관계사 코드 ( 고정 )
	 */
	public static final String AFLCO_CD 				= "aflcoCd";
	public static final String AFLCO_CD_VALUE			= "001";
	
	/**
	 * 사용자 유형 
	 * ( T_EMP.TYPE_ID ) 
	 */
	public static final String EMP_TYPE_GENERAL 		= "001";
	public static final String EMP_TYPE_ADMIN 			= "999";
	
	public static final String SUCCESS = "success";
	public static final String JSON_VIEW = "jsonView";
	public static final String ERR_MSG = "errMsg";
}

package com.ecs.global.i18n;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

public class QuadMaxMessageSource extends ReloadableResourceBundleMessageSource implements MessageSource {
	
	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}

	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}

	public String getMessage(String code) {
		return getReloadableResourceBundleMessageSource().getMessage(code, null, LocaleContextHolder.getLocale());
	}
	
	public String getMessage(String code, Object arg) {
		return getReloadableResourceBundleMessageSource().getMessage(code, new Object[]{arg}, LocaleContextHolder.getLocale());
	}
	
	public String getMessage(String code, Object[] args) {
		return getReloadableResourceBundleMessageSource().getMessage(code, args, LocaleContextHolder.getLocale());
	}

	public String getMessageArgs(String code, Object[] args, Locale locale) {
		return getReloadableResourceBundleMessageSource().getMessage(code, args, locale);
	}
	
	public String getMessageArgs(String code, Object[] args, String defaultMessage, Locale locale) {
		return getReloadableResourceBundleMessageSource().getMessage(code, args, defaultMessage, locale);
	}
}

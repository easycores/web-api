package com.ecs.global.i18n;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.Properties;

public class QuadMaxReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(QuadMaxReloadableResourceBundleMessageSource.class);
	
	private static final String PROPERTIES_SUFFIX = ".properties";
	
	private final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

	@Override
	protected PropertiesHolder refreshProperties(String filename, PropertiesHolder propHolder) {
		if (filename.startsWith("classpath*:")) {
			return refreshClassPathProperties(filename, propHolder);
		} else {
			return super.refreshProperties(filename, propHolder);
		}
	}
	
	private PropertiesHolder refreshClassPathProperties(String filename, PropertiesHolder propHolder) {
		Properties properties = new Properties();
		long lastModified = -1;
		try {
			Resource[] resources = resolver.getResources(filename + PROPERTIES_SUFFIX);
			for (Resource resource : resources) {
			String sourcePath = resource.getURI().toString().replace(PROPERTIES_SUFFIX, "");
			PropertiesHolder holder = super.refreshProperties(sourcePath, propHolder);
			properties.putAll(holder.getProperties());
			if (lastModified < resource.lastModified())
				lastModified = resource.lastModified();
			}
		} catch (IOException ie) {
			LOGGER.error("IOException ", ie);
		}
		return new PropertiesHolder(properties, lastModified);
	}
}

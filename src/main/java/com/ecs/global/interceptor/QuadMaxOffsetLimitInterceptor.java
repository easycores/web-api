package com.ecs.global.interceptor;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.global.constant.Const;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
import com.ecs.global.util.Util;
import org.apache.ibatis.cache.Cache;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.github.miemiedev.mybatis.paginator.OffsetLimitInterceptor;
import com.github.miemiedev.mybatis.paginator.dialect.Dialect;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.github.miemiedev.mybatis.paginator.support.PropertiesHelper;
import com.github.miemiedev.mybatis.paginator.support.SQLHelp;

/**
 * Created by kimsangyong on 2016. 5. 25..
 */
@Intercepts({@Signature(
		type = Executor.class,
		method = "query",
		args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class QuadMaxOffsetLimitInterceptor extends OffsetLimitInterceptor {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuadMaxOffsetLimitInterceptor.class);

	public static final int MAPPED_STATEMENT_INDEX = 0;
	public static final int PARAMETER_INDEX = 1;
	public static final int ROWBOUNDS_INDEX = 2;
	public static final int RESULT_HANDLER_INDEX = 3;

	 String oracleDialect;
	 String mssqlDialect;
	 String db2Dialect;
	 String mysqlDialect;

	 String quadmaxDialectClass;
	 boolean quadmaxAsyncTotalCount;

	static ExecutorService quadmaxPool;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object intercept(final Invocation invocation) throws Throwable {
		final Executor executor = (Executor) invocation.getTarget();
		final Object[] queryArgs = invocation.getArgs();
		final MappedStatement ms = (MappedStatement) queryArgs[MAPPED_STATEMENT_INDEX];
		final Object parameter = queryArgs[PARAMETER_INDEX];
		final RowBounds rowBounds = (RowBounds) queryArgs[ROWBOUNDS_INDEX];
		final PageBounds pageBounds = new PageBounds(rowBounds);

		//페이징 쿼리시 paraminterceptor 타시 않아 삽입
		addExtraParams(parameter);

		if (pageBounds.getOffset() == RowBounds.NO_ROW_OFFSET
				&& pageBounds.getLimit() == RowBounds.NO_ROW_LIMIT
				&& pageBounds.getOrders().isEmpty()) {
			return invocation.proceed();
		}

		final Dialect dialect;
		try {
			String dialectString = "";

			String databaseId = new VendorDatabaseIdProvider().getDatabaseId(ms.getConfiguration().getEnvironment().getDataSource());

			if (databaseId == null) databaseId = "";

			if (databaseId.toUpperCase().contains("ORACLE")) {
				dialectString = oracleDialect;
			} else if (databaseId.toUpperCase().contains("SQL SERVER")) {
				dialectString = mssqlDialect;
			} else if (databaseId.toUpperCase().contains("DB2")) {
				dialectString = db2Dialect;
			} else if (databaseId.toUpperCase().contains("MYSQL")) {
				dialectString = mysqlDialect;
			} else {
				dialectString = quadmaxDialectClass;
			}
			Class clazz = Class.forName(dialectString);
			Constructor constructor = clazz.getConstructor(MappedStatement.class, Object.class, PageBounds.class);
			dialect = (Dialect) constructor.newInstance(ms, parameter, pageBounds);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new ClassNotFoundException("Cannot create dialect instance: " + quadmaxDialectClass, e);
		}

		final BoundSql boundSql = ms.getBoundSql(parameter);

		queryArgs[MAPPED_STATEMENT_INDEX] = quadmaxCopyFromNewSql(ms, boundSql, dialect.getPageSQL(), dialect.getParameterMappings(), dialect.getParameterObject());
		queryArgs[PARAMETER_INDEX] = dialect.getParameterObject();
		queryArgs[ROWBOUNDS_INDEX] = new RowBounds(RowBounds.NO_ROW_OFFSET, RowBounds.NO_ROW_LIMIT);

		boolean async = pageBounds.getAsyncTotalCount() == null ? quadmaxAsyncTotalCount : pageBounds.getAsyncTotalCount();
		Future<List> listFuture = quadmaxCall(new Callable<List>() {
			public List call() throws Exception {
				return (List) invocation.proceed();
			}
		}, async);


		if (pageBounds.isContainsTotalCount()) {
			Callable<Paginator> countTask = new Callable() {
				public Object call() throws Exception {
					Integer count;
					Cache cache = ms.getCache();
					if (cache != null && ms.isUseCache() && ms.getConfiguration().isCacheEnabled()) {
						CacheKey cacheKey = executor.createCacheKey(ms, parameter, new PageBounds(), quadmaxCopyFromBoundSql(ms, boundSql, dialect.getCountSQL(), boundSql.getParameterMappings(), boundSql.getParameterObject()));
						count = (Integer) cache.getObject(cacheKey);
						if (count == null) {
							count = SQLHelp.getCount(ms, executor.getTransaction(), parameter, boundSql, dialect);
							cache.putObject(cacheKey, count);
						}
					} else {
						count = SQLHelp.getCount(ms, executor.getTransaction(), parameter, boundSql, dialect);
					}
					return new Paginator(pageBounds.getPage(), pageBounds.getLimit(), count);
				}
			};
			Future<Paginator> countFutrue = quadmaxCall(countTask, async);
			return new PageList(listFuture.get(), countFutrue.get());
		}

		return listFuture.get();
	}

	@Override
	public void setProperties(Properties properties) {
		PropertiesHelper propertiesHelper = new PropertiesHelper(properties);
		setDialectClass(propertiesHelper.getRequiredString("dialectClass"));

		setOracleDialect(propertiesHelper.getRequiredString("oracleDialect"));
		setMssqlDialect(propertiesHelper.getRequiredString("mssqlDialect"));
		setDb2Dialect(propertiesHelper.getRequiredString("db2Dialect"));
		setMysqlDialect(propertiesHelper.getRequiredString("mysqlDialect"));

		setAsyncTotalCount(propertiesHelper.getBoolean("asyncTotalCount", false));

		setPoolMaxSize(propertiesHelper.getInt("poolMaxSize", 0));

	}

	private MappedStatement quadmaxCopyFromNewSql(MappedStatement ms, BoundSql boundSql,
										   String sql, List<ParameterMapping> parameterMappings, Object parameter) {
		BoundSql newBoundSql = quadmaxCopyFromBoundSql(ms, boundSql, sql, parameterMappings, parameter);
		return quadmaxCopyFromMappedStatement(ms, new BoundSqlSqlSource(newBoundSql));
	}


	private BoundSql quadmaxCopyFromBoundSql(MappedStatement ms, BoundSql boundSql,
									  String sql, List<ParameterMapping> parameterMappings, Object parameter) {
		BoundSql newBoundSql = new BoundSql(ms.getConfiguration(), sql, parameterMappings, parameter);
		for (ParameterMapping mapping : boundSql.getParameterMappings()) {
			String prop = mapping.getProperty();
			if (boundSql.hasAdditionalParameter(prop)) {
				newBoundSql.setAdditionalParameter(prop, boundSql.getAdditionalParameter(prop));
			}
		}
		return newBoundSql;
	}

	//see: MapperBuilderAssistant
	private MappedStatement quadmaxCopyFromMappedStatement(MappedStatement ms, SqlSource newSqlSource) {
		MappedStatement.Builder builder = new MappedStatement.Builder(ms.getConfiguration(), ms.getId(), newSqlSource, ms.getSqlCommandType());

		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
		if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
			StringBuilder keyProperties = new StringBuilder();
			for (String keyProperty : ms.getKeyProperties()) {
				keyProperties.append(keyProperty).append(",");
			}
			keyProperties.delete(keyProperties.length() - 1, keyProperties.length());
			builder.keyProperty(keyProperties.toString());
		}

		//setStatementTimeout()
		builder.timeout(ms.getTimeout());

		//setStatementResultMap()
		builder.parameterMap(ms.getParameterMap());

		//setStatementResultMap()
		builder.resultMaps(ms.getResultMaps());
		builder.resultSetType(ms.getResultSetType());

		//setStatementCache()
		builder.cache(ms.getCache());
		builder.flushCacheRequired(ms.isFlushCacheRequired());
		builder.useCache(ms.isUseCache());

		return builder.build();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T> Future<T> quadmaxCall(Callable callable, boolean async) {
		if (async) {
			return quadmaxPool.submit(callable);
		} else {
			FutureTask<T> future = new FutureTask(callable);
			future.run();
			return future;
		}
	}

	private void addExtraParams(Object parameter) {
		//페이징 쿼리시 paraminterceptor 타시 않아 삽입
		if(parameter instanceof Map){
			Map<String, Object> paramMap = (Map<String, Object>)parameter;
			String quadMaxlanguage = null;
			UserVO userVO = null;

			if(QuadMaxUserDetailsHelper.isAuthenticated().booleanValue()) {
				userVO = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();
				if (!paramMap.containsKey(Const.USER_VO)) {
					paramMap.put(Const.USER_VO, userVO);
				}
			}

			if (!paramMap.containsKey("quadMaxlanguage")) {
				quadMaxlanguage = Util.getQuadMaxLanguage();
				paramMap.put("quadMaxlanguage", quadMaxlanguage);
			}

			// 이마트 기본 관계사 코드
			if (!paramMap.containsKey(Const.AFLCO_CD)) {
				paramMap.put(Const.AFLCO_CD, Const.AFLCO_CD_VALUE);
			}
		}
	}

	@Override
	public void setDialectClass(String dialectClass) {
		LOGGER.debug("dialectClass: {} ", dialectClass);
		this.quadmaxDialectClass = dialectClass;
	}

	public void setOracleDialect(String oracleDialect) {
		this.oracleDialect = oracleDialect;
	}

	public void setMssqlDialect(String mssqlDialect) {
		this.mssqlDialect = mssqlDialect;
	}

	public void setDb2Dialect(String db2Dialect) {
		this.db2Dialect = db2Dialect;
	}

	public void setMysqlDialect(String mysqlDialect) {
		this.mysqlDialect = mysqlDialect;
	}
}

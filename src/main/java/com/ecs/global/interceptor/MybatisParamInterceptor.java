package com.ecs.global.interceptor;

import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Properties;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.global.constant.Const;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
import com.ecs.global.util.Util;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Intercepts({@Signature(type=ParameterHandler.class, method="setParameters", args={PreparedStatement.class})})
public class MybatisParamInterceptor implements Interceptor{
	private static final Logger LOGGER = LoggerFactory.getLogger(MybatisParamInterceptor.class);

	@SuppressWarnings("unchecked")
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		ParameterHandler parameterHandler = null;
		Object parameters = null;
		Map<String, Object> paramMap = null;

		parameterHandler = (ParameterHandler) invocation.getTarget();
		parameters = parameterHandler.getParameterObject();
		
		boolean printF = true;


		LOGGER.debug("***************************************************************************");

		/**
		 * 파라미터가 맵일경우
		 */
		if (parameters instanceof Map) {
			paramMap = (Map<String, Object>) parameters;
			String quadMaxlanguage = null;
			UserVO userVO = null;

			if(QuadMaxUserDetailsHelper.isAuthenticated().booleanValue()) {
				userVO = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();

				if (!paramMap.containsKey(Const.USER_VO)) {
					paramMap.put(Const.USER_VO, userVO);
				}
			}
			
			if (!paramMap.containsKey("quadMaxlanguage")) {
				quadMaxlanguage = Util.getQuadMaxLanguage();
				paramMap.put("quadMaxlanguage", quadMaxlanguage);
			}
			
//			if (!paramMap.containsKey("prdtDiNm")) {
//				printF = false;
//			}
			
			// 관계사 코드
			if (!paramMap.containsKey(Const.AFLCO_CD)) {
				paramMap.put(Const.AFLCO_CD, Const.AFLCO_CD_VALUE);
			}
		}
		
		if (printF) LOGGER.debug("Mybatis parameters : {} ", parameters);
		LOGGER.debug("***************************************************************************");

		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		//
	}

}

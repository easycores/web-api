package com.ecs.global.config;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.spring3.properties.EncryptablePropertyPlaceholderConfigurer;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class PropertiesConfig {

    @Bean
    public EnvironmentStringPBEConfig environmentStringPBEConfig() {
        EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
        config.setProvider(new BouncyCastleProvider());
        config.setAlgorithm("PBEWITHSHA256AND256BITAES-CBC-BC");
        config.setProviderName("BC");
        config.setPassword("SMART_QUADMAX");
        return config;
    }

    @Bean
    public StandardPBEStringEncryptor configurationEncryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setConfig(environmentStringPBEConfig());
        return encryptor;
    }

    @Bean
    public EncryptablePropertyPlaceholderConfigurer propertyConfigurer() {
        EncryptablePropertyPlaceholderConfigurer configurer
                = new EncryptablePropertyPlaceholderConfigurer(configurationEncryptor());
        configurer.setLocations(
            new ClassPathResource("application.properties"),
            new ClassPathResource("jdbc.properties")
        );
        return configurer;
    }

    // @Value("#{application['key']}") 의 표현 가능
    @Bean(name="application")
    public PropertiesFactoryBean applicationProperties() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("application.properties"));
        return bean;
    }

    // @Value("#{jdbc['key']}") 의 표현 가능
    @Bean(name = "jdbc")
    public PropertiesFactoryBean jdbcProperties() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("jdbc.properties"));
        return bean;
    }

}
























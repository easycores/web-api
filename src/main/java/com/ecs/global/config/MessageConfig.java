package com.ecs.global.config;

import com.ecs.global.i18n.QuadMaxMessageSource;
import com.ecs.global.i18n.QuadMaxReloadableResourceBundleMessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfig {

    @Bean
    public QuadMaxMessageSource quadMaxMessageSource() {
        QuadMaxMessageSource quadMaxMessageSource = new QuadMaxMessageSource();
        quadMaxMessageSource.setReloadableResourceBundleMessageSource(messageSource());
        quadMaxMessageSource.setDefaultEncoding("UTF-8");
        return quadMaxMessageSource;
    }

    @Bean
    public QuadMaxReloadableResourceBundleMessageSource messageSource(){
        QuadMaxReloadableResourceBundleMessageSource messageSource = new QuadMaxReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/message/message-common");
        messageSource.setCacheSeconds(180);
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}

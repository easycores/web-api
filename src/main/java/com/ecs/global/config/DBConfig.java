package com.ecs.global.config;

import com.ecs.global.interceptor.MybatisParamInterceptor;
import com.ecs.global.interceptor.MybatisStatementInterceptor;
import com.ecs.global.interceptor.QuadMaxOffsetLimitInterceptor;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.*;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DBConfig {

    @Value("${jdbc.QuadMax.DriverClassName}") String Quadmax_DriverClassName;
    @Value("${jdbc.QuadMax.Url}") String Quadmax_Url;
    @Value("${jdbc.QuadMax.UserName}") String Quadmax_UserName;
    @Value("${jdbc.QuadMax.Password}") String Quadmax_Password;
    @Value("${jdbc.QuadMax.initialSize}") int Quadmax_initialSize;
    @Value("${jdbc.QuadMax.minIdle}") int Quadmax_minIdle;
    @Value("${jdbc.QuadMax.maxIdle}") int Quadmax_maxIdle;
    @Value("${jdbc.QuadMax.maxTotal}") int Quadmax_maxTotal;

    // QUADMAX DB
    @Bean(name = {"dataSource-quadmax", "quadMax.dataSource"}, destroyMethod = "close")
    public HikariDataSource dataSourceQuadMax() {

        HikariConfig hikariConfig = new HikariConfig();

        /*  아래 4가지 속성에 대해 hikariDatasource 사용시 불필요 여부 확인 필요
            basicDataSource.setInitialSize(Quadmax_initialSize);
            basicDataSource.setMaxIdle(Quadmax_maxIdle);
            basicDataSource.setMaxWaitMillis(10000);
            basicDataSource.setTestOnBorrow(true);
        */
        hikariConfig.setJdbcUrl(Quadmax_Url);
        hikariConfig.setMinimumIdle(Quadmax_minIdle);
        hikariConfig.setConnectionTestQuery("/*==IGNORE_LOG==*/ SELECT 1");
        hikariConfig.setMaximumPoolSize(Quadmax_maxTotal);
        hikariConfig.setValidationTimeout(5000);
        hikariConfig.setDriverClassName(Quadmax_DriverClassName);
        hikariConfig.setUsername(Quadmax_UserName);
        hikariConfig.setPassword(Quadmax_Password);

        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public org.apache.ibatis.session.Configuration configuration() throws Exception {
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();

        Properties variables = PropertiesLoaderUtils.loadProperties(new ClassPathResource("jdbc.properties"));
        configuration.setVariables(variables);

        configuration.setCacheEnabled(false);
        configuration.setUseGeneratedKeys(true);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setJdbcTypeForNull(JdbcType.NULL);

        configuration.getTypeAliasRegistry().registerAlias("resultMap","com.ecs.infrastructure.model.ResultMap");
        configuration.getTypeAliasRegistry().registerAlias("caseMap","org.apache.commons.collections.map.CaseInsensitiveMap");

        configuration.addInterceptor(new MybatisParamInterceptor());
        QuadMaxOffsetLimitInterceptor quadMaxInterceptor = new QuadMaxOffsetLimitInterceptor();
        quadMaxInterceptor.setDialectClass("com.github.miemiedev.mybatis.paginator.dialect.MySQLDialect"); // 기본 Dialect 클래스
        quadMaxInterceptor.setOracleDialect("com.github.miemiedev.mybatis.paginator.dialect.OracleDialect");
        quadMaxInterceptor.setMssqlDialect("com.ecs.infrastructure.dialect.SQLServerDialect");
        quadMaxInterceptor.setDb2Dialect("com.ecs.infrastructure.dialect.DB2Dialect");
        quadMaxInterceptor.setMysqlDialect("com.github.miemiedev.mybatis.paginator.dialect.MySQLDialect");
        configuration.addInterceptor(quadMaxInterceptor);
        configuration.addInterceptor(new MybatisStatementInterceptor());

        return configuration;
    }

    @Bean
    public VendorDatabaseIdProvider databaseIdProvider() {
        Properties vendorProperties = new Properties();
        vendorProperties.put("Server", "sqlserver");
        vendorProperties.put("DB2", "db2");
        vendorProperties.put("Oracle", "oracle");
        vendorProperties.put("MySql", "mysql");
        VendorDatabaseIdProvider provider = new VendorDatabaseIdProvider();
        provider.setProperties(vendorProperties);
        return provider;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setDatabaseIdProvider(databaseIdProvider());

        Resource[] mapperLocations = new PathMatchingResourcePatternResolver()
                .getResources("classpath:mybatis/mapper/**/*Mapper.xml");

        sessionFactoryBean.setMapperLocations(mapperLocations);
        sessionFactoryBean.setConfiguration(configuration());

        return sessionFactoryBean;
    }

    @Bean
    public TransactionManager txManager(DataSource dataSource) {
        // datasource : hikari datasource
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public TransactionInterceptor txAdvice(TransactionManager txManager){

        NameMatchTransactionAttributeSource txAttributeSource = new NameMatchTransactionAttributeSource();
        HashMap<String, TransactionAttribute> txMethods = new HashMap<>();

        RuleBasedTransactionAttribute txAttribute1 = new RuleBasedTransactionAttribute();
        txAttribute1.setRollbackRules(Collections.singletonList(new RollbackRuleAttribute(Exception.class)));
        txAttribute1.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        txAttribute1.setReadOnly(false);
        txAttribute1.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);

        txMethods.put("insert*", txAttribute1);
        txMethods.put("update*", txAttribute1);
        txMethods.put("delete*", txAttribute1);
        txMethods.put("save*", txAttribute1);
        txMethods.put("*", txAttribute1);

        RuleBasedTransactionAttribute txAttribute2 = new RuleBasedTransactionAttribute();
        txAttribute2.setReadOnly(true);

        txMethods.put("select*", txAttribute2);
        txMethods.put("get*", txAttribute2);

        txAttributeSource.setNameMap(txMethods);

        return new TransactionInterceptor(txManager, txAttributeSource);
    }

    @Bean
    public Advisor transactionAdviceAdvisor() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("within(@org.springframework.stereotype.Service *)");
        return new DefaultPointcutAdvisor(pointcut, txAdvice(txManager(dataSourceQuadMax())));
    }
}


















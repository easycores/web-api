package com.ecs.global.config;

import com.ecs.global.view.CsvDownloadView;
import com.ecs.global.view.QuadMaxJsonView;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.BeanNameViewResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class ViewConfig {

	@Bean
	public BeanNameViewResolver beanNameViewResolver() {
		BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
		beanNameViewResolver.setOrder(0);
		return beanNameViewResolver;
	}

    @Bean
    public QuadMaxJsonView jsonView(){
        QuadMaxJsonView quadMaxJsonView = new QuadMaxJsonView();
        quadMaxJsonView.setContentType("application/json;charset=UTF-8");

        List<String> xssEscapeExclude = Arrays.asList(
                "/dashboard/selectBusinessQueryList.do",
				"/dashboard/selectBusinessQueryFieldDataList.do",
				"/dashboard/selectBusinessQueryLayoutDataList.do",
				"/dashboard/selectDashboardJsonInfo.do",
				"/webflow/TargetingData.do",
				"/workflow/modal/*",
				"/workflow/Node*",
				"/workflow/WorkflowExec*",
				"/channel/ChannelContentsData.do",
				"/channel/ChannelContentsInfo.do",
				"/common/contTemplate/getAjaxData.do",
				"/common/customer/getAjaxData.do",
				"/common/cmonGubun/getAjaxData.do",
				"/common/cmon/getAjaxData.do",
				"/targetsupport/HierarchyInfoData.do",
				"/manage/template/BusinessQueryManageAjax.do",
				"/manage/template/BusinessQueryInfoData.do",
				"/manage/template/BusinessQueryPromptInfo.do",
				
				"/manage/template/ChannelTypeInfo.do",
				
				"/manage/system/CommonCode*",
				"/manage/system/GubunCode*",
				"/manage/template/ChannelContentsTemplateInfo.do",
				"/manage/template/ChannelTextTemplateInfo.do",
				"/manage/template/ChannelContentsMessagePagedList.do",
				"/manage/template/ChannelContentsMessage.do",
				"/ebmScenario/getComponentData.do",
				"/ebmRuleFunction/getRuleFunctionData.do",
				"/ebmScenario/getFactData.do",
				"/ebmScenario/getMessageData.do",
				
				"/common/prodList/getAjaxData.do",
				"/product/popup/productExcepUpload.do",
				"/product/popup/selectCampSuccssCndtnPrdt.do",
				
				/* 타겟팅 노드 팝업 [ 상품, 상품 소분류  ] */
				"/common/prdtDiCdList/getAjaxData.do",
				"/common/prdtCateCdList/getAjaxData.do",
				"/common/prdtGcodeCdList/getAjaxData.do",
				"/common/prdtMcodeCdList/getAjaxData.do",
				"/common/prdtDcodeCdList/getAjaxData.do",
				"/common/gridPrdtList/getAjaxData.do",
				"/common/gridPrdtListByRows/getAjaxData.do",
				"/common/gridPrdtDcodeList/getAjaxData.do",
				
				/* 오퍼/채널 노드  : 오퍼 */
				"/common/offerTypeCdList/getAjaxData.do",
				"/common/campContentsMsg/getAjaxData.do",
				
				/* 대시보드 팝업  (성과분석) */
				"/dashboardchart/getInitData.do",
				
				/* 알림톡 템플릿 */
				"/common/contTemplateSelect/getAjaxData.do"
        );
        quadMaxJsonView.setExcludePaths(xssEscapeExclude);
        quadMaxJsonView.setPrettyPrint(true);
        return quadMaxJsonView;
    }

    @Bean
    public CsvDownloadView csvDownloadView() {
        return new CsvDownloadView();
    }

}

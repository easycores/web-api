package com.ecs.domain.rest.ecscontainer.scenario.model.api;

import lombok.Data;

public class EcsContainerGetResponse {

    @Data
    public static class ResponseListDTO {
        private String campId;
    }

}

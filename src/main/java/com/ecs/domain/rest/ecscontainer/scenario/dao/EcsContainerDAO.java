package com.ecs.domain.rest.ecscontainer.scenario.dao;

import org.apache.ibatis.annotations.Mapper;

import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

@Mapper
public interface EcsContainerDAO {

	public ResultMap selectCampaign(String campId);

}

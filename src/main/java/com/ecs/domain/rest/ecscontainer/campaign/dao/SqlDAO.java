package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("sqlDAO")
@Mapper
public interface SqlDAO {

	int deleteCampSql(String campId);
	int insertCampSql(Map<String, Object> paramMap);
	int insertCampSqlWithSelect(Map<String, Object> paramMap);
}

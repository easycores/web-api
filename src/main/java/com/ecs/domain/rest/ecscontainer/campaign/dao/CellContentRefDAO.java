package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("cellContentRefDAO")
@Mapper
public interface CellContentRefDAO {

	int insertCellContentRefWithSelect(Map<String, Object> paramMap);

}

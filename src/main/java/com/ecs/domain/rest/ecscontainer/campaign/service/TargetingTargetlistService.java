package com.ecs.domain.rest.ecscontainer.campaign.service;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import java.util.List;
import java.util.Map;

public interface TargetingTargetlistService {

	List<ResultMap> getTnTargetlist(Map<String, Object> paramMap, PageBounds pageBounds);
	
	List<ResultMap> getTnTargetlist(Map<String, Object> paramMap);

	List<ResultMap> getTargetingTargetlistSaveData(Map<String, Object> paramMap);


}

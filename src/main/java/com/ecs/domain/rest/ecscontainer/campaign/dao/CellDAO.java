package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("cellDAO")
@Mapper
public interface CellDAO {


	int insertCellWithSelect(Map<String, Object> paramMap);

	//승인요청/자가승인 추가
	String checkCellNodeCount(String campId);
	String checkCellNodeCustCount(String campId);
}

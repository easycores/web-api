package com.ecs.domain.rest.ecscontainer.scenario.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ecs.domain.rest.ecscontainer.scenario.dao.EcsContainerDAO;
//import com.ecs.quadmax.ecscontainer.scenario.dao.EcsContainerDAO;
import com.ecs.domain.rest.ecscontainer.scenario.service.EcsContainerService;
//import com.ecs.quadmax.ecscontainer.scenario.service.EcsContainerService;
import com.ecs.infrastructure.model.ResultMap;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service("ecsContainerService")
public class EcsContainerServiceImpl implements EcsContainerService  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EcsContainerServiceImpl.class);

	@Autowired
	private EcsContainerDAO ecsContainerDAO;
	
	@Value("#{application['application.cms.queryserverUrl']}")
	private String queryServerUrl;
	
	@Value("#{application['application.cms.queryserverUrl2']}")
	private String queryServerUrl2;
	

	@Override
	public ResultMap selectCampaign(String campId) {
		return ecsContainerDAO.selectCampaign(campId);
	}

	//승인/반려 처리 추가
	@Override
	public String getAliveQueryServerUrl() {
		List<String> serverURLList = getQueryServerUrlList();
		
		if (serverURLList == null || serverURLList.size() == 0) {
			return "N/E";
		}
		
		for (String serverUrl : serverURLList) {
			if (StringUtils.isNotEmpty(serverUrl) && serverUrl.endsWith("/")) {
				serverUrl = serverUrl.substring(0, serverUrl.length()-1);
			}
			
			// Server Status API
			String url = serverUrl + "/query/status";
			HttpEntity<String> req = setHeader(null);
			RestTemplate restTemplate = new RestTemplate();
			
			try {
				ResponseEntity<Object> obj = restTemplate.exchange(url, HttpMethod.GET, req, Object.class);		
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.convertValue(obj.getBody(), JsonNode.class);
				
				String res = node.get("status").asText();
				if (res.equals("alive")) {
					return serverUrl;
				}
			} catch (RuntimeException e) {
				LOGGER.error("***** ServerURL {} is not available! *****", serverUrl);
			} catch (Exception e) {
				LOGGER.error("***** ServerURL {} is not available! *****", serverUrl);
			}
		}
		
		return "N/A";
	}
	
	//승인/반려 처리 추가
	@Override
	public List<String> getQueryServerUrlList() {
		List<String> list = new ArrayList<String>();
		if (StringUtils.isNotEmpty(queryServerUrl))
			list = Arrays.asList(queryServerUrl.split(","));
		return list;
	}
	
	//승인/반려 처리 추가
	public HttpEntity<String> setHeader(String body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(body, headers);
	}

	//승인/반려 처리 추가
	@Override
	public String getMasterServerURL() throws IOException {
		List<String> urlList = getQueryServerUrlList();
		String masterServer = null;
		
		if(urlList.isEmpty()) {
			return "N/E";
		}
		for(int i = 0; i < urlList.size(); i++) {
			String serverUrl = urlList.get(i);
			if (StringUtils.isNotEmpty(serverUrl) && serverUrl.endsWith("/")) {
				serverUrl = serverUrl.substring(0, serverUrl.length()-1);
			}
			try {
				URL url = new URL(serverUrl + "/scheduler/schedule/status");
				HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
				
				con.setConnectTimeout(5000); 
				con.setReadTimeout(5000); 
				con.setRequestMethod("GET");
		
				StringBuilder sb = new StringBuilder();
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
						String line;
						line = br.readLine();
						while (line != null) {
							sb.append(line).append("\n");
							line = br.readLine();
						}
						ObjectMapper mapper = new ObjectMapper();
						ObjectNode jsonObject = mapper.readValue(sb.toString(), ObjectNode.class);
						masterServer = jsonObject.get("master").asText();
						break;
					}
				}
				
			} catch (RuntimeException e) {
				LOGGER.error("***** ServerURL {} is not available! *****", serverUrl);
			} catch (Exception e) {
				LOGGER.error("***** ServerURL {} is not available! *****", serverUrl);
			}
		}
		if(masterServer == null) {
			return "N/A";
		}
		
		return masterServer;
	}
		
	
}

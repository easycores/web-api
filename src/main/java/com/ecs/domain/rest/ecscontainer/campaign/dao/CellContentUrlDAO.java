package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("cellContentUrlDAO")
@Mapper
public interface CellContentUrlDAO {

	int insertCellContentUrlWithSelect(Map<String, Object> paramMap);

}

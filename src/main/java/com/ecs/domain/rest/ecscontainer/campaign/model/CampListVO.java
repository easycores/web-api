package com.ecs.domain.rest.ecscontainer.campaign.model;

public class CampListVO {
	private String campId;                      //캠페인ID
	private String campName;                    //캠페인명
	private String campGpId;                    //캠페인그룹ID
	private String campGpName;                  //캠페인그룹명
	private String campTypeCd;                  //캠페인유형코드
	private String campTypeName;                //캠페인유형명
	private String ctypId;                      //설계유형ID
	private String ctypName;                    //설계유형명
	private String cstsId;                      //캠페인상태ID
	private String cstsName;                    //캠페인상태명
	private String campSdate;                   //캠페인시작일자(YYYYMMDD)
	private String campEdate;                   //캠페인종료일자(YYYYMMDD)
	private String campDesc;                    //비고
	private String delF;                        //삭제여부
	private String regEmpId;                    //등록자ID
	private String empName;                  //등록자명
	private String regDeptId;                   //등록부서ID
	private String deptName;                 //등록부서명
	private String regDate;                     //등록일
	private String regTime;
//	private String targetStr;                    //대상지점
//	private String campStrName;					//대상지점
	private Double campRoi;                      //사전ROI
	private String cellInfo;                     //고객군정보
	private Double progRate;                     // 진행율
	private String chnlGpName;                   /* 접촉채널 */
	private String campRegEmpName;               /* 담당자   */
	private String campRegEmpId;                 // 담당자ID
	private String campExecNo;                   /* 서브 쿼리와의 조인을 위한 KEY (2/3) */
	private String cellNodeId;                   /* 서브 쿼리와의 조인을 위한 KEY (3/3) */
	private String objBrch;			             /* 대상지점 */
	private Double expRoi;                       // 사전ROI
	private String cellName;                    // 고객군명
	private String campDailyResp;               // 대상군/비교군
	private Double ogPurchRspnRate;          // 대상군 구매반응률
	private Double cgPurchRspnRate;          // 비교군 구매반응률
	private String obgrpMembCnt;				//대상고객수(완료)
	private Double purchRspnRate;				//구매반응률(완료)
	private Double offerRspnRate;				//오퍼반응률(완료)
	private String enpns;						//발생비용(완료)
	private String prfVlm;						//추가매출이익(완료)
	private Double roi;							//최종ROI(완료)
	private String campExecMainName;				//캠페인실행주체
//	private String cateDirName;					// 캠페인 분류 명
	private String campCateName;				//캠페인분류명
	private String updEmpId;                    //당당자ID
	private String updEmpName;                  //담당자명
	private String updDeptId;                   //담당부서ID
	private String updDeptName;                 //담당부서명
	private String channelNodes;				//채널노드정보
	private String schType;						//스캐줄 타입
	//------------------------------------------------------------
	private String schNextTime;					// 다음 작업타임
	private String schNextDateTime;				// 다음 작업일시
	//------------------------------------------------------------
	private String projectcategory;				// 캠페인 카테고리 (EBM, TARGET)
	private String projecttype;					// 캠페인 타입 (containers, flowchart)
	private String campBaseTypeName;
	private String regDttm;
	private String asisCampYn;
	
	private String sccssCndtnNm ;
	private String hqStrDivNm;
	private String biztpNm;
	private String strNm;
	private String campCate;
	private String strCd;
	private String biztpCd;
	
	private String orderStr;
	
	private int exNodeCnt;
	private int exNodeCntT;
	private int exNodeCntC;
	private int exNodeCntE;
	private int reqCustCnt;
	private int successCustCnt;
	private int sendCustCnt;
	private int excldCustCnt;
	private int failCustCnt;
	private String chnlName;
	private String cellNodeName;
	private String offerIfStatus;
	private String chnlSendStatus;
	
	private String procSchedDttm;
	private String sendDate;

	public String getCampId() {
		return campId;
	}

	public String getCampCateName() {
		return campCateName;
	}

	public void setCampCateName(String campCateName) {
		this.campCateName = campCateName;
	}

	public void setCampId(String campId) {
		this.campId = campId;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public String getCampGpId() {
		return campGpId;
	}

	public void setCampGpId(String campGpId) {
		this.campGpId = campGpId;
	}

	public String getCampGpName() {
		return campGpName;
	}

	public void setCampGpName(String campGpName) {
		this.campGpName = campGpName;
	}

	public String getCampTypeCd() {
		return campTypeCd;
	}

	public void setCampTypeCd(String campTypeCd) {
		this.campTypeCd = campTypeCd;
	}

	public String getCampTypeName() {
		return campTypeName;
	}

	public void setCampTypeName(String campTypeName) {
		this.campTypeName = campTypeName;
	}

	public String getCtypId() {
		return ctypId;
	}

	public void setCtypId(String ctypId) {
		this.ctypId = ctypId;
	}

	public String getCtypName() {
		return ctypName;
	}

	public void setCtypName(String ctypName) {
		this.ctypName = ctypName;
	}

	public String getCstsId() {
		return cstsId;
	}

	public void setCstsId(String cstsId) {
		this.cstsId = cstsId;
	}

	public String getCstsName() {
		return cstsName;
	}

	public void setCstsName(String cstsName) {
		this.cstsName = cstsName;
	}

	public String getCampSdate() {
		return campSdate;
	}

	public void setCampSdate(String campSdate) {
		this.campSdate = campSdate;
	}

	public String getCampEdate() {
		return campEdate;
	}

	public void setCampEdate(String campEdate) {
		this.campEdate = campEdate;
	}

	public String getCampDesc() {
		return campDesc;
	}

	public void setCampDesc(String campDesc) {
		this.campDesc = campDesc;
	}

	public String getDelF() {
		return delF;
	}

	public void setDelF(String delF) {
		this.delF = delF;
	}

	public String getRegEmpId() {
		return regEmpId;
	}

	public void setRegEmpId(String regEmpId) {
		this.regEmpId = regEmpId;
	}

	public String getRegDeptId() {
		return regDeptId;
	}

	public void setRegDeptId(String regDeptId) {
		this.regDeptId = regDeptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public Double getOgPurchRspnRate() {
		return ogPurchRspnRate;
	}

	public void setOgPurchRspnRate(Double ogPurchRspnRate) {
		this.ogPurchRspnRate = ogPurchRspnRate;
	}

	public Double getCgPurchRspnRate() {
		return cgPurchRspnRate;
	}

	public void setCgPurchRspnRate(Double cgPurchRspnRate) {
		this.cgPurchRspnRate = cgPurchRspnRate;
	}

	public String getCampRegEmpId() {
		return campRegEmpId;
	}

	public void setCampRegEmpId(String campRegEmpId) {
		this.campRegEmpId = campRegEmpId;
	}

	public String getCampDailyResp() {
		return campDailyResp;
	}

	public void setCampDailyResp(String campDailyResp) {
		this.campDailyResp = campDailyResp;
	}

	public String getCellName() {
		return cellName;
	}

	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

	public Double getExpRoi() {
		return expRoi;
	}

	public void setExpRoi(Double expRoi) {
		this.expRoi = expRoi;
	}

	public Double getProgRate() {
		return progRate;
	}

	public void setProgRate(Double progRate) {
		this.progRate = progRate;
	}

	public String getChnlGpName() {
		return chnlGpName;
	}

	public void setChnlGpName(String chnlGpName) {
		this.chnlGpName = chnlGpName;
	}

	public String getCampRegEmpName() {
		return campRegEmpName;
	}

	public void setCampRegEmpName(String campRegEmpName) {
		this.campRegEmpName = campRegEmpName;
	}

	public String getCampExecNo() {
		return campExecNo;
	}

	public void setCampExecNo(String campExecNo) {
		this.campExecNo = campExecNo;
	}

	public String getCellNodeId() {
		return cellNodeId;
	}

	public void setCellNodeId(String cellNodeId) {
		this.cellNodeId = cellNodeId;
	}

	public String getObjBrch() {
		return objBrch;
	}

	public void setObjBrch(String objBrch) {
		this.objBrch = objBrch;
	}

	public String getCellInfo() {
		return cellInfo;
	}

	public void setCellInfo(String cellInfo) {
		this.cellInfo = cellInfo;
	}

	public Double getCampRoi() {
		return campRoi;
	}

	public void setCampRoi(Double campRoi) {
		this.campRoi = campRoi;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getObgrpMembCnt() {
		return obgrpMembCnt;
	}

	public void setObgrpMembCnt(String obgrpMembCnt) {
		this.obgrpMembCnt = obgrpMembCnt;
	}

	public Double getPurchRspnRate() {
		return purchRspnRate;
	}

	public void setPurchRspnRate(Double purchRspnRate) {
		this.purchRspnRate = purchRspnRate;
	}

	public Double getOfferRspnRate() {
		return offerRspnRate;
	}

	public void setOfferRspnRate(Double offerRspnRate) {
		this.offerRspnRate = offerRspnRate;
	}

	public String getEnpns() {
		return enpns;
	}

	public void setEnpns(String enpns) {
		this.enpns = enpns;
	}

	public String getPrfVlm() {
		return prfVlm;
	}

	public void setPrfVlm(String prfVlm) {
		this.prfVlm = prfVlm;
	}

	public Double getRoi() {
		return roi;
	}

	public void setRoi(Double roi) {
		this.roi = roi;
	}

	public String getCampExecMainName() {
		return campExecMainName;
	}

	public void setCampExecMainName(String campExecMainName) {
		this.campExecMainName = campExecMainName;
	}

	public String getUpdEmpId() {
		return updEmpId;
	}

	public void setUpdEmpId(String updEmpId) {
		this.updEmpId = updEmpId;
	}

	public String getUpdEmpName() {
		return updEmpName;
	}

	public void setUpdEmpName(String updEmpName) {
		this.updEmpName = updEmpName;
	}

	public String getUpdDeptId() {
		return updDeptId;
	}

	public void setUpdDeptId(String updDeptId) {
		this.updDeptId = updDeptId;
	}

	public String getUpdDeptName() {
		return updDeptName;
	}

	public void setUpdDeptName(String updDeptName) {
		this.updDeptName = updDeptName;
	}

	public String getChannelNodes() {
		return channelNodes;
	}

	public void setChannelNodes(String channelNodes) {
		this.channelNodes = channelNodes;
	}

	public String getSchType() {
		return schType;
	}

	public void setSchType(String schType) {
		this.schType = schType;
	}

	public String getSchNextTime() {
		return schNextTime;
	}

	public void setSchNextTime(String schNextTime) {
		this.schNextTime = schNextTime;
	}

	public String getSchNextDateTime() {
		return schNextDateTime;
	}

	public void setSchNextDateTime(String schNextDateTime) {
		this.schNextDateTime = schNextDateTime;
	}
	
	public String getProjectcategory() {
		return projectcategory;
	}

	public void setProjectcategory(String projectcategory) {
		this.projectcategory = projectcategory;
	}

	public String getProjecttype() {
		return projecttype;
	}

	public void setProjecttype(String projecttype) {
		this.projecttype = projecttype;
	}

	public String getCampBaseTypeName() {
		return campBaseTypeName;
	}

	public void setCampBaseTypeName(String campBaseTypeName) {
		this.campBaseTypeName = campBaseTypeName;
	}

	public String getRegDttm() {
		return regDttm;
	}

	public void setRegDttm(String regDttm) {
		this.regDttm = regDttm;
	}	
	
	public String getSccssCndtnNm() {
		return sccssCndtnNm;
	}
	public void setSccssCndtnNm(String sccssCndtnNm) {
		this.sccssCndtnNm = sccssCndtnNm;
	}
	public String getHqStrDivNm() {
		return hqStrDivNm;
	}
	public void setHqStrDivNm(String hqStrDivNm) {
		this.hqStrDivNm = hqStrDivNm;
	}
	public String getBiztpNm() {
		return biztpNm;
	}
	public void setBiztpNm(String biztpNm) {
		this.biztpNm = biztpNm;
	}
	public String getStrNm() {
		return strNm;
	}
	public void setStrNm(String strNm) {
		this.strNm = strNm;
	}

	public String getCampCate() {
		return campCate;
	}

	public void setCampCate(String campCate) {
		this.campCate = campCate;
	}

	public String getStrCd() {
		return strCd;
	}

	public void setStrCd(String strCd) {
		this.strCd = strCd;
	}

	public String getBiztpCd() {
		return biztpCd;
	}

	public void setBiztpCd(String biztpCd) {
		this.biztpCd = biztpCd;
	}

	public String getOrderStr() {
		return orderStr;
	}

	public void setOrderStr(String orderStr) {
		this.orderStr = orderStr;
	}

	public int getExNodeCnt() {
		return exNodeCnt;
	}

	public void setExNodeCnt(int exNodeCnt) {
		this.exNodeCnt = exNodeCnt;
	}

	public int getExNodeCntT() {
		return exNodeCntT;
	}

	public void setExNodeCntT(int exNodeCntT) {
		this.exNodeCntT = exNodeCntT;
	}

	public int getExNodeCntC() {
		return exNodeCntC;
	}

	public void setExNodeCntC(int exNodeCntC) {
		this.exNodeCntC = exNodeCntC;
	}

	public int getExNodeCntE() {
		return exNodeCntE;
	}

	public void setExNodeCntE(int exNodeCntE) {
		this.exNodeCntE = exNodeCntE;
	}

	public int getReqCustCnt() {
		return reqCustCnt;
	}

	public void setReqCustCnt(int reqCustCnt) {
		this.reqCustCnt = reqCustCnt;
	}

	public int getSuccessCustCnt() {
		return successCustCnt;
	}

	public void setSuccessCustCnt(int successCustCnt) {
		this.successCustCnt = successCustCnt;
	}

	public int getExcldCustCnt() {
		return excldCustCnt;
	}

	public void setExcldCustCnt(int excldCustCnt) {
		this.excldCustCnt = excldCustCnt;
	}

	public int getSendCustCnt() {
		return sendCustCnt;
	}

	public void setSendCustCnt(int sendCustCnt) {
		this.sendCustCnt = sendCustCnt;
	}
	
	public int getFailCustCnt() {
		return failCustCnt;
	}

	public void setFailCustCnt(int failCustCnt) {
		this.failCustCnt = failCustCnt;
	}

	public String getChnlName() {
		return chnlName;
	}

	public void setChnlName(String chnlName) {
		this.chnlName = chnlName;
	}

	public String getCellNodeName() {
		return cellNodeName;
	}

	public void setCellNodeName(String cellNodeName) {
		this.cellNodeName = cellNodeName;
	}

	public String getOfferIfStatus() {
		return offerIfStatus;
	}

	public void setOfferIfStatus(String offerIfStatus) {
		this.offerIfStatus = offerIfStatus;
	}

	public String getChnlSendStatus() {
		return chnlSendStatus;
	}

	public void setChnlSendStatus(String chnlSendStatus) {
		this.chnlSendStatus = chnlSendStatus;
	}

	public String getAsisCampYn() {
		return asisCampYn;
	}

	public void setAsisCampYn(String asisCampYn) {
		this.asisCampYn = asisCampYn;
	}

	public String getProcSchedDttm() {
		return procSchedDttm;
	}

	public void setProcSchedDttm(String procSchedDttm) {
		this.procSchedDttm = procSchedDttm;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}


}

package com.ecs.domain.rest.ecscontainer.campaign.service.impl;

import com.ecs.domain.rest.ecscontainer.campaign.dao.CampListDAO;
import com.ecs.domain.rest.ecscontainer.campaign.service.CampListService;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("campListService")
public class CampListServiceImpl implements CampListService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CampListServiceImpl.class);

	@Autowired
	private CampListDAO campListDAO;

	public List<ResultMap> selectCampList(Map<String, Object> paramMap, PageBounds pageBounds) {
		if(QuadMaxUserDetailsHelper.isAuthenticated().booleanValue()) {
			paramMap.put("userVO", QuadMaxUserDetailsHelper.getAuthenticatedUser());
		}

		return campListDAO.selectCampList(paramMap, pageBounds);
	}

	/**
	 * 캠페인 전체 조회(페이징 처리 안함)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ResultMap> selectCampList(Map<String, Object> paramMap) {
		if(QuadMaxUserDetailsHelper.isAuthenticated().booleanValue()) {
			paramMap.put("userVO", QuadMaxUserDetailsHelper.getAuthenticatedUser());
		}
		return campListDAO.selectCampList(paramMap);
	}
	
	//기획전환 추가
	@Override
	public int updateCampaignStatus(Map<String, Object> paramMap) {
		return campListDAO.updateCampaignStatus(paramMap);
	}

}


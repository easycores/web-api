package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ecs.domain.rest.ecscontainer.campaign.model.ContainerVO;
import com.ecs.infrastructure.model.ResultMap;

import java.util.Map;

@Mapper
public interface ContainerDAO {

	ContainerVO selectContainerWithPeriod(String projectID);
	List<ResultMap> selectSchedulings(String projectID, String nodeId);
	List<ResultMap> selectTimeEvents(String projectID,String nodeId);
	List<ResultMap> selectResposeTargetings(String projectID,String nodeId);
	List<ResultMap> selectComments(String projectID,String nodeId);

	//CopyCampaign 추가
	ContainerVO selectContainer(String projectID);
	int insertContainerWithSelect(Map<String, Object> paramMap);
	int updateContainer(ContainerVO project);
	int insertContainer(ContainerVO project);
	
}

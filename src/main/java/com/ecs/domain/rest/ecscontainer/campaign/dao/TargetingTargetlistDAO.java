package com.ecs.domain.rest.ecscontainer.campaign.dao;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TargetingTargetlistDAO {

	List<ResultMap> selectTnTargetlist(Map<String, Object> paramMap, PageBounds pageBounds);
	
	List<ResultMap> selectTnTargetlist(Map<String, Object> paramMap);

	List<ResultMap> selectNodeRmTargetlist(Map<String, Object> paramMap);

	List<ResultMap> selectNodeTnTargetlist(Map<String, Object> paramMap);
	

}

package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("campaignPolicyDAO")
@Mapper
public interface CampaignPolicyDAO {

	int insertCampaignPolicyWithSelect(Map<String, Object> paramMap);

}

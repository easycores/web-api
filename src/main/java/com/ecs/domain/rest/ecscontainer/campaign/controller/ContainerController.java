package com.ecs.domain.rest.ecscontainer.campaign.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ecs.domain.rest.ecscontainer.campaign.model.CampListVO;
import com.ecs.domain.rest.ecscontainer.campaign.model.ContainerVO;
import com.ecs.domain.rest.ecscontainer.campaign.service.CampListService;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
import com.ecs.domain.rest.ecscontainer.campaign.service.TargetingTargetlistService;
import com.ecs.global.constant.Const;
import com.ecs.global.i18n.QuadMaxMessageSource;
import com.ecs.global.util.CmmnUtil;
import com.ecs.infrastructure.model.ResultMap;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class ContainerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContainerController.class);

	@Autowired
	private ContainerService containerService;

	@Autowired
	private CampListService campListService;

	@Autowired
	private TargetingTargetlistService targetingTargetlistService;


	@Resource(name = "quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;

	/**
	 * 캠페인 목록 조회
	 *
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes" })
	@GetMapping(value = "/campaigns")
	public ResponseEntity selectCampList(@RequestParam final Map<String, Object> paramMap) {
		String campPeriod = (String) paramMap.get("srchCampPeriod");
		String contPeriod = (String) paramMap.get("srchContPeriod");

		// 등록자 array
		String srchEmpId = (String) paramMap.get("srchEmpId");
		if(null != srchEmpId && !"".equals(srchEmpId)) {
			paramMap.put("srchEmpIds", srchEmpId.split(","));
		}
		// 등록부서 array
		String srchDeptId = (String) paramMap.get("srchDeptId");
		if(null != srchDeptId && !"".equals(srchDeptId)) {
			paramMap.put("srchDeptIds", srchDeptId.split(","));
		}
		// 캠페인계획 array
//		String campGpId = (String) paramMap.get("campGpId");
//		if(null != campGpId && !"".equals(campGpId)) {
//			paramMap.put("campGpIds", campGpId.split(","));
//		}
		// 설계방식 array
		String ctypId = (String) paramMap.get("srchCtypId");
		if(null != ctypId && !"".equals(ctypId)) {
			paramMap.put("srchCtypIds", ctypId.split(","));
		}
		// 캠페인유형 array
		String campTypeCd = (String) paramMap.get("srchCampTypeCd");
		if(null != campTypeCd && !"".equals(campTypeCd)) {
			paramMap.put("srchCampTypeCds", campTypeCd.split(","));
		}
		// 캠페인상태 array
		String cstsId = (String) paramMap.get("srchCstsId");
		if(null != cstsId && !"".equals(cstsId)) {
			paramMap.put("srchCstsIds", cstsId.split(","));
		}

		setPeriod(paramMap, campPeriod);
		setContPeriod(paramMap, contPeriod);
		setSendType(paramMap, (String)paramMap.get("sendType"));

		// 점포사용자일 경우 점포 세션정보로 기본 검색 값 설정
		//containerService.setCampListReadDefaultParam(paramMap);

		ModelAndView modelAndView = new ModelAndView();

		if ("true".equals(paramMap.get("excel")) || "true".equals(paramMap.get("csvDown"))) {
			List<CampListVO> list = CmmnUtil.convertResultMapList(campListService.selectCampList(paramMap), CampListVO.class);

			List<String> recNames = Arrays.asList(((String) paramMap.get("columnIds")).split(","));
			List<String> recTitles = Arrays.asList(((String) paramMap.get("columnNames")).split(","));
			String fileName = (String) paramMap.get("fileName");

			modelAndView.addObject("records", list);
			modelAndView.addObject("recNames", recNames);
			modelAndView.addObject("recTitles", recTitles);
			modelAndView.addObject("fileName", fileName);

			if ("true".equals(paramMap.get("excel"))) {
				modelAndView.setViewName("excelDownloadView");
			} else {
				modelAndView.setViewName("csvDownloadView");
			}
		} else {
			// PageBounds를 사용해 페이징 처리.
			//PageBounds pageBounds = PagingUtil.getPageBoundsDataTable(paramMap);

			List<CampListVO> list = CmmnUtil.convertResultMapList(campListService.selectCampList(paramMap), CampListVO.class);

			//PageList pageList = (PageList) list;

			modelAndView.addObject("data", list);

		}

		return new ResponseEntity<>(modelAndView,HttpStatus.OK);
	}


	/**
	 * 캠페인 정보조회
	 *
	 * @param projectID
	 * @return
	 */
	@GetMapping(value = "/campaigns/{projectID}")
	public ResponseEntity<Map> readContainer(@PathVariable("projectID") String projectID) {
		ArrayList<ContainerVO> list = new ArrayList<>();

//			if (!containerService.isCampInfoReadAuthByCampId(projectID)) {
//				throw new QuadMaxException(quadMaxMessageSource, "MC0195");
//			}

			Optional.ofNullable(containerService.readContainerWithPeriod(projectID)).ifPresent(list::add);
			Map<String, ArrayList<ContainerVO>> map = new HashMap<>();
			map.put("data",list);


			return new ResponseEntity<>(map, HttpStatus.OK);
	}
	/**
	 * 캠페인 실행 우선 순위 수정
	 *
	 * @param campExecRank
	 * @return
	 */
	@PutMapping(value = "/campaigns/{projectID}/camp_exec_ranks")
	public ResponseEntity updateCampExecRank(@RequestParam Map<String,Object> paramMap) {

		Map<String, Integer> map = new HashMap<>();
		map.put("data", containerService.updateCampExecRank(paramMap));

		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	/**
	 * 스케쥴노드 정보 조회
	 *
	 * @param projectID,nodeId
	 * @return
	 */
	@GetMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/Schedulings")
	public ResponseEntity<Map> readSchedulings(@PathVariable("projectID") String projectID,@PathVariable("nodeId") String nodeId) throws JsonProcessingException {
			//테스트 끝난 후에는 주석 풀어야함.
//			if (!containerService.isCampInfoReadAuthByCampId(projectID)) {
//				throw new QuadMaxException(quadMaxMessageSource, "MC0195");
//			}
			List<ResultMap> data = containerService.selectSchedulings(projectID,nodeId);
			Map map = new HashMap<>();

			map.put("data",data);

			return new ResponseEntity<>(map, HttpStatus.OK);
	}

	/**
	 * 실행대기노드 정보 조회
	 *
	 * @param projectID,nodeId
	 * @return
	 */
	@GetMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/timeevents")
	public ResponseEntity<Map> readTimeEvents(@PathVariable("projectID") String projectID,@PathVariable("nodeId") String nodeId) throws JsonProcessingException {
		//테스트 끝난 후에는 주석 풀어야함.
//			if (!containerService.isCampInfoReadAuthByCampId(projectID)) {
//				throw new QuadMaxException(quadMaxMessageSource, "MC0195");
//			}

		List<ResultMap> data = containerService.selectTimeEvents(projectID,nodeId);
		Map map = new HashMap<>();

		map.put("data",data);

		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	/**
	 * 반응결과추출노드 정보 조회
	 *
	 * @param projectID,nodeId
	 * @return
	 */
	@GetMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/response_targetings")
	public ResponseEntity<Map> readResponseTargetings(@PathVariable("projectID") String projectID,@PathVariable("nodeId") String nodeId) throws JsonProcessingException {
		//테스트 끝난 후에는 주석 풀어야함.
//			if (!containerService.isCampInfoReadAuthByCampId(projectID)) {
//				throw new QuadMaxException(quadMaxMessageSource, "MC0195");
//			}

		List<ResultMap> data = containerService.selectResposeTargetings(projectID,nodeId);
		Map map = new HashMap<>();

		map.put("data",data);

		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	/**
	 * 코멘트노드 정보 조회
	 *
	 * @param projectID,nodeId
	 * @return
	 */
	@GetMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/comments")
	public ResponseEntity<Map> readComments(@PathVariable("projectID") String projectID,@PathVariable("nodeId") String nodeId) throws JsonProcessingException {
		//테스트 끝난 후에는 주석 풀어야함.
//			if (!containerService.isCampInfoReadAuthByCampId(projectID)) {
//				throw new QuadMaxException(quadMaxMessageSource, "MC0195");
//			}

		List<ResultMap> data = containerService.selectComments(projectID,nodeId);
		Map map = new HashMap<>();

		map.put("data",data);

		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	/**
	 * 타겟팅 노드 정보 조회
	 *
	 * @param targetlistType
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/targetlists")
	public ResponseEntity<Map> getTnTargetlist(@RequestParam final Map<String, Object> params)  {

		Map map = new HashMap<>();
		List<ResultMap> list = new ArrayList<ResultMap>();

		// 타겟팅 권한여부
		String businessAuthYn = StringUtils.defaultIfEmpty((String) params.get("businessAuthYn"), "N");

		if ("Y".equals(businessAuthYn)) {
			String pagingYn = StringUtils.defaultIfEmpty((String) params.get("pagingYn"), "Y");

			if ("Y".equals(pagingYn)) {
				/*// PageBounds를 사용해 페이징 처리.
				PageBounds pageBounds = PagingUtil.getPageBoundsDataTable(params);

				list = targetingTargetlistService.getTnTargetlist(params, pageBounds);

				PageList pageList = (PageList) list;
				modelAndView.addObject("recordsFiltered", PagingUtil.getPageTotalCount(pageList));*/

			} else {
				list = targetingTargetlistService.getTnTargetlist(params);
			}
		}

		map.put("data", list);


		return new ResponseEntity<>(map,HttpStatus.OK);
	}

	/**
	 * 타겟팅 노드 정보 조회(RM)
	 *
	 * @param targetlistType
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = "/campaigns/{projectID}/nodes/{nodeId}/targetlists")
	public ResponseEntity getTargetingTargetlistSaveData( @RequestParam Map<String, Object> params)  {
		Map map = new HashMap();
		map.put("data", targetingTargetlistService.getTargetingTargetlistSaveData(params));

		return new ResponseEntity(map,HttpStatus.OK);
	}



	/**
	 * 검색조건 중 캠페인기간의 startDate, endDate 를 paramMap에 담는다.
	 * @param paramMap
	 * @param campPeriod
	 */
	private void setPeriod(@RequestParam Map<String, Object> paramMap, String campPeriod) {
		if (campPeriod != null && !"".equals(campPeriod)) {
			String[] period = campPeriod.split(",");

			if (period != null && period.length > 0) {
				paramMap.put("srchStartDate", period[0]);
				if (period.length > 1) {
					paramMap.put("srchEndDate", period[1]);
				}
			}
		}
	}

	/**
	 * 검색조건 중 발송일자의 contStartDate, contEndDate 를 paramMap에 담는다.
	 * @param paramMap
	 * @param campPeriod
	 */
	private void setContPeriod(@RequestParam Map<String, Object> paramMap, String contPeriod) {
		if (contPeriod != null && !"".equals(contPeriod)) {
			String[] period = contPeriod.split(",");

			if (period != null && period.length > 0) {
				paramMap.put("srchContStartDate", period[0]);
				if (period.length > 1) {
					paramMap.put("srchContEndDate", period[1]);
				}
			}
		}
	}

	/**
	 * 검색조건 중 발송구분값 sendType을 실제 조회조건이 될 realTimeF, todayF 로 변환하여 paramMap에 담는다.
	 * @param paramMap
	 * @param campPeriod
	 */
	private void setSendType(@RequestParam Map<String, Object> paramMap, String sendType) {
		if (sendType != null && !"".equals(sendType)) {
			if (Const.CHANNEL_SEND_TYPE_R.equals(sendType)) {
				paramMap.put("realTimeF", "N");
				paramMap.put("todayF", "N");
			} else if (Const.CHANNEL_SEND_TYPE_D.equals(sendType)) {
				paramMap.put("realTimeF", "N");
				paramMap.put("todayF", "Y");
			} else if (Const.CHANNEL_SEND_TYPE_Y.equals(sendType)) {
				paramMap.put("realTimeF", "Y");
				paramMap.put("todayF", "N");
			}
		}
	}

}
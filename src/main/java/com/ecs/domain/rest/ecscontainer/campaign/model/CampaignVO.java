package com.ecs.domain.rest.ecscontainer.campaign.model;

public class CampaignVO {
	/**
	 * 캠페인ID.
	 */
	private String campId;
	/**
	 * 캠페인그룹ID.
	 */
	private String campGpId;
	/**
	 * 상태ID.
	 */
	private String cstsId;
	/**
	 * 분류ID.
	 */
	private int cateDirId;
	/**
	 * 유형ID.
	 */
	private String ctypId;
	/**
	 * 설계방식코드.
	 */
	private String campTypeCd;
	/**
	 * 가장상위캠페인속성(타켓캠페인,실시간캠페인).
	 */
	private String campBaseType;
	/**
	 * 캠페이모드-실행-시뮬.
	 */
	private String campMode;	
	/**
	 * 캠페인명.
	 */
	private String campName;
	/**
	 * 시작일자.
	 */
	private String campSdate;
	/**
	 * 종료일자.
	 */
	private String campEdate;
	/**
	 * 성과측정시작일자.
	 */
	private String analCampSdate;
	/**
	 * 성과측정종료일자.
	 */
	private String analCampEdate;
	/**
	 * 시뮬레이션시작시간.
	 */
	private String simulSdate;
	/**
	 * 시뮬레이션종료시간.
	 */
	private String simulEdate;	
	/**
	 * 캠페인정보.
	 */
	private String campDesc;
	/**
	 * 템플릿ID.
	 */
	private String templateId;
	/**
	 *  [2017-02-15] 캠페인 언어 컬럼추가(다국어처리)
	 */
	private String lang;
	/**
	 * 반복캠페인여부.
	 */
	private String repeatYn;
	/**
	 * 등록자ID.
	 */
	private String regEmpId;
	/**
	 * 등록부서ID.
	 */
	private String regDeptId;
	/**
	 * 등록일자.
	 */
	private String regDate;
	/**
	 * 등록시간.
	 */
	private String regTime;
	/**
	 * 수정자ID.
	 */
	private String updEmpId;
	/**
	 * 수정부서ID.
	 */
	private String updDeptId;
	/**
	 * 수정일자.
	 */
	private String updDate;
	/**
	 * 수정시간.
	 */
	private String updTime;
	/**
	 * 삭제여부.
	 */
	private String delF;
	/**
	 * 본사점포구분.
	 */
	private String hqStrDivCd;
	/**
	 * 업태코드.
	 */
	private String biztpCd;
	/**
	 * 점포코드.
	 */
	private String strCd;
	
	private String extrReqYn;
	
	private String extrReqDesc;
	
	// 안내성캠페인 카테고리 코드
	private String infoCateCd;
	/**
	 * 제조사 캠페인 여부
	 */
	private String mfbizCampF;
	
	private String strNm;
	
	private String remindCampF;
	
	
	public String getCampId() {
		return campId;
	}
	public void setCampId(String campId) {
		this.campId = campId;
	}
	public String getCampGpId() {
		return campGpId;
	}
	public void setCampGpId(String campGpId) {
		this.campGpId = campGpId;
	}
	public String getCstsId() {
		return cstsId;
	}
	public void setCstsId(String cstsId) {
		this.cstsId = cstsId;
	}
	public int getCateDirId() {
		return cateDirId;
	}
	public void setCateDirId(int cateDirId) {
		this.cateDirId = cateDirId;
	}
	public String getCtypId() {
		return ctypId;
	}
	public void setCtypId(String ctypId) {
		this.ctypId = ctypId;
	}
	public String getCampTypeCd() {
		return campTypeCd;
	}
	public void setCampTypeCd(String campTypeCd) {
		this.campTypeCd = campTypeCd;
	}
	public String getCampBaseType() {
		return campBaseType;
	}
	public void setCampBaseType(String campBaseType) {
		this.campBaseType = campBaseType;
	}
	public String getCampMode() {
		return campMode;
	}
	public void setCampMode(String campMode) {
		this.campMode = campMode;
	}
	public String getCampName() {
		return campName;
	}
	public void setCampName(String campName) {
		this.campName = campName;
	}
	public String getCampSdate() {
		return campSdate;
	}
	public void setCampSdate(String campSdate) {
		this.campSdate = campSdate;
	}
	public String getCampEdate() {
		return campEdate;
	}
	public void setCampEdate(String campEdate) {
		this.campEdate = campEdate;
	}
	public String getAnalCampSdate() {
		return analCampSdate;
	}
	public void setAnalCampSdate(String analCampSdate) {
		this.analCampSdate = analCampSdate;
	}
	public String getAnalCampEdate() {
		return analCampEdate;
	}
	public void setAnalCampEdate(String analCampEdate) {
		this.analCampEdate = analCampEdate;
	}
	public String getSimulSdate() {
		return simulSdate;
	}
	public void setSimulSdate(String simulSdate) {
		this.simulSdate = simulSdate;
	}
	public String getSimulEdate() {
		return simulEdate;
	}
	public void setSimulEdate(String simulEdate) {
		this.simulEdate = simulEdate;
	}
	public String getCampDesc() {
		return campDesc;
	}
	public void setCampDesc(String campDesc) {
		this.campDesc = campDesc;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getRepeatYn() {
		return repeatYn;
	}
	public void setRepeatYn(String repeatYn) {
		this.repeatYn = repeatYn;
	}
	public String getRegEmpId() {
		return regEmpId;
	}
	public void setRegEmpId(String regEmpId) {
		this.regEmpId = regEmpId;
	}
	public String getRegDeptId() {
		return regDeptId;
	}
	public void setRegDeptId(String regDeptId) {
		this.regDeptId = regDeptId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getUpdEmpId() {
		return updEmpId;
	}
	public void setUpdEmpId(String updEmpId) {
		this.updEmpId = updEmpId;
	}
	public String getUpdDeptId() {
		return updDeptId;
	}
	public void setUpdDeptId(String updDeptId) {
		this.updDeptId = updDeptId;
	}
	public String getUpdDate() {
		return updDate;
	}
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	public String getUpdTime() {
		return updTime;
	}
	public void setUpdTime(String updTime) {
		this.updTime = updTime;
	}
	public String getDelF() {
		return delF;
	}
	public void setDelF(String delF) {
		this.delF = delF;
	}
	public String getHqStrDivCd() {
		return hqStrDivCd;
	}
	public void setHqStrDivCd(String hqStrDivCd) {
		this.hqStrDivCd = hqStrDivCd;
	}
	public String getBiztpCd() {
		return biztpCd;
	}
	public void setBiztpCd(String biztpCd) {
		this.biztpCd = biztpCd;
	}
	public String getStrCd() {
		return strCd;
	}
	public void setStrCd(String strCd) {
		this.strCd = strCd;
	}
	public String getExtrReqYn() {
		return extrReqYn;
	}
	public void setExtrReqYn(String extrReqYn) {
		this.extrReqYn = extrReqYn;
	}
	public String getExtrReqDesc() {
		return extrReqDesc;
	}
	public void setExtrReqDesc(String extrReqDesc) {
		this.extrReqDesc = extrReqDesc;
	}
	public String getInfoCateCd() {
		return infoCateCd;
	}
	public void setInfoCateCd(String infoCateCd) {
		this.infoCateCd = infoCateCd;
	}
	public String getMfbizCampF() {
		return mfbizCampF;
	}
	public void setMfbizCampF(String mfbizCampF) {
		this.mfbizCampF = mfbizCampF;
	}
	public String getStrNm() {
		return strNm;
	}
	public void setStrNm(String strNm) {
		this.strNm = strNm;
	}
	public String getRemindCampF() {
		return remindCampF;
	}
	public void setRemindCampF(String remindCampF) {
		this.remindCampF = remindCampF;
	}
	
}

package com.ecs.domain.rest.ecscontainer.scenario.model.api;

import lombok.Data;

public class EcsContainerGetRequest {

    @Data
    public static class RequestListDTO {
        private String sourceId;
        private String campName;
        private String campPeriod;
    }

}

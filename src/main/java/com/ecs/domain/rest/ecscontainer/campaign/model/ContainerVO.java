package com.ecs.domain.rest.ecscontainer.campaign.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ContainerVO {

	@JsonProperty(required = true)
	private String campId;                      //캠페인ID
	@JsonProperty(required = true)
	private String campName;                    //캠페인명
	@JsonProperty(required = true)
	private String ctypId;                      //설계유형ID
	@JsonProperty(required = true)
	private String cstsId;                      //캠페인상태ID
	@JsonProperty(required = true)
	private String campSdate;                   //캠페인시작일자(YYYYMMDD)
	@JsonProperty(required = true)
	private String campEdate;                   //캠페인종료일자(YYYYMMDD)
	@JsonProperty(required = true)
	private String sccssCndtnCd;				//성공조건
	@JsonProperty(required = true)
	private String campDesc;                    //비고
	@JsonProperty(required = true)
	private String remindCampF;                    //재발송여부
	@JsonProperty(required = true)
	private String cateDirId;                    //캠페인분류

	public String getCampId() {
		return campId;
	}

	public void setCampId(String campId) {
		this.campId = campId;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public String getCtypId() {
		return ctypId;
	}

	public void setCtypId(String ctypId) {
		this.ctypId = ctypId;
	}

	public String getCstsId() {
		return cstsId;
	}

	public void setCstsId(String cstsId) {
		this.cstsId = cstsId;
	}

	public String getCampSdate() {
		return campSdate;
	}

	public void setCampSdate(String campSdate) {
		this.campSdate = campSdate;
	}

	public String getCampEdate() {
		return campEdate;
	}

	public void setCampEdate(String campEdate) {
		this.campEdate = campEdate;
	}

	public String getSccssCndtnCd() {
		return sccssCndtnCd;
	}

	public void setSccssCndtnCd(String sccssCndtnCd) {
		this.sccssCndtnCd = sccssCndtnCd;
	}

	public String getCampDesc() {
		return campDesc;
	}

	public void setCampDesc(String campDesc) {
		this.campDesc = campDesc;
	}

	public String getRemindCampF() {
		return remindCampF;
	}

	public void setRemindCampF(String remindCampF) {
		this.remindCampF = remindCampF;
	}

	public String getCateDirId() {
		return cateDirId;
	}

	public void setCateDirId(String cateDirId) {
		this.cateDirId = cateDirId;
	}



	/**
	 * 캠페인ID
	 */
	@JsonProperty(required = true)
	private String projectID;
	/**
	 * 프로젝트명
	 */
	@JsonProperty(required = true)
	private String projectname;
	/**
	 * 캠페인구분 (TARGET / EBM)
	 */
	@JsonProperty(required = true)
	private String projectcategory;
	/**
	 * 캠페인유형 (containers / flowchart)
	 */
	@JsonProperty(required = true)
	private String projecttype;
	/**
	 * 캠페인상태 (csts_id)
	 */
	@JsonProperty(required = true)
	private String projectstatus;
	/**
	 * 설명
	 */
	@JsonProperty(required = true)
	private String projectdesc;
	/**
	 * 등록자
	 */
	@JsonProperty(required = true)
	private String projectowner;
	/**
	 * 생성일
	 */
	private String projectcreated;
	/**
	 * 사용자
	 */
	@JsonProperty(required = true)
	private String projectuserid;
	/**
	 * 수정일
	 */
	private String projectmodified;
	/**
	 * 버전번호
	 */
	private String projectVersion;
	/**
	 * 프로젝트 전체 JSON
	 */
	@JsonProperty(required = true)
	private ObjectNode projectjson;
	/**
	 * 프로젝트 전체 JSON 문자열 전환
	 */
	private String projectstr;
	/**
	 * 캠페인 시작일
	 */
	private String projectsdate;
	/**
	 * 캠페인 종료일
	 */
	private String projectedate;
	/**
	 * 캠페인 분류ID
	 */
	private String projectcatedirid;
	/**
	 * 저장 노드 IDs
	 */
	private JsonNode savenodes;
	/**
	 * 삭제 노드 IDs
	 */
	private JsonNode deletenodes;
	/**
	 * 상품 JSON
	 */
	private Map<String, Object> prodInfo;
	/**
	 * 분석기간 시작일
	 */
	private String projectanalsdate;
	/**
	 * 분석기간 종료일
	 */
	private String projectanaledate;

	
	public String getProjectID() {
		return projectID;
	}
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = replaceQuote(projectname);
	}
	public String getProjectcategory() {
		return projectcategory;
	}
	public void setProjectcategory(String projectcategory) {
		this.projectcategory = projectcategory;
	}
	public String getProjecttype() {
		return projecttype;
	}
	public void setProjecttype(String projecttype) {
		this.projecttype = projecttype;
	}
	public String getProjectstatus() {
		return projectstatus;
	}
	public void setProjectstatus(String projectstatus) {
		this.projectstatus = projectstatus;
	}
	public String getProjectdesc() {
		return projectdesc;
	}
	public void setProjectdesc(String projectdesc) {
		this.projectdesc = replaceQuote(projectdesc);
	}
	public String getProjectowner() {
		return projectowner;
	}
	public void setProjectowner(String projectowner) {
		this.projectowner = projectowner;
	}
	public String getProjectcreated() {
		return projectcreated;
	}
	public void setProjectcreated(String projectcreated) {
		this.projectcreated = projectcreated;
	}
	public String getProjectuserid() {
		return projectuserid;
	}
	public void setProjectuserid(String projectuserid) {
		this.projectuserid = projectuserid;
	}
	public String getProjectmodified() {
		return projectmodified;
	}
	public void setProjectmodified(String projectmodified) {
		this.projectmodified = projectmodified;
	}
	public String getProjectVersion() {
		return projectVersion;
	}
	public void setProjectVersion(String projectVersion) {
		this.projectVersion = projectVersion;
	}
	public ObjectNode getProjectjson() {
		return projectjson;
	}
	public void setProjectjson(ObjectNode projectjson) throws IOException {
		ObjectMapper mapper = new ObjectMapper();

		this.projectjson = projectjson;
		setProjectstr(mapper.writeValueAsString(projectjson));
	}
	public String getProjectstr() {
		return projectstr;
	}
	public void setProjectstr(String projectstr) throws IOException {
		ObjectMapper mapper = new ObjectMapper();

		this.projectstr = projectstr;
		this.projectjson = mapper.readValue(replaceQuote(projectstr), ObjectNode.class);
	}
	public String getProjectsdate() {
		return projectsdate;
	}
	public void setProjectsdate(String projectsdate) {
		this.projectsdate = projectsdate;
	}
	public String getProjectedate() {
		return projectedate;
	}
	public void setProjectedate(String projectedate) {
		this.projectedate = projectedate;
	}
	public String getProjectcatedirid() {
		return projectcatedirid;
	}
	public void setProjectcatedirid(String projectcatedirid) {
		this.projectcatedirid = projectcatedirid;
	}
	public JsonNode getSavenodes() {
		return savenodes;
	}
	public void setSavenodes(JsonNode savenodes) {
		this.savenodes = savenodes;
	}
	public JsonNode getDeletenodes() {
		return deletenodes;
	}
	public void setDeletenodes(JsonNode deletenodes) {
		this.deletenodes = deletenodes;
	}
	public Map<String, Object> getProdInfo() {
		return prodInfo;
	}
	public void setProdInfo(Map<String, Object> prodInfo) {
		this.prodInfo = prodInfo;
	}
	
	public String replaceQuote(String str) {
		return str.replace("\\\\\"", "\\\"")
				.replace("\\\\\"", "\\\"")
				.replace("\\\\t", "\\t")
				.replace("\\\\n", "\\n")
				.replace("\\\\r", "\\r")
				.replace("\\\\&", "\\&")
				.replace("\''", "\'");
	}
	public String getProjectanalsdate() {
		return projectanalsdate;
	}
	public void setProjectanalsdate(String projectanalsdate) {
		this.projectanalsdate = projectanalsdate;
	}
	public String getProjectanaledate() {
		return projectanaledate;
	}
	public void setProjectanaledate(String projectanaledate) {
		this.projectanaledate = projectanaledate;
	}
}

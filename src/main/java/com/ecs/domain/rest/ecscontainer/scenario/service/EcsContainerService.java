package com.ecs.domain.rest.ecscontainer.scenario.service;

import java.io.IOException;
import java.util.List;

import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

public interface EcsContainerService {

	public ResultMap selectCampaign(String campId);

	//승인/반려 처리 추가
	String getAliveQueryServerUrl();
	List<String> getQueryServerUrlList();
	public String getMasterServerURL() throws IOException;
}

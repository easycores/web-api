package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//승인요청/자가승인 추가
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;

@Mapper
public interface CampaignDAO {
	int updateCampExecRank(Map<String, Object> paramMap);
	
	//CopyCampaign 추가
	int insertCampaignWithSelect(Map<String, Object> paramMap);
	
	//승인요청/자가승인 추가
	CampaignVO selectCampaign(String campId);	
	
	//승인/반려 처리 추가
	int updateAppointDatetimeStatus(Map<String, Object> paramMap);
}

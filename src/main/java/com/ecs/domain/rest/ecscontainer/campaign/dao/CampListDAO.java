package com.ecs.domain.rest.ecscontainer.campaign.dao;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 캠페인 조회 DAO
 */
@Mapper
public interface CampListDAO {
	/**
	 * 캠페인 조회
	 * @param paramMap
	 * @param pageBounds
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectCampList(Map<String, Object> paramMap, PageBounds pageBounds);

	/**
	 * 캠페인 전체 조회(페이징 처리 안함)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectCampList(Map<String, Object> paramMap);

	//기획전환 추가
	/**
	 * 캠페인상태 수정
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	int updateCampaignStatus(Map<String, Object> paramMap);
}

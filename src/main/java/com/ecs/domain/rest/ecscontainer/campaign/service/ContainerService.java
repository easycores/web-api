package com.ecs.domain.rest.ecscontainer.campaign.service;

import java.util.List;
import java.util.Map;

import com.ecs.domain.rest.ecscontainer.campaign.model.ContainerVO;
import com.ecs.infrastructure.model.ResultMap;

//CopyCampaign 추가
import com.ecs.global.exception.QuadMaxException;
import java.io.IOException;
//승인요청/자가승인 추가
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;

public interface ContainerService {


	void setCampListReadDefaultParam(Map<String, Object> paramMap);
	void setCampListReadParam(Map<String, Object> paramMap, String[] paramNames);

	ContainerVO readContainerWithPeriod(String projectID);

	List<ResultMap> selectSchedulings(String projectID, String nodeId);
	List<ResultMap> selectTimeEvents(String projectID, String nodeId);
	List<ResultMap> selectResposeTargetings(String projectID, String nodeId);
	List<ResultMap> selectComments(String projectID, String nodeId);


	int updateCampExecRank(Map<String, Object> paramMap);
	
	
	//CopyCampaign 추가
	ContainerVO readContainer(String projectID);
	void saveAsContainer(Map<String, Object> paramMap) throws QuadMaxException, IOException;
	void saveUi(String projectID, ContainerVO project) throws QuadMaxException;
	int getCampExecRank(String campTypeCd, String remindCampF);
	
	//승인요청/자가승인 추가
	CampaignVO readCampaign(String projectID);
	String checkApproval(CampaignVO campInfo, String aprvStatCd, List<Map<String, Object>> aprvList);
	String checkChannelOffer(CampaignVO campInfo);
	String checkNodeCount(CampaignVO campInfo);
}

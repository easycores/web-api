package com.ecs.domain.rest.ecscontainer.scenario.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

//CopyCampaign.do 에서 사용안함
//import com.ecs.quadmax.infrastructure.model.ResultMap;

import com.ecs.domain.common.login.model.UserVO;
//import com.ecs.quadmax.login.model.UserVO;

//CopyCampaign.do 에서 사용안함
//import com.ecs.quadmax.common.service.DynamicSqlService;

import com.ecs.domain.rest.common.service.MakingIdService;
//import com.ecs.quadmax.common.service.MakingIdService;

//CopyCampaign.do 에서 사용안함
//import com.ecs.quadmax.ecscontainer.campaign.service.TargetingTargetlistService;

import com.ecs.domain.rest.ecscontainer.campaign.model.ContainerVO;
//import com.ecs.quadmax.ecscontainer.campaign.model.ContainerVO;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
import com.ecs.domain.rest.ecscontainer.scenario.model.api.EcsContainerGetRequest;
import com.ecs.domain.rest.ecscontainer.scenario.model.api.EcsContainerGetResponse;

//CopyCampaign.do 에서 사용안함
//import com.ecs.quadmax.ecscontainer.campaign.service.TargetingTargetlistService;

//import com.ecs.quadmax.ecscontainer.scenario.model.EcsOfferVO;
//import com.ecs.quadmax.ecscontainer.scenario.model.EcsPromotionVO;

import com.ecs.domain.rest.ecscontainer.scenario.service.EcsContainerService;
//import com.ecs.quadmax.common.Const;
import com.ecs.global.i18n.QuadMaxMessageSource;
//import com.ecs.quadmax.common.QuadMaxMessageSource;
//import com.ecs.quadmax.common.util.PagingUtil;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
//import com.ecs.quadmax.common.util.QuadMaxUserDetailsHelper;
import com.ecs.global.util.Util;
import com.ecs.infrastructure.model.EcsResponse;

/**
 * 캠페인 조회
 *
 * @author h2y
 */
@RestController
public class EcsContainerController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(TestConController.class);
	
	@Autowired
	private EcsContainerService ecsContainerService;
	
	@Autowired
	private MakingIdService makingIdService;
	
	@Autowired
	private ContainerService containerService;	

	@Resource(name="quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;	

	/*
	 * 캠페인 복사
	 * @param model
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/CopyCampaign")
	public EcsResponse<List<EcsContainerGetResponse.ResponseListDTO>> copyCampaign(EcsContainerGetRequest.RequestListDTO reqDto) {	
	
		Map<String, Object> paramMap = new HashMap<>();

		List<EcsContainerGetResponse.ResponseListDTO> voList = new ArrayList<>();
		
		LOGGER.info(" >> reqDto => {}", reqDto);
		
		if (reqDto.getSourceId() == null || "".equals(reqDto.getSourceId())) {
			return new EcsResponse(HttpStatus.NO_CONTENT);
		}
		
    	paramMap.put("sourceId", reqDto.getSourceId());
    	//paramMap.put("campName", "테스트 " + DateUtil.getCurrentDateTime());
    	//paramMap.put("copyCampName", paramMap.get("campName"));
    	paramMap.put("copyCampName", reqDto.getCampName());
    	//paramMap.put("campPeriod", "20220517,20220520");
    	
    	String tmpDateStr = Util.isNull((String) reqDto.getCampPeriod(), "");
    	String[] tmpDate = tmpDateStr.split(",");		
    	paramMap.put("copyCampSdate", tmpDate[0]);
    	paramMap.put("copyCampEdate", tmpDate[1]);
    	
    	LOGGER.info("\n >> copyCampaign paramMap => {}", paramMap);
		UserVO user = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();

		//String campId = Util.isNull((String) paramMap.get("campId"), "");
		String campId = Util.isNull((String) paramMap.get("sourceId"), "");
		
		LOGGER.info("\n >> copyCampaign campId => {}", campId);
		
		ContainerVO vo = containerService.readContainer(campId);

		Map<String, Object> map = new HashMap<>();
		
		//TODO user 코멘트 처리 주석 해제할것
//		if ("03".equals(user.getHqStrDivCd())) {
//			ResultMap campInfo = ecsContainerService.selectCampaign(campId);
//			if ("02".equals(campInfo.get("hqStrDivCd"))) {
//				//model.addAttribute("returnMessage", quadMaxMessageSource.getMessage("MC0198"));
//				//model.addAttribute("success", false);
//				//return Const.JSON_VIEW;
//				
//				map.put("returnMessage", quadMaxMessageSource.getMessage("MC0198"));
//				map.put("success", false);
//				
//				return new ResponseEntity<>(map, HttpStatus.OK);
//				
//			}
//		}

		String campBaseType = vo.getProjectcategory();
		String newId = "";
		String id = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		String ebmId = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		if (campBaseType.equals("TARGET")) newId = makingIdService.getCampId("CMP", id, 9, 4);
		else newId = makingIdService.getCampId("EBM", ebmId, 9, 4);

		vo.setCampId(newId);

		try {
			paramMap.put("oldId", campId);
			paramMap.put("newId", newId);
			paramMap.put("cstsId", "001");

			LOGGER.info("\n >>containerService.saveAsContainer paramMap ==> {}", paramMap);
			
		    // 호출 대상 매서드
			containerService.saveAsContainer(paramMap);
		} catch (Exception e) {
		  // 예외처리
		  LOGGER.info("\n >> FAILED to containerService.saveAsContainer");
		}

//		map.put("oldId", campId);
//		map.put("newId", newId);
//		map.put("cstsId", "001");
		
		//TODO user 코멘트 처리 주석 해제할것
//		map.put("hqStrDivCd", user.getHqStrDivCd());
//		map.put("strCd", user.getStrCd());
//		map.put("biztpCd", user.getBiztpCd());
		
//		map.put("campId", newId);
//		map.put("success", true);		
		
		map.put("status", "");
		map.put("code", "");
		map.put("message", "");
		map.put("messageDev", "");
		map.put("data", vo);

		LOGGER.info("\n >> copyCampaign map => {}", map);

		LOGGER.info("\n >> copyCampaign newId => {}", newId);

		//ResponseListDTO tmpDTO = new ResponseListDTO();
		
		EcsContainerGetResponse.ResponseListDTO tmpDTO = new EcsContainerGetResponse.ResponseListDTO();
		
		tmpDTO.setCampId(newId);
		
		LOGGER.info("\n >> copyCampaign tmpDTO.getCampId() => {}", tmpDTO.getCampId());
		
		voList.add(tmpDTO);
		
		LOGGER.info("\n >> copyCampaign voList => {}", voList);
		
		return new EcsResponse(voList, HttpStatus.OK);
	}

}

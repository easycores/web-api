package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//승인요청/자가승인 추가
import com.ecs.infrastructure.model.ResultMap;

//@MapperScan("cellContentDAO")
@Mapper
public interface CellContentDAO {

	//CopyCampaign 추가
	int insertCellContentWithSelect(Map<String, Object> paramMap);

	//승인요청/자가승인 추가
	String checkChnlCount(Map<String, Object> paramMap);
	List<ResultMap> selectChnlDistribSendDateInfoList(Map<String, Object> paramMap);
}

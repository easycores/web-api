package com.ecs.domain.rest.ecscontainer.campaign.service.impl;



import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CampaignDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CampaignPolicyDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CellContentDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CellContentRefDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CellContentUrlDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CellDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.ContainerDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.SqlDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.TargetingDAO;
import com.ecs.domain.rest.ecscontainer.campaign.model.ContainerVO;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
import com.ecs.domain.rest.product.dao.ProductDAO;
import com.ecs.global.constant.Const;

//CopyCampaign 추가
import com.ecs.global.exception.QuadMaxException;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
import com.ecs.infrastructure.model.ResultMap;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

//승인요청/자가승인 추가
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;
import com.ecs.global.i18n.QuadMaxMessageSource;
import com.ecs.global.util.DateUtil;
import com.ecs.domain.rest.approve.dao.NewApproveDAO;

@Service("containerService")
public class ContainerServiceImpl implements ContainerService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContainerServiceImpl.class);


	@Autowired
	private CampaignDAO campaignDAO;

	@Autowired
	private ContainerDAO containerDAO;

	//CopyCampaign 추가
	@Autowired
	private CampaignPolicyDAO campaignPolicyDAO;
	@Autowired
	private TargetingDAO targetingDAO;	
	@Autowired
	private CellDAO cellDAO;
	@Autowired
	private CellContentDAO cellContentDAO;
	@Autowired
	private CellContentUrlDAO cellContentUrlDAO;	
	@Autowired
	private CellContentRefDAO cellContentRefDAO;
	@Autowired
	private SqlDAO sqlDAO;
	@Autowired
	private ProductDAO productDAO;

	@Autowired
	private NewApproveDAO newApproveDAO;
	
	@Resource(name="quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;

	@Override
	public void setCampListReadDefaultParam(Map<String, Object> paramMap) {
		String[] paramNames = new String[] {
				"srchHqStrDivCd"
				, "srchBiztpCd"
				, "srchStrCd"
				, "srchStrCds"};
		setCampListReadParam(paramMap, paramNames);
	}

	@Override
	public void setCampListReadParam(Map<String, Object> paramMap, String[] paramNames) {
		UserVO user = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();

		if (user.getMathId() == 10000 || "02".equals(user.getHqStrDivCd())) {
			return;
		}
		if (paramNames == null) {
			return;
		}
		for (String name : paramNames) {
			if ("hqStrDivCd".equals(name) || "srchHqStrDivCd".equals(name)) {
				paramMap.put(name, user.getHqStrDivCd());
			}
			if ("biztpCd".equals(name) || "srchBiztpCd".equals(name)) {
				paramMap.put(name, user.getBiztpCd());
			}
			if ("strCd".equals(name) || "srchStrCd".equals(name)) {
				paramMap.put(name, user.getStrCd());
			}
			if ("srchStrCds".equals(name)) {
				paramMap.put(name, new String[] {user.getStrCd()});
			}
		}
	}

	/**
	 * T_CONTINAERS 조회
	 * (+ 캠페인 기간 & 캠페인 카테고리 유형)
	 */
	@Override
	public ContainerVO readContainerWithPeriod(String projectID) {
		return containerDAO.selectContainerWithPeriod(projectID);
	}
	@Override
	public List<ResultMap> selectSchedulings(String projectID, String nodeId){
		return  containerDAO.selectSchedulings(projectID,nodeId);
	}

	@Override
	public  List<ResultMap> selectTimeEvents(String projectID, String nodeId) {
		return  containerDAO.selectTimeEvents(projectID,nodeId);
	}
	@Override
	public  List<ResultMap> selectResposeTargetings(String projectID, String nodeId) {
		return  containerDAO.selectResposeTargetings(projectID,nodeId);
	}
	@Override
	public  List<ResultMap> selectComments(String projectID, String nodeId) {
		return  containerDAO.selectComments(projectID,nodeId);
	}
	@Override
	public int updateCampExecRank(Map<String, Object> paramMap) {
		return campaignDAO.updateCampExecRank(paramMap);
	}
	
	//CopyCampaign 추가
	/**
	 * T_CONTAINERS 조회
	 */
	@Override
	public ContainerVO readContainer(String projectID) {
		return containerDAO.selectContainer(projectID);
	}
	
	//CopyCampaign 추가
	/*
	 * 다른 이름으로 저장
	 * 템플릿을 기반으로 캠페인 저장 (paramMap - templateId 추가)
	 * UI 저장(T_COTAINERS), 노드 정보 저장(CAMPINFO, TARGETING, CHANNELOFFER)
	 */
	@Override
	public void saveAsContainer(Map<String, Object> paramMap) throws QuadMaxException, IOException {
		// T_CONTAINERS 복사
		paramMap.put("projectstatus", "001");
		paramMap.put("projectVersion", "1");
		
		if (containerDAO.insertContainerWithSelect(paramMap) <= 0) {
			LOGGER.error("FAILED to copy T_CONTAINERS");
			throw new QuadMaxException("FAILED to copy T_CONTAINERS");
		}
		
		/////////////////////////////////////////////////////수정해야함
		String newId = (String)paramMap.get("newId");

		ContainerVO project = containerDAO.selectContainer(newId);
		ObjectMapper mapper = new ObjectMapper();
		String projectstr = project.getProjectstr();
		ObjectNode projectjson = mapper.readValue(projectstr, ObjectNode.class);
		JsonNode nodeInfo = Optional.of(projectjson.get("node_info")).get();
		
		for(int i=0; i<nodeInfo.size(); i++) {
			JsonNode curNode = Optional.of(nodeInfo.get(i)).get();
			String nodeType = Optional.of(curNode.get("nodeType")).map(JsonNode::textValue).orElseThrow(NullPointerException::new);
			if("CHANNELOFFER".equals(nodeType)) {
				JsonNode curChannels = Optional.of(curNode.get("channels")).get();
				for(int j=0; j<curChannels.size(); j++) {
					ObjectNode curChannelTemp = (ObjectNode)Optional.of(curChannels.get(j)).get();
					ObjectNode curChannel = (ObjectNode)Optional.of(curChannels.get(j).get("channel")).get();
//					발송구분 세팅 관련
					curChannel.remove("contDate");
					curChannel.remove("contHour");
					curChannel.remove("contMin");
					curChannel.remove("contTime");
					curChannel.remove("realTimeF");
					curChannel.remove("todayF");
					curChannel.put("realTimeF", "N");
					curChannel.put("todayF", "N");
					
//					첨부파일 삭제 관련
					curChannel.remove("msgImgHtml");
					curChannel.remove("fileId1");
					curChannel.remove("fileName1");
					curChannel.remove("filePath1");
					curChannel.remove("fileId2");
					curChannel.remove("fileName2");
					curChannel.remove("filePath2");
					curChannel.remove("fileId3");
					curChannel.remove("fileName3");
					curChannel.remove("filePath3");
					curChannel.remove("hpFileId");
					curChannel.remove("hpFileName");
					curChannel.remove("contentsFileId");
					curChannel.remove("contentsFilePath");
					
					curChannelTemp.remove("channel");
					curChannelTemp.set("channel",(JsonNode)curChannel);

					((ObjectNode)projectjson.get("node_info").get(i).get("channels").get(j)).setAll(curChannelTemp);
				}
			}
		}
		project.setProjectjson(projectjson);
		
		
		saveUi(newId, project);
		
		/////////////////////////////////////////////////////
		// 노드 복사 (CAMPINFO, TARGETING, CHANNELOFFER)
		
		String campType = (String) paramMap.get("campType");
		String remindCampF = (String) paramMap.get("remindCampF");
		
		//TODO user 코멘트 처리 주석 해제할것
		int campExecRank = 0;
		//int campExecRank = getCampExecRank(campType, remindCampF);
		
		LOGGER.debug("@@@@@@@campType : {}", campType);
		LOGGER.debug("@@@@@@@campExecRank : {}", campExecRank);
		
		paramMap.put("campExecRank", campExecRank);
		
		if (campaignDAO.insertCampaignWithSelect(paramMap) <= 0) {
			throw new QuadMaxException("FAILED to copy T_CAMPAIGN");
		}
		
		campaignPolicyDAO.insertCampaignPolicyWithSelect(paramMap);
		targetingDAO.insertTargetingWithSelect(paramMap);
		cellDAO.insertCellWithSelect(paramMap);
		cellContentDAO.insertCellContentWithSelect(paramMap);
		cellContentUrlDAO.insertCellContentUrlWithSelect(paramMap);
		cellContentRefDAO.insertCellContentRefWithSelect(paramMap);
		// 상품 복사
		productDAO.insertCampSuccssCndtnPrdtWithSelect(paramMap);
		// sql노드 복사
		sqlDAO.insertCampSqlWithSelect(paramMap);
		
	}	

	//CopyCampaign 추가
	/**
	 * T_CONTAINERS 저장
	 * UI
	 * @param projectID
	 * @param project
	 * @throws QuadMaxException 
	 */
	@Override
	public void saveUi(String projectID, ContainerVO project) throws QuadMaxException {
		int flag = -1;
		
		LOGGER.info("\n saveUi start \n");
		
		if (containerDAO.selectContainer(projectID) != null) {
			flag = containerDAO.updateContainer(project);
		} else {
			flag = containerDAO.insertContainer(project);
		}
		
		LOGGER.info("\n saveUi after \n");
		
		if (flag <= 0) {
			throw new QuadMaxException("FAILED to insert/update T_CONTAINERS");
		}
	}	
	
	//CopyCampaign 추가
	@Override
	public int getCampExecRank(String campTypeCd, String remindCampF) {
		UserVO user = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();
		String hqStrDivCd = user.getHqStrDivCd();
		if ("IC".equals(campTypeCd) || ("Y".equals(remindCampF) && hqStrDivCd.equals(Const.DIV_CD_HQ))) {
			return 0;
		} else if (hqStrDivCd.equals(Const.DIV_CD_HQ)) {
			return 2;
		} else {
			return 1;
		}
	}	
	
	//승인요청/자가승인 추가
	/**
	 * T_CAMPAIGN 조회
	 */
	@Override
	public CampaignVO readCampaign(String projectID) {
		return campaignDAO.selectCampaign(projectID);
	}
	
	//승인요청/자가승인 추가
	@Override
	public String checkApproval(CampaignVO campInfo, String aprvStatCd, List<Map<String, Object>> aprvList) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("campId", campInfo.getCampId());
		
		String check = "";
		
		// 처리가능한 상태 표기
		if(!"x".equals(aprvStatCd) && !"1".equals(aprvStatCd) && !"2".equals(aprvStatCd) && !"5".equals(aprvStatCd)) {
			LOGGER.debug("aprvStatCd : {}", aprvStatCd);
			return quadMaxMessageSource.getMessage("MC0177");	// 승인요청 혹은 자가승인요청만 처리됩니다.
		}
		
		if(Integer.parseInt(DateUtil.getYYYYMMDD(new Date())) > Integer.parseInt(campInfo.getCampSdate())) {
			return "[승인요청불가]캠페인 시작일이 이미 경과되었습니다.";	//[승인요청불가]캠페인 시작일이 이미 경과되었습니다.
		}
		
		//기존 승인정보 확인
		String prevAprvCd = null;
		prevAprvCd = newApproveDAO.selectPrevAprvCd(paramMap);
		if(null != prevAprvCd && !"".equals(prevAprvCd)) {
			return "[처리불가]이미 승인요청된 건입니다.";	//[처리불가] 이미 승인요청된 건입니다.
		}
		
		// 설정 승인자 1명인지 체크
		if (aprvList == null || aprvList.size() < 1 ) {
			return "승인자는 최소 1명 이상이어야 합니다.";
		}
		
		// 기획설계상태인지 체크 & 승인요청자와 기획자가 같은지 체크
		//TODO user 코멘트 처리 주석 해제할것
		///web-api/src/main/resources/mybatis/mapper/rest/approval/newApproveMapper.xml 
		//id="checkCampCstsId"에 <!--		   AND STR_CD = #{userVO.strCd} --> 주석처리해제
		check = newApproveDAO.checkCampCstsId(paramMap);
		if ("N".equals(check)) {
			return "[처리불가]기획 상태일 경우에만 승인요청이 가능합니다.";
		}
		
		return "Y";
	}
	
	//승인요청/자가승인 추가
	@Override
	public String checkChannelOffer(CampaignVO campInfo) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("campId", campInfo.getCampId());
		
		// 고객군당 채널이 1개 이상인지 체크
		String check = cellContentDAO.checkChnlCount(paramMap);
		if ("N".equals(check)) {
			return "고객군당 채널은 1개 이상 등록해야 합니다.";
		}
		
		/*
		 *  쿠폰제공캠페인일 경우 
		 */
//		if ("PC".equals(campInfo.getCstsId())) {
//			
//			LOGGER.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//			LOGGER.debug("캠페인 유형 : 홍보캠페인");
//			LOGGER.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//			
//			// 바코드MMS 쿠폰이 1개인지 체크
//			check = cellContentDAO.checkChnlCountForCoupon(paramMap);
//			if ("N".equals(check)) {
//				return "바코드MMS는 쿠폰을 1개만 등록가능합니다.";
//			}
//		}
		
//		List<ResultMap> sendDateInfoList = cellContentDAO.selectChnlSendDateInfoList(paramMap);
		List<ResultMap> sendDateInfoList = cellContentDAO.selectChnlDistribSendDateInfoList(paramMap);
		
		LOGGER.info("\n >> cellContentDAO.selectChnlDistribSendDateInfoList => {}", sendDateInfoList);
		
		if (sendDateInfoList == null || sendDateInfoList.isEmpty()) {
			return "채널 설정이 잘못되었습니다. (채널발송분배 예약발송일시를 확인하세요.)";
		}
		for (ResultMap info : sendDateInfoList) {
			String contDatetimeStr = (String) info.get("contDatetime");
			String contDate = (String) info.get("contDate");
			String contTime = (String) info.get("contTime");
			String realTimeF = (String) info.get("realTimeF");
			String todayF = (String) info.get("todayF");
			int resvMinDay = (int) Optional.ofNullable(Integer.parseInt((String)info.get("resvMinDay"))).orElse(0); 			// 예약발송등록최소일수
//			int resvCloseTime = (int) Optional.ofNullable(Integer.parseInt((String)info.get("resvCloseTime"))).orElse(0); 		// 예약발송등록마감시간
//			int todayAmSendTime = (int) info.get("todayAmSendTime"); 															// 당일발송오전발송시간(점포)
//			int todayAmCloseTime = (int) Optional.ofNullable(Integer.parseInt((String)info.get("todayAmCloseTime"))).orElse(0); // 당일발송오전등록마감시간
//			int todayPmSendTime = (int) info.get("todayPmSendTime"); 															// 당일발송오후발송시간(점포)
//			int todayPmCloseTime = (int) Optional.ofNullable(Integer.parseInt((String)info.get("todayPmCloseTime"))).orElse(0); // 당일발송오후등록마감시간
//			int realCloseTime = (int) Optional.ofNullable(Integer.parseInt((String)info.get("realCloseTime"))).orElse(0); 		// 즉시발송즉시발송등록마감시간
			String chnlName = (String) info.get("chnlName");
			long distribCustCnt = (Long) Optional.ofNullable(info.get("distribCustCnt")).orElse(0); 	// 발송분배건수
			
			// 발송예정일시 필수 체크
			if (StringUtils.isEmpty(contDate) || StringUtils.isEmpty(contTime)) {
				return chnlName + "채널 - 예약발송일시 설정이 잘못되었습니다.";
			}
			// 발송예정일시 < 과거 체크
			if (distribCustCnt < 1) {
				return chnlName + "채널 - 발송분배건수는 최소 1이상 이어야 합니다.";
			}
			
			LocalDateTime curLocalDatetime = null;
			LocalDateTime contLocalDatetime = null;
			LocalDate curLocalDate = null;
			LocalDate contLocalDate = null;
//			String curDate = null;
//			int contHour = 0;
//			int curHour = 0;
			if (contDatetimeStr != null) {
				curLocalDatetime = LocalDateTime.now();
				contLocalDatetime = LocalDateTime.parse(contDatetimeStr, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
				curLocalDate = LocalDate.now();
				contLocalDate = LocalDate.parse(contDate, DateTimeFormatter.ofPattern("yyyyMMdd"));
				
//				curDate = curLocalDatetime.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
//				contHour = contLocalDatetime.getHour();
//				curHour = curLocalDatetime.getHour();
			}
			
			/*
			 *  예약발송일 경우
			 */
			if ("N".equals(realTimeF) && "N".equals(todayF)) {
				
				// 발송예정일시 < 과거 체크
				if (contLocalDatetime.isBefore(curLocalDatetime)) {
					return chnlName + "채널 - 예약발송일시를 과거로 설정할 수 없습니다.";
				}
				
				// 발송예정일 >= 오늘 + 예약발송최소일수 체크
				if (curLocalDate != null && contLocalDate.compareTo(curLocalDate.plusDays(resvMinDay)) < 0) {
					return chnlName + "채널 - 예약발송일자가 최소요청일자를 지났습니다.";
				}				
				
				// 점포 캠페인일 경우
//				if ("03".equals(campInfo.getHqStrDivCd()) && curLocalDate != null &&
//						contLocalDate.isEqual(curLocalDate.plusDays(1)) && curHour > resvCloseTime) { // 발송예정일이 내일 & 현재시간 <= 예약발송등록마감시간
//					return chnlName + "채널의 경우 내일 진행되는 캠페인은 발송예약건이 종료되었습니다.\\n내일 오전 " + todayAmCloseTime + "시 이전에 당일발송을 이용해 주세요.";
//				}
				 
			/*
			 * 당일발송일 경우
			 */
//			} else if ("N".equals(realTimeF) && "Y".equals(todayF)) {
//				// 발송예정일 = 오늘
//				if (contLocalDate != null && !contLocalDate.isEqual(curLocalDate)) {
//					return chnlName + "채널 - 채널예약발송일자가 아닙니다.";
//				}
//				
//				// 점포 캠페인일 경우
//				if ("03".equals(campInfo.getHqStrDivCd())) {
//					// (발송예정시간이 당일발송오전발송시간 AND 현재시간 <= 당일발송오전등록마감시간)
//					if (contHour <= 12) {
//						if (curHour > todayAmCloseTime) {
//							return "당일발송오전등록마감시간을 지났습니다.";
//						}
//					// (발송예정시간이 당일발송오후발송시간 AND 현재시간 <= 당일발송오후등록마감시간)
//					} else {
//						if (curHour > todayPmCloseTime) {
//							return "당일발송오후등록마감시간을 지났습니다.";
//						}
//					}
//				}
//
//			/*
//			 * 즉시발송일 경우
//			 */
//			} else if ("Y".equals(realTimeF)) {
//				// 현재시간 <= 즉시발송등록마감시간
//				if (curHour > realCloseTime) {
//					return "즉시발송등록마감시간을 지났습니다.";
//				}
			} else {
				return "채널 설정이 잘못되었습니다.";
			}
			
			
		}
		
		return "Y";
	}

	//승인요청/자가승인 추가
	@Override
	public String checkNodeCount(CampaignVO campInfo) {
		String check = "";
		
		check = cellDAO.checkCellNodeCount(campInfo.getCampId());
		if ("N".equals(check)) {
			return "고객군 노드가 존재하지 않습니다.";
		}
		
		check = cellDAO.checkCellNodeCustCount(campInfo.getCampId());
		if (!"NM_SQL".equals(campInfo.getCtypId()) && "N".equals(check)) {
			return quadMaxMessageSource.getMessage("MC0197");
		}
		
		return "Y";
	}
	
	
}

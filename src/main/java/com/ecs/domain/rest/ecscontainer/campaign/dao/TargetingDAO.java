package com.ecs.domain.rest.ecscontainer.campaign.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.ecs.infrastructure.model.ResultMap;

@Mapper
public interface TargetingDAO {

	List<ResultMap> selectTargetingList(Map<String, Object> paramMap);
	
	//CopyCampaign 추가
	int insertTargetingWithSelect(Map<String, Object> paramMap);
}

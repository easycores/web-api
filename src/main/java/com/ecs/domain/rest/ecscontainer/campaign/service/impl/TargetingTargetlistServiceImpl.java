package com.ecs.domain.rest.ecscontainer.campaign.service.impl;

import com.ecs.domain.rest.ecscontainer.campaign.dao.TargetingDAO;
import com.ecs.domain.rest.ecscontainer.campaign.dao.TargetingTargetlistDAO;
import com.ecs.domain.rest.ecscontainer.campaign.service.TargetingTargetlistService;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("targetingTargetlistService")
public class TargetingTargetlistServiceImpl implements TargetingTargetlistService {
	
	@Autowired
	private TargetingTargetlistDAO targetingTargetlistDAO;
	@Autowired
	private TargetingDAO targetingDAO;

	public String KEYWORD_TN_ID = "@@타겟리스트@@";
	public String KEYWORD_TN_SGMN_ID = "@@세그먼트@@";
	public String KEYWORD_RM_ID = "@@캠페인_회차_고객군_발송순번_정정문자@@";
	
	public String TARGETLIST_TYPE_TN = "TN";
	public String TARGETLIST_TYPE_RM = "RM";
	

	@Override
	public List<ResultMap> getTnTargetlist(Map<String, Object> paramMap, PageBounds pageBounds) {
		setSearchParams(paramMap);
		return targetingTargetlistDAO.selectTnTargetlist(paramMap, pageBounds);
	}
	
	@Override
	public List<ResultMap> getTnTargetlist(Map<String, Object> paramMap) {
		setSearchParams(paramMap);
		return targetingTargetlistDAO.selectTnTargetlist(paramMap);
	}
	private void setSearchParams(Map<String, Object> params) {
		String srchRegPeriod = (String) params.get("srchRegPeriod");
		if (srchRegPeriod != null && !"".equals(srchRegPeriod)) {
			String[] period = srchRegPeriod.split(",");			
			if (period != null && period.length > 0) {
				params.put("srchRegStartDate", period[0]);
				if (period.length > 1) {
					params.put("srchRegEndDate", period[1]);
				}
			}
		}
		
		String srchCampPeriod = (String) params.get("srchCampPeriod");
		if (srchCampPeriod != null && !"".equals(srchCampPeriod)) {
			String[] period = srchCampPeriod.split(",");
			if (period != null && period.length > 0) {
				params.put("srchCampStartDate", period[0]);
				if (period.length > 1) {
					params.put("srchCampEndDate", period[1]);
				}
			}
		}
		String srchSendPeriod = (String) params.get("srchSendPeriod");
		if (srchSendPeriod != null && !"".equals(srchSendPeriod)) {
			String[] period = srchSendPeriod.split(",");			
			if (period != null && period.length > 0) {
				params.put("srchSendStartDate", period[0]);
				if (period.length > 1) {
					params.put("srchSendEndDate", period[1]);
				}
			}
		}
	}

	@Override
	public List<ResultMap> getTargetingTargetlistSaveData(Map<String, Object> paramMap) {
		List<ResultMap> targetlistData = null;

		String targetlistType = (String) paramMap.get("targetlistType");

		// 저장된 T_CAMP_TARGETING 정보
		paramMap.put("nodeID", paramMap.get("nodeId"));
		List<ResultMap> campTargetingList = targetingDAO.selectTargetingList(paramMap);

		if (campTargetingList != null) {
			String ids = "";
			String sgmnIds = "";
			// 저장된 정보를 불러올 경우
			for (ResultMap map : campTargetingList) {
				String promptKeyword = (String) map.get("promptKeyword");
				if (KEYWORD_TN_ID.equals(promptKeyword) || KEYWORD_RM_ID.equals(promptKeyword)) {
					ids = (String) map.get("promptValue");
				}
				if (KEYWORD_TN_SGMN_ID.equals(promptKeyword)) {
					sgmnIds = (String) map.get("promptValue");
				}
			}

			if (TARGETLIST_TYPE_RM.equals(targetlistType)) {
				paramMap.put("ids", ids.split(","));
				targetlistData = targetingTargetlistDAO.selectNodeRmTargetlist(paramMap);
			} else if (TARGETLIST_TYPE_TN.equals(targetlistType)) {
				paramMap.put("tId", ids);
				paramMap.put("sgmnIds", sgmnIds.split(","));
				targetlistData = targetingTargetlistDAO.selectNodeTnTargetlist(paramMap);
			}
		}

		return targetlistData;
	}
	

}

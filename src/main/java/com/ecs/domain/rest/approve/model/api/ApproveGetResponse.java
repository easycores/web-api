package com.ecs.domain.rest.approve.model.api;

import lombok.Data;

public class ApproveGetResponse {

    @Data
    public static class ResponseListDTO {
        private Long aprvReqNo;
        private Long aprvReqSeq;
        private String oCd;
        private String oMsg;

    }

}

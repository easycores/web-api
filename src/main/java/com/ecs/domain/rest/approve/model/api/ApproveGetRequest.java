package com.ecs.domain.rest.approve.model.api;

import lombok.Data;

public class ApproveGetRequest {

    @Data
    public static class RequestListDTO {
        private String status;
        private String userId;
        private String seq;
        private String cstsId;
    }

}

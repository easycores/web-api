package com.ecs.domain.rest.approve.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApproveVO {

	@JsonProperty(required = false)
	private long aprvReqNo;               //승인요청번호	

	@JsonProperty(required = false)
	private long aprvReqSeq;               //승인요청스퀀스번호		
	
	@JsonProperty(required = false)
	private String oCd;                      //결과코드
	
	@JsonProperty(required = false)
	private String oMsg;                      //결과메시지	

	public long getAprvReqNo() {
		return aprvReqNo;
	}

	public void setAprvReqNo(long aprvReqNo) {
		this.aprvReqNo = aprvReqNo;
	}	

	public long getAprvReqSeq() {
		return aprvReqSeq;
	}

	public void setAprvReqSeq(long aprvReqSeq) {
		this.aprvReqSeq = aprvReqSeq;
	}	
	
	public String getOCd() {
		return oCd;
	}

	public void setOCd(String oCd) {
		this.oCd = oCd;
	}
	
	public String getOMsg() {
		return oMsg;
	}

	public void setOMsg(String oMsg) {
		this.oMsg = oMsg;
	}

}

package com.ecs.domain.rest.approve.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

//@MapperScan("NewApproveDAO")
@Mapper
public interface NewApproveDAO {
	/**
	 * 승인 정보 조회
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectApprovalData(Map<String, Object> paramMap);

	/**
	 * 승인요청/자가승인
	 *
	 * @param paramMap@throws Exception
	 */
	ResultMap aprvReq(Map<String, Object> paramMap);
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> getApprovalList(Map<String, Object> paramMap);

	/**
	 * 기존 승인정보 확인
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	String selectPrevAprvCd(Map<String, Object> paramMap);

	/**
	 * 승인요청정보 생성
	 * @param paramMap
	 * @throws Exception
	 */
	void insertCampAprvInfo(Map<String, Object> paramMap);
	
	/**
	 * 결재라인 저장
	 * @param paramMap
	 * @throws Exception
	 */
	void insertCampAprvLines(Map<String, Object> paramMap);
	
	String checkCampCstsId(Map<String, Object> paramMap);
	
	int updateAprvId(Map<String, Object> paramMap);
	
	int updateAprvRejectReason(Map<String, Object> paramMap);
	
	ResultMap getApprovalDocHtml(Map<String, Object> paramMap);
	
	String selectAprvReqUser();
}

package com.ecs.domain.rest.approve.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

//@MapperScan("emiDAO")
@Mapper
public interface EmiDAO {

	int insertEmiBlsmsignInfo(Map<String, Object> paramMap);
	
	ResultMap callSpEmiSignReq();
}

package com.ecs.domain.rest.approve.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecs.domain.rest.approve.dao.DraftDAO;
//import com.ecs.quadmax.approve.dao.DraftDAO;
import com.ecs.domain.rest.approve.service.DraftService;
//import com.ecs.quadmax.approve.service.DraftService;
import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

@Service("draftService")
public class DraftServiceImpl implements DraftService {
	
	@Autowired
	private DraftDAO draftDAO;

	@Override
	public ResultMap getCampaignDocInfo(Map<String, Object> paramMap) {
		return draftDAO.getCampaignDocInfo(paramMap);
	}

	@Override
	public List<ResultMap> getCampChannelDocInfo(Map<String, Object> paramMap) {
		return draftDAO.getCampChannelDocInfo(paramMap);
	}

	@Override
	public List<ResultMap> getCampCellDocInfo(Map<String, Object> paramMap) {
		return draftDAO.getCampCellDocInfo(paramMap);
	}
	
	@Override
	public List<ResultMap> getCampOfferDocInfo(Map<String, Object> paramMap) {
		return draftDAO.getCampOfferDocInfo(paramMap);
	}

	@Override
	public List<ResultMap> getCampSccssDocList(Map<String, Object> paramMap) {
		return draftDAO.getCampSccssDocList(paramMap);
	}

	@Override
	public ResultMap getCampRoiDocList(Map<String, Object> paramMap) {
		return draftDAO.getCampRoiDocList(paramMap);
	}

	@Override
	public List<ResultMap> getEbmDetectList(Map<String, Object> paramMap) {
		return draftDAO.getEbmDetectList(paramMap);
	}

	
}

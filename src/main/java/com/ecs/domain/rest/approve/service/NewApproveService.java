package com.ecs.domain.rest.approve.service;

import java.io.IOException;
import java.util.Map;

public interface NewApproveService {

	/**
	 * 승인요청/자가승인(프로시저 x)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	String aprvReqWeb(Map<String, Object> paramMap) throws IOException;

}

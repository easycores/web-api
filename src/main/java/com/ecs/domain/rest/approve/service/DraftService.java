package com.ecs.domain.rest.approve.service;

import java.util.List;
import java.util.Map;

import com.ecs.infrastructure.model.ResultMap;
//import com.ecs.quadmax.infrastructure.model.ResultMap;

public interface DraftService {

	ResultMap getCampaignDocInfo(Map<String, Object> paramMap);
	
	List<ResultMap> getCampChannelDocInfo(Map<String, Object> paramMap);
	
	List<ResultMap> getCampCellDocInfo(Map<String, Object> paramMap);
	
	List<ResultMap> getCampOfferDocInfo(Map<String, Object> paramMap);
	
	List<ResultMap> getCampSccssDocList(Map<String, Object> paramMap);
	
	ResultMap getCampRoiDocList(Map<String, Object> paramMap);
	
	List<ResultMap> getEbmDetectList(Map<String, Object> paramMap);
}

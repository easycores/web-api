package com.ecs.domain.rest.approve.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecs.domain.rest.approve.dao.EmiDAO;
//import com.ecs.quadmax.approve.dao.EmiDAO;
import com.ecs.domain.rest.approve.dao.NewApproveDAO;
//import com.ecs.quadmax.approve.dao.NewApproveDAO;
import com.ecs.domain.rest.approve.service.DraftService;
//import com.ecs.quadmax.approve.service.DraftService;
import com.ecs.domain.rest.approve.service.NewApproveService;
//import com.ecs.quadmax.login.model.UserVO;
import com.ecs.domain.rest.aprovalmanage.dao.NewAprovalManagementDAO;
//import com.ecs.quadmax.manage.system.aprovalmanage.dao.NewAprovalManagementDAO;
import com.ecs.domain.rest.aprovalmanage.service.NewAprovalManagementService;
//import com.ecs.quadmax.common.QuadMaxMessageSource;
import com.ecs.domain.rest.common.service.MakingIdService;
//import com.ecs.quadmax.common.util.Util;
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;
//import com.ecs.quadmax.ecscontainer.campaign.model.CampaignVO;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
//import com.ecs.quadmax.common.Const;
import com.ecs.global.i18n.QuadMaxMessageSource;
//import com.ecs.quadmax.common.util.DateUtil;
import com.ecs.global.util.JsonUtil;
import com.ecs.global.util.Util;

@Service("approveService2")
public class NewApproveServiceImpl implements NewApproveService {
	private static final Logger LOGGER = LoggerFactory.getLogger(NewApproveServiceImpl.class);
	
	@Autowired
	private NewApproveDAO newApproveDAO;
	
	@Autowired
	private ContainerService containerService;
	
	@Autowired
	private MakingIdService makingIdService;
	
	@Autowired
	private NewAprovalManagementDAO newAprovalManagementDAO;
	
	@Autowired
	private NewAprovalManagementService newAprovalManagementService;

	@Resource(name="quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;
	
	/**
	 * 자가승인/승인요청 (자바)
	 * PR_CMS_APRV_REQ_LINES
	 * @param paramMap@throws Exception
	 * @throws IOException 
	 */
	@Override
	public String aprvReqWeb(Map<String, Object> paramMap) throws IOException {
		String aprvStatCd = (String) paramMap.get("aprvStatCd");
		String campId = (String) paramMap.get("campId");
		
		//승인대상자 처리
		//List<Map<String, Object>> aprvList = JsonUtil.getListMap((String)paramMap.get("aprvList"));
		List<Map<String, Object>> aprvList = new ArrayList<Map<String, Object>>();
		if (!"".equals((String)paramMap.get("userId"))) {
			//aprvList = JsonUtil.getListMap((String)paramMap.get("userId"));
	    	String tmpUserStr = Util.isNull((String) paramMap.get("userId"), "");
	    	String[] tmpUserList = tmpUserStr.split(",");
	    	Integer tmpCnt = 0;
	    	
	    	for (int i = 0; i < tmpUserList.length; i++) {
	    		
	    		Map<String, Object> tmpUserMap = new HashMap<String, Object>();
	    		
	    		tmpCnt = i+1; 
	    		tmpUserMap.put("seq", tmpCnt.toString());
	    		tmpUserMap.put("empId", tmpUserList[i]);	    		
	    		aprvList.add(tmpUserMap);
	    	}
	    	
			LOGGER.info("\n >> aprvList => {} ", aprvList);			
		}

		CampaignVO campaignVO = containerService.readCampaign(campId);

		//TODO user 코멘트 처리 주석 해제할것
//		UserVO user = (UserVO)QuadMaxUserDetailsHelper.getAuthenticatedUser();
//		if (user == null) {
//			return "[처리불가] 승인요청자를 지정할 수 없습니다.";
//		}
		
		// 캠페인에 저장된 오퍼중에 개인화쿠폰(OF13)이 있는 경우 - 앱쿠폰 게시정보가 매핑(엑셀업로드)되었는지 확인  
//		boolean isMapped = true;
//		String unMappedMsg = "";
//		List<ResultMap> campOffers = offerService.selectCampOfferListByCampId(campId);
//		for (Map<String, Object> offerInfo : campOffers) {
//			if("OF13".equals(offerInfo.get("offerTypeCd"))) {
//				offerInfo.put("nodeId", offerInfo.get("cellNodeId"));
//				if(!offerService.isOfferDetailMappedWithAppEvent(offerInfo)) {
//					unMappedMsg = "앱쿠폰게시정보가 매핑되지 않은 개인화쿠폰이 존재합니다. 노드ID : " + offerInfo.get("cellNodeId");
//					isMapped = false;
//					break;
//				}
//			}
//		}
//		if (!isMapped) {
//			return unMappedMsg;
//		}
		
		//자가승인 결재라인 자동 설정
		if ("5".equals(aprvStatCd)) {
			if (aprvList == null || aprvList.size() == 0) {
				aprvList = new ArrayList<Map<String, Object>>();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("seq", "1");
				//TODO user 코멘트 처리 주석 해제할것
				//map.put("empId", user.getEmpId());
				map.put("empId", "admin");
				aprvList.add(map);
			}
		}

		//결재라인 지정 오류 확인
		if(aprvList == null || aprvList.size() == 0) {
			return quadMaxMessageSource.getMessage("MC0180");	//[처리불가] 결재라인을 지정할 수 없습니다.
		}

		// 점포캠페인일때 기획자와 승인자가 같을 경우 에러처리
		for (Map<String, Object> map : aprvList) {
			LOGGER.info("\n >> map.get(seq) => {} ", map.get("seq"));	
			LOGGER.info("\n >> map.get(empId) => {} ", map.get("empId"));	
			String seq = (String) map.get("seq");
			String empId = (String) map.get("empId");
			if (!StringUtils.isNumeric(seq) || Integer.parseInt(seq) < 1) {
				return "[처리불가] 결재순번이 잘못 되었습니다.";
			}
			
			LOGGER.info("\n >> empId => {} ", empId);
			if (StringUtils.isEmpty(empId)) {
				return "[처리불가] 결재라인이 잘못 설정되었습니다.";
			}

			LOGGER.info("\n >> aprvStatCd => {} ", aprvStatCd);
			LOGGER.info("\n >> campaignVO.getHqStrDivCd() => {} ", campaignVO.getHqStrDivCd());
			if (!"5".equals(aprvStatCd) &&  "03".equals(campaignVO.getHqStrDivCd())) {
				if (campaignVO.getRegEmpId().equals((String) map.get("empId")))
					return "[처리불가] 승인자와 기획자가 같습니다.";
			}
		}
				
		String valid = "";
		valid = containerService.checkApproval(campaignVO, aprvStatCd, aprvList);

		if (!"Y".equals(valid)) {
			return valid;
		}
		valid = containerService.checkChannelOffer(campaignVO);

		if (!"Y".equals(valid)) {
			return valid;
		}
//		valid = containerService.checkRoi(campaignVO);
//		if (!"Y".equals(valid)) {
//			return valid;
//		}
		valid = containerService.checkNodeCount(campaignVO);

		if (!"Y".equals(valid)) {
			return valid;
		}
		
		//TODO user 코멘트 처리 주석 해제할것
		//String aprvReqEmpId = user.getEmpId();
		String aprvReqEmpId = "";
		// 요청자가 quadmax_mart.tb_dw_user 에 없는 요청자일 때-요청자를 공통코드의 val1로 변경
//		if ("03".equals(campaignVO.getHqStrDivCd()) && "40".equals(user.getBiztpCd())) {
//			aprvReqEmpId = newApproveDAO.selectAprvReqUser();
//			if (aprvReqEmpId == null || "".equals(aprvReqEmpId)) {
//				return "결재 요청자가 설정되지 않았습니다. 관리자에게 문의해 주세요.";
//			}
//		}
		paramMap.put("aprvReqEmpId", aprvReqEmpId);
		
		//승인요청번호 select (2099 고정으로 가야함.)
		String aprvReqNo = Integer.toString(makingIdService.getSerialNumber("APRV", "2099"));
		paramMap.put("aprvReqNo", aprvReqNo);
		
		//승인요청정보 생성
		newApproveDAO.insertCampAprvInfo(paramMap);
		
		//결재라인 저장
		for (Map<String, Object> map : aprvList) {
			paramMap.put("aprvEmpId", (String) map.get("empId"));
			paramMap.put("aprvReqSeq", (String) map.get("seq"));
			newApproveDAO.insertCampAprvLines(paramMap);
		}
		
		
		String nextCstsId = null;
		// 자가승인 시
//		paramMap.put("seq", 1);
//		if("5".equals(aprvStatCd)) {
//			paramMap.put("aprvStatCd", "2");
//			newAprovalManagementDAO.aprvRsltLines(paramMap);
//			newAprovalManagementDAO.updateAprvInfo(paramMap);
//			nextCstsId = newAprovalManagementService.getCstsIdAfterApproval(campaignVO);
//		}
//		if(nextCstsId == null) {
			nextCstsId = newAprovalManagementService.getCstsIdUntilApproval(aprvStatCd);
//		}
		paramMap.put("nextCstsId", nextCstsId);
//		if("5".equals(aprvStatCd)) {
//			paramMap.put("nextCstsId", "006");
//		}
		newAprovalManagementDAO.updateCstsAprovalForCampaign(paramMap);
		newAprovalManagementDAO.updateCstsAprovalForContainer(paramMap);
		
//		if ("002".equals(nextCstsId)) {
//			String docurl = null;
//			HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
//			int port = req.getServerPort();
//			if (port == 80) {
//				docurl = "http://" + serverName;
//			} else if (port == 443) {
//				docurl = "https://" + serverName;
//			} else if (port == 8443){
//				docurl = "https://" + serverName + ":8443";
//			} else {
//				docurl = "http://" + serverName + ":" + port;
//			}
//			docurl += "/approve/aprvDocHtml.do?campId=" + campId + "&aprvReqNo=" + aprvReqNo;
//			
//			paramMap.put("title", "CRM 캠페인 기안");
//			paramMap.put("docurl", docurl);
//			paramMap.put("htmlData", getCmsDraftDoc(paramMap));
//			paramMap.put("kyulempno1", aprvList);
//			paramMap.put("kyulname1", paramMap.get("empName"));
//			if ((String)paramMap.get("ccEmpno") !=null) {
//				paramMap.put("ccEmpno", paramMap.get("ccEmpno"));
//			}
//			emiDAO.insertEmiBlsmsignInfo(paramMap);
//			int aprvId = CmmnUtil.toInt(paramMap.get("sendno"));
//			
//			paramMap.put("aprvId", aprvId);
//			newApproveDAO.updateAprvId(paramMap);
//		}
		
		return null;
	}

}

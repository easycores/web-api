package com.ecs.domain.rest.approve.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.domain.rest.approve.model.ApproveVO;
import com.ecs.domain.rest.approve.service.NewApproveService;
import com.ecs.domain.rest.aprovalmanage.model.api.AprovalManagementGetRequest;
import com.ecs.domain.rest.aprovalmanage.model.api.AprovalManagementGetResponse;
import com.ecs.domain.rest.aprovalmanage.service.NewAprovalManagementService;

//기획전환 추가
import com.ecs.domain.rest.ecscontainer.campaign.service.CampListService;
import com.ecs.domain.rest.ecscontainer.scenario.controller.TestConController;
import com.ecs.domain.rest.ecscontainer.scenario.model.api.EcsContainerGetResponse;
import com.ecs.global.constant.Const;
//import com.ecs.quadmax.common.Const;
import com.ecs.global.i18n.QuadMaxMessageSource;
import com.ecs.global.util.SessionUtil;
import com.ecs.global.util.Util;
import com.ecs.infrastructure.model.EcsResponse;
import com.ecs.domain.rest.approve.model.api.ApproveGetRequest;
import com.ecs.domain.rest.approve.model.api.ApproveGetResponse;

//@Controller
//@RequestMapping(value="/approve")
@RestController
public class ApproveController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestConController.class);
	
	@Autowired
	private NewApproveService newRecognizeService;
	
	@Autowired
	private CampListService campListService;

	@Autowired
	private NewAprovalManagementService newAprovalManagementService;	

	@Resource(name="quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;	
	
	/**
	 * 승인요청/자가승인
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	@PostMapping(value = "/campaigns/{campId}/approvals")
	public EcsResponse<List<ApproveGetResponse.ResponseListDTO>> aprvReq(@PathVariable("campId") String campId, ApproveGetRequest.RequestListDTO reqDto) throws IOException {

		Map<String, Object> paramMap = new HashMap<>();

		List<ApproveGetResponse.ResponseListDTO> voList = new ArrayList<>();
		
		ApproveGetResponse.ResponseListDTO tmpDTO = new ApproveGetResponse.ResponseListDTO();
		
		LOGGER.info(" >> reqDto => {}", reqDto);

		paramMap.put("campId", campId);

		String aprvReqSeq = "";

		if  (reqDto.getSeq() == null || "".equals(reqDto.getSeq())) {
			aprvReqSeq = "1";
		} else {
			aprvReqSeq = reqDto.getSeq();
		}
		
		String cstsId = "";

		if  (reqDto.getCstsId() == null || "".equals(reqDto.getCstsId())) {
			cstsId = "";
		} else {
			cstsId = reqDto.getCstsId();
		}

		LOGGER.info("\n >> reqDto.getUserId() => {}", reqDto.getUserId());
		
		Map<String, Object> tmpMap = new HashMap<>();
		
		if ("requestApproved".equals(reqDto.getStatus()) && "".equals(reqDto.getUserId())) {
			paramMap.put("aprvStatCd", "5");
			paramMap.put("userId", "");	
		} else if ("requestApproved".equals(reqDto.getStatus()) && !"".equals(reqDto.getUserId())) {
			//paramMap.put("aprvStatCd", "x");
			paramMap.put("aprvStatCd", "1");
			paramMap.put("userId", reqDto.getUserId());
		} else if ("planning".equals(reqDto.getStatus())) {
			LOGGER.info("\n >> 기획전환 상태변경 CampListDAO.updateCampaignStatus cstsId 값 재확인 필요");

			paramMap.put("campId", campId);
			paramMap.put("seq", aprvReqSeq);
			paramMap.put("cstsId", cstsId);			
			
			paramMap.put("newCstsId", Const.CSTS_WRITE_K);
			int updateCount = campListService.updateCampaignStatus(paramMap);

			return new EcsResponse(HttpStatus.OK);
		} else if ("requestCanceled".equals(reqDto.getStatus())) {
			LOGGER.info("\n >> 승인요청취소");
			
			paramMap.put("campId", campId);
			paramMap.put("seq", aprvReqSeq);
			paramMap.put("aprvStatCd", "4");
			paramMap.put("aprvFlag", "false");
			paramMap.put("userId", "");	
			
			Map<String, Object> cancleMap = new HashMap<>();
			
			try {
				LOGGER.info("\n >> 승인요청취소 paramMap => {}", paramMap);
				cancleMap = this.aprvCancle(paramMap);
			} catch(IOException e) {
				LOGGER.error("aprvCancle IOException", e);		
			} catch(SQLException e) {
				LOGGER.error("aprvCancle IOException", e);	
			}
			
			return new EcsResponse(HttpStatus.OK);
				
		} else {
			LOGGER.info("\n >> 추후 재확인 필요");
			return new EcsResponse(HttpStatus.NO_CONTENT);
		}
		

		String oMsg = newRecognizeService.aprvReqWeb(paramMap);

		paramMap.put("oMsg", oMsg);
		if(oMsg != null && !"".equals(oMsg)) {
			paramMap.put("oCd", "FAIL");
		}
		else {
//			if (!"5".equals(aprvStatCd)) {
//				newRecognizeService.aprvReqWebSp();
//			}
			paramMap.put("oCd", "OK");
			
		}
		
		//TODO user 코멘트 처리 주석 해제할것
//		UserVO user = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();
//		paramMap.put("hqStrDivCd", user.getHqStrDivCd());
		
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.addObject("result", paramMap);
//		modelAndView.setViewName(Const.JSON_VIEW);
//		return modelAndView;

		LOGGER.info("\n >> aprvReq paramMap => {}", paramMap);

		tmpDTO.setAprvReqNo(Long.parseLong((String)paramMap.get("aprvReqNo")));
		tmpDTO.setAprvReqSeq(Long.parseLong((String)paramMap.get("aprvReqSeq")));
		tmpDTO.setOCd((String)paramMap.get("oCd"));
		tmpDTO.setOMsg(oMsg);
		
		voList.add(tmpDTO);
		
		LOGGER.info("\n >> copyCampaign voList => {}", voList);
		
		return new EcsResponse(voList, HttpStatus.OK);
	}
	
	//승인요청취소 추가
	/**
	 * 승인요청취소
	 * @param model
	 * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @throws IOException 
	 * @
	 */
	//@RequestMapping(value="/approveCancle.do", method=RequestMethod.POST)
	public Map<String, Object> aprvCancle (Map<String, Object> paramMap) throws IOException, SQLException {
		
		int successCnt = 0;
		String errorMsg = "";
		
		UserVO userInfo = (UserVO) SessionUtil.getAttribute(Const.SESSION_USER);
		//TODO user 코멘트 처리 주석 해제할것
		//paramMap.put("aprvEmpId", userInfo.getEmpId());
		paramMap.put("aprvEmpId", "admin");
		
		Map<String, Object> map = new HashMap<>();
		
		LOGGER.info("\n newAprovalManagementService.getAprvReqNo paramMap => {}", paramMap);
		
		paramMap.put("aprvReqNo", newAprovalManagementService.getAprvReqNo(paramMap));
		
		int checkCnt = newAprovalManagementService.checkApproveReqCancle(paramMap);//승인요청취소전 체크사항
		
		LOGGER.info("\n checkCnt => {}", checkCnt);
		
		if(checkCnt > 0){
			successCnt = newAprovalManagementService.aprvRsltLinesCancle(paramMap);
			if(successCnt == -1){
				errorMsg = quadMaxMessageSource.getMessage("MM0020");
				map.put("message", errorMsg);
				map.put("messageDev", errorMsg);
				//model.addAttribute("errMsg", errorMsg);
			}
		}else{
			successCnt = -1;
		}
		
		//model.addAttribute(Const.SUCCESS, successCnt > 0);
		
		//return Const.JSON_VIEW;
		return map;
	}		
}

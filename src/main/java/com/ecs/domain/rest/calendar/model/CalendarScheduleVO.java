package com.ecs.domain.rest.calendar.model;

import lombok.Data;
import java.util.List;
public class CalendarScheduleVO {
    @Data
    public static class GetReplacementStringDTO {
        private List<SundayVO> sundayList;
        private List<ChannelScheduleVO> chnlScheduleList;
        private List<HolidayVO> holidayList;
        private List<MonthChannelVO> monCampSendChnlList;
    }

    @Data
    public static class SundayVO {
        private String holiDt;
        private String holiKindNm;
    }

    @Data
    public static class ChannelScheduleVO {
        private String chnlScheduleDt;
        private String chnlScheduleNm;
    }

    @Data
    public static class HolidayVO {
        private String holiDt;
        private String holiKindNm;
    }

    @Data
    public static class MonthChannelVO {
        private String procSchedDt;
        private String totChnlCnt;
        private String totCustCnt;
    }
}



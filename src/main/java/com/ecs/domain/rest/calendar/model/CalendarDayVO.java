package com.ecs.domain.rest.calendar.model;

import lombok.Data;

@Data
public class CalendarDayVO {
    private String campId;
    private String campNm;
    private String chnlId;
    private String chnlNm;
    private String cstsId;
    private String cstsNm;
    private String procSchedDt;
    private int reqCustCnt;
    private int sortOrd;
}

package com.ecs.domain.rest.calendar.controller;


import com.ecs.domain.rest.calendar.model.CalendarDayVO;
import com.ecs.domain.rest.calendar.model.CalendarScheduleVO;
import com.ecs.domain.rest.calendar.sevice.CalendarService;
import com.ecs.global.util.CmmnUtil;
import com.ecs.global.util.PagingUtil;
import com.ecs.infrastructure.model.EcsResponse;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/calendars")
public class CalendarController {

    @Autowired
    public CalendarService channelScheduleService;


    /**
     * 채널 스케쥴 목록 조회
     */
    @GetMapping
    public EcsResponse<CalendarScheduleVO.GetReplacementStringDTO> getChannelScheduleList(@RequestParam(value = "searchYm", required = true) String searchYm) {
        CalendarScheduleVO.GetReplacementStringDTO definitions = new CalendarScheduleVO.GetReplacementStringDTO();

        List<CalendarScheduleVO.SundayVO> sundayVO = CmmnUtil.convertResultMapList(channelScheduleService.getSundayList(searchYm), CalendarScheduleVO.SundayVO.class);
        List<CalendarScheduleVO.ChannelScheduleVO> channelScheduleVO = CmmnUtil.convertResultMapList(channelScheduleService.getChnlScheduleList(searchYm), CalendarScheduleVO.ChannelScheduleVO.class);
        List<CalendarScheduleVO.HolidayVO> holidayVO = CmmnUtil.convertResultMapList(channelScheduleService.getHolidayList(searchYm), CalendarScheduleVO.HolidayVO.class);
        List<CalendarScheduleVO.MonthChannelVO> monthChannelVO = CmmnUtil.convertResultMapList(channelScheduleService.getMonCampSendChnlList(searchYm), CalendarScheduleVO.MonthChannelVO.class);

        definitions.setSundayList(sundayVO);
        definitions.setChnlScheduleList(channelScheduleVO);
        definitions.setHolidayList(holidayVO);
        definitions.setMonCampSendChnlList(monthChannelVO);

        return new EcsResponse<>(definitions, HttpStatus.OK);
    }

    /**
     * 캠페인달력 일별 조회
     */
    @GetMapping("/date")
    public EcsResponse<List<CalendarDayVO>> getDayCampSendChnlList(@RequestParam(value = "searchYm", required = true) String searchYm,
                                                             @RequestParam(value = "page_number", required = true) int page_number,
                                                             @RequestParam(value = "page_size", required = true) int page_size,
                                                             @RequestParam(value = "lang", required = true) String lang,
                                                             @RequestParam Map<String, Object> paramMap) {

        PageBounds pageBounds = PagingUtil.getSimplePageBounds(paramMap);
        List<ResultMap> calendarDayData = channelScheduleService.getDayCampSendChnlList(paramMap, pageBounds);
        PageList pageList = new PageList(calendarDayData);

        List<CalendarDayVO> calendarDayList = CmmnUtil.convertResultMapList(calendarDayData, CalendarDayVO.class);

        return new EcsResponse<>(calendarDayList, pageList, HttpStatus.OK);
    }
}





package com.ecs.domain.rest.calendar.sevice.impl;

import com.ecs.domain.rest.calendar.dao.CalendarDAO;
import com.ecs.domain.rest.calendar.sevice.CalendarService;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CalendarServiceImpl implements CalendarService {

    @Autowired
    CalendarDAO calendarDAO;
    @Override
    public List<ResultMap> getSundayList(String searchYm) {
        return calendarDAO.getSundayList(searchYm);
    }

    @Override
    public List<ResultMap> getChnlScheduleList(String searchYm) {
        return calendarDAO.getChnlScheduleList(searchYm);
    }

    @Override
    public List<ResultMap> getHolidayList(String searchYm) {
        return calendarDAO.getHolidayList(searchYm);
    }

    @Override
    public List<ResultMap> getMonCampSendChnlList(String searchYm) {
        return calendarDAO.getMonCampSendChnlList(searchYm);
    }

    @Override
    public List<ResultMap> getDayCampSendChnlList(Map<String, Object> paramMap, PageBounds pageBounds) {
        return calendarDAO.getDayCampSendChnlList(paramMap, pageBounds);
    }

}

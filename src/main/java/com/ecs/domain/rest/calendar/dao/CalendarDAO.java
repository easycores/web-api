package com.ecs.domain.rest.calendar.dao;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;

import java.util.List;
import java.util.Map;

@Mapper
public interface CalendarDAO {
    List<ResultMap> getSundayList(String searchYm);

    List<ResultMap> getChnlScheduleList(String searchYm);

    List<ResultMap> getHolidayList(String searchYm);

    List<ResultMap> getMonCampSendChnlList(String searchYm);

    List<ResultMap> getDayCampSendChnlList(Map<String, Object> paramMap, PageBounds pageBounds);
}

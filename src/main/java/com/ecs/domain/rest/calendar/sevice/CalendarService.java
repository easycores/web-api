package com.ecs.domain.rest.calendar.sevice;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import java.util.List;
import java.util.Map;

public interface CalendarService {
    List<ResultMap> getSundayList(String searchYm);

    List<ResultMap> getChnlScheduleList(String searchYm);

    List<ResultMap> getHolidayList(String searchYm);

    List<ResultMap> getMonCampSendChnlList(String searchYm);

    List<ResultMap> getDayCampSendChnlList(Map<String, Object> paramMap, PageBounds pageBounds);
}

package com.ecs.domain.rest.offer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class CampOfferVO {

    private String eventCd;
    private Long offerCost;
    private String offerCostTypeCd;
    private String offerDesc;
    private String offerEdate;
    private String offerId;
    private String offerName;
    private String offerSdate;
    private String offerTypeCd;
    private String offerTypeName;
}

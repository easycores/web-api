package com.ecs.domain.rest.offer.service;

import com.ecs.domain.rest.offer.model.CampOfferVO;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import java.util.List;

public interface OfferService {
    List<CampOfferVO> getCampOfferList(String campId, String cellNodeId);
    List<CampOfferVO> getCampOfferList(String campId, String cellNodeId, PageBounds pageBounds);
    int saveCampOfferList(String campId, String cellNodeId, List<CampOfferVO> offerList);
}

package com.ecs.domain.rest.offer.controller;

import com.ecs.domain.rest.offer.model.CampOfferVO;
import com.ecs.domain.rest.offer.model.api.OfferPostRequest;
import com.ecs.domain.rest.offer.service.OfferService;
import com.ecs.global.util.CmmnUtil;
import com.ecs.global.util.PagingUtil;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import org.apache.commons.beanutils.BeanUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/campaign/{campId}/nodes/{nodeId}/offers")
@Validated
public class OfferController {

    @Autowired
    private OfferService offerService;

    @GetMapping
    public ResponseEntity findOfferList(Model model, @PathVariable String campId, @PathVariable String nodeId,
                                       @RequestParam final Map<String, Object> paramMap) {
        List offerList;
        if(PagingUtil.isPageBoundRequired(paramMap)){
            PageBounds pageBounds = PagingUtil.getSimplePageBounds(paramMap);
            offerList = offerService.getCampOfferList(campId,nodeId,pageBounds);
            PageList pageList = (PageList) offerList;
            model.addAllAttributes(PagingUtil.getSimplePageConditions(pageList));
        } else {
            offerList = offerService.getCampOfferList(campId,nodeId);
        }
        model.addAttribute("data", offerList);

        List<CampOfferVO> campOfferVOS = new ArrayList<>();

        return ResponseEntity.ok().body(model);
    }

    @PostMapping
    public ResponseEntity saveOfferList(@PathVariable String campId, @PathVariable String nodeId,
                                        @Valid @RequestBody List<OfferPostRequest.SaveOfferListDTO> offerList) throws InvocationTargetException, IllegalAccessException {

//        * Validation 확인
//
//        $.ajax({
//                url : "/cms/api/v1/campaign/{campId}/nodes/{nodeId}/offers",
//                method : "POST",
//                data:JSON.stringify([{eventCd:""}]),
//        contentType:'application/json',
//                success : (data) => {
//            console.log(data)
//        }
//       })

        // List 가 아닌 경우
        // ModelMapper modelMapper = new ModelMapper();
        // modelMapper.map(origin, VO.class)

        // List 의 경우
        List<CampOfferVO> offerVoList = CmmnUtil.deepCopyVoList(offerList, CampOfferVO.class);
        System.out.println("offerVoList : "+ offerVoList.toString());
        int result = offerService.saveCampOfferList(campId,nodeId,offerVoList);

        return ResponseEntity.ok().body("");
    }

}

package com.ecs.domain.rest.offer.dao;

import com.ecs.domain.rest.offer.model.CampOfferVO;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CampOfferDAO {
    List<CampOfferVO> selectCampOfferList(@Param("campId") String campId, @Param("cellNodeId") String cellNodeId);
    List<CampOfferVO> selectCampOfferList(@Param("campId") String campId, @Param("cellNodeId") String cellNodeId, PageBounds pageBounds);
    int insertCampOfferList(@Param("campId") String campId, @Param("cellNodeId") String cellNodeId, @Param("offerList") List<CampOfferVO> offerList);
}

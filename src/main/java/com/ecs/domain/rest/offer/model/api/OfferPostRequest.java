package com.ecs.domain.rest.offer.model.api;

import lombok.Data;

import javax.validation.constraints.NotNull;


public class OfferPostRequest {
    @Data
    public static class SaveOfferListDTO {
        @NotNull
        private String eventCd;
        @NotNull
        private Long offerCost;
        @NotNull
        private String offerCostTypeCd;
        @NotNull
        private String offerDesc;
        @NotNull
        private String offerEdate;
        @NotNull
        private String offerId;
        @NotNull
        private String offerName;
        @NotNull
        private String offerSdate;
        @NotNull
        private String offerTypeCd;
        @NotNull
        private String offerTypeName;
    }
}

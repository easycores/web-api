package com.ecs.domain.rest.offer.service.impl;

import com.ecs.domain.rest.offer.dao.CampOfferDAO;
import com.ecs.domain.rest.offer.model.CampOfferVO;
import com.ecs.domain.rest.offer.service.OfferService;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private CampOfferDAO campOfferDAO;

    @Override
    public List<CampOfferVO> getCampOfferList(String campId, String cellNodeId) {
        return campOfferDAO.selectCampOfferList(campId, cellNodeId);
    }

    @Override
    public List<CampOfferVO> getCampOfferList(String campId, String cellNodeId, PageBounds pageBounds) {
        return campOfferDAO.selectCampOfferList(campId, cellNodeId, pageBounds);
    }

    @Override
    public int saveCampOfferList(String campId, String cellNodeId, List<CampOfferVO> offerList) {
        return campOfferDAO.insertCampOfferList(campId, cellNodeId, offerList);
    }
}

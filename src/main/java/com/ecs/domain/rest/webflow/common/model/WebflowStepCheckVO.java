package com.ecs.domain.rest.webflow.common.model;

public class WebflowStepCheckVO {
	private String save = "N";
	private String run = "N";
	private Object checkInfo;
	
	public String getSave() {
		return save;
	}

	public void setSave(String save) {
		this.save = save;
	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public Object getCheckInfo() {
		return checkInfo;
	}

	public void setCheckInfo(Object checkInfo) {
		this.checkInfo = checkInfo;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + " ["
				+ "save=" + save
				+ ", run=" + run 
				+ ", checkInfo=" + checkInfo 
		+ "]";
	}
}

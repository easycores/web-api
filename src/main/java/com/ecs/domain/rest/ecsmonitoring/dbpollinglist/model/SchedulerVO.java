package com.ecs.domain.rest.ecsmonitoring.dbpollinglist.model;

public class SchedulerVO {
	
	private String startDT;
	private String endDT;
	private String name;
	private String schedule;
	private String callMode;
	private String callString;
	private String created;
	private String updated;
	private String owner;
	private HttpBody httpBody;
	private String next;
	private int seq;
	private String sc_ID;
	
	public String getStartDT() {
		return startDT;
	}

	public void setStartDT(String startDT) {
		this.startDT = startDT;
	}

	public String getEndDT() {
		return endDT;
	}

	public void setEndDT(String endDT) {
		this.endDT = endDT;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getCallMode() {
		return callMode;
	}

	public void setCallMode(String callMode) {
		this.callMode = callMode;
	}

	public String getCallString() {
		return callString;
	}

	public void setCallString(String callString) {
		this.callString = callString;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public HttpBody getHttpBody() {
		return httpBody;
	}

	public void setHttpBody(HttpBody httpBody) {
		this.httpBody = httpBody;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getSc_ID() {
		return sc_ID;
	}

	public void setSc_ID(String sc_ID) {
		this.sc_ID = sc_ID;
	}

	public static class HttpBody {
		private String DP_ID;
		private String CONNECTION_ID;
		private String user;
		public String getDP_ID() {
			return DP_ID;
		}
		public void setDP_ID(String dP_ID) {
			DP_ID = dP_ID;
		}
		public String getCONNECTION_ID() {
			return CONNECTION_ID;
		}
		public void setCONNECTION_ID(String cONNECTION_ID) {
			CONNECTION_ID = cONNECTION_ID;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
	}

	@Override
	public String toString() {
		return "SchedulerVO [startDT=" + startDT + ", endDT=" + endDT + ", name=" + name + ", schedule=" + schedule
				+ ", callMode=" + callMode + ", callString=" + callString + ", created=" + created + ", updated="
				+ updated + ", owner=" + owner + ", httpBody=" + httpBody + ", next=" + next + ", seq=" + seq
				+ ", sc_ID=" + sc_ID + "]";
	}
}

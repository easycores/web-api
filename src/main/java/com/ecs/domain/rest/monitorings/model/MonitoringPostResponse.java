package com.ecs.domain.rest.monitorings.model;

import lombok.Data;

public class MonitoringPostResponse {
    @Data
    public static class ReserveSendingManageVO {
        private String campId;
        private String campName;
        private String campExecNo;
        private String campSdate;
        private String campEdate;
        private String ctypName;
        private String campTypeName;
        private String chnlSendSeq;
        private String cellNodeId;
        private String cellNodeName;
        private String chnlId;
        private String chnlName;
        private String contId;
        private int reqCustCnt;
        private int successCustCnt;
        private int failCustCnt;
        private int countC;
        private int countE;
        private String chnlSendYn;
        private String chnlSendYnName;
        private String procSchedDttm;
        private String procEndDttm;
        private String regEmpId;
        private String regEmpName;
        private String cstsName;
        private String procErrMsg;
        private String scId;
    }
}

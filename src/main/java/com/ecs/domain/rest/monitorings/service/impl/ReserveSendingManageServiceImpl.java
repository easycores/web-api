package com.ecs.domain.rest.monitorings.service.impl;

import com.ecs.domain.rest.monitorings.dao.ReserveSendingManageDAO;
import com.ecs.domain.rest.monitorings.service.ReserveSendingManageService;
import com.ecs.infrastructure.model.ResultMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class ReserveSendingManageServiceImpl implements ReserveSendingManageService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReserveSendingManageService.class);

	@Autowired
	ReserveSendingManageDAO reserveSendingManageDAO;

	public List<ResultMap> selectReserveSendingManageList(Map<String, Object> paramMap) {
		return reserveSendingManageDAO.selectReserveSendingManageList(paramMap);
	}
}

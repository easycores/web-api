package com.ecs.domain.rest.monitorings.model;

import lombok.Data;

public class MonitoringPostRequest {
    @Data
   public static class MonitoringPostRequestDTO{
       String srchCampPeriod;
       String srchCampKeyword;
       String srchCampTypeCd;
       String srchProcSchedPeriod;
       String srchChnlSendYn;
       String srchChnlId;
   }
}

package com.ecs.domain.rest.monitorings.controller;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.domain.rest.monitorings.model.MonitoringPostRequest;
import com.ecs.domain.rest.monitorings.model.MonitoringPostResponse;
import com.ecs.domain.rest.monitorings.service.ReserveSendingManageService;
import com.ecs.global.constant.Const;
import com.ecs.global.util.CmmnUtil;
import com.ecs.global.util.QuadMaxUserDetailsHelper;
import com.ecs.global.util.SessionUtil;
import com.ecs.infrastructure.model.EcsResponse;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ReserveSendingManageController {
	@Autowired
	private ReserveSendingManageService reserveSendingManageService;

	@PostMapping(value = "/monitorings")
	@ResponseBody
	public EcsResponse<List<MonitoringPostResponse.ReserveSendingManageVO>> selectReserveSendingManageList
			(MonitoringPostRequest.MonitoringPostRequestDTO monitoringPostRequestDTO) throws Exception {
		
		System.out.println("Controller 호출");

		Map<String, Object> paramMap  = setSearchParameter(monitoringPostRequestDTO);

		UserVO user = (UserVO) QuadMaxUserDetailsHelper.getAuthenticatedUser();

		UserVO userInfo = new UserVO();
		userInfo.setEmpId("admin");
		userInfo.setDeptId("001");

		paramMap.put("deptId", userInfo.getDeptId());

		List<MonitoringPostResponse.ReserveSendingManageVO> list = CmmnUtil.convertResultMapList(reserveSendingManageService.selectReserveSendingManageList(paramMap), MonitoringPostResponse.ReserveSendingManageVO.class);

		return new EcsResponse<>(list, HttpStatus.OK);
	}

	public Map<String, Object> setSearchParameter(@RequestParam MonitoringPostRequest.MonitoringPostRequestDTO monitoringPostRequestDTO) {
		Map<String, Object> paramMap = new HashMap<>();
		String srchCampPeriod = monitoringPostRequestDTO.getSrchCampPeriod();

		if(null != srchCampPeriod && !"".equals(srchCampPeriod)) {
			String[] tempDate = srchCampPeriod.split(",");
			paramMap.put("srchCampStartDate", tempDate.length > 0 ? tempDate[0] : "");
			paramMap.put("srchCampEndDate", tempDate.length > 1 ? tempDate[1] : "");
		}

		String srchProcSchedPeriod = monitoringPostRequestDTO.getSrchProcSchedPeriod();
		if(null != srchProcSchedPeriod && !"".equals(srchProcSchedPeriod)) {
			String[] tempDate = srchProcSchedPeriod.split(",");
			paramMap.put("srchProcSchedStartDate", tempDate.length > 0 ? tempDate[0]+"000000" : "");
			paramMap.put("srchProcSchedEndDate", tempDate.length > 1 ? tempDate[1]+"235959" : "");
		}

		String srchChnlSendYn = monitoringPostRequestDTO.getSrchChnlSendYn();
		if(null != srchChnlSendYn && !"".equals(srchChnlSendYn)) {
			paramMap.put("srchChnlSendYns", srchChnlSendYn);
		}

		String srchChnlId = monitoringPostRequestDTO.getSrchChnlId();
		if(null != srchChnlId && !"".equals(srchChnlId)) {
			paramMap.put("srchChnlIds", srchChnlId);
		}

		String srchCampKeyword = monitoringPostRequestDTO.getSrchCampKeyword();
		if(null != srchCampKeyword && !"".equals(srchCampKeyword)) {
			paramMap.put("srchCampKeyword", srchCampKeyword);
		}

		String srchCampTypeCd = monitoringPostRequestDTO.getSrchCampTypeCd();
		if(null != srchCampTypeCd && !"".equals(srchCampTypeCd)) {
			paramMap.put("srchCampTypeCd", srchCampTypeCd.split(","));
		}


		return paramMap;		
	}
}

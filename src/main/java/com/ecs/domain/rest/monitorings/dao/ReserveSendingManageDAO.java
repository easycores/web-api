package com.ecs.domain.rest.monitorings.dao;

import com.ecs.infrastructure.model.ResultMap;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface ReserveSendingManageDAO {
	List<ResultMap> selectReserveSendingManageList(Map<String, Object> paramMap);

}

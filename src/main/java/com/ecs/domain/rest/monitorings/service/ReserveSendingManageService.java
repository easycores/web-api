package com.ecs.domain.rest.monitorings.service;

import com.ecs.infrastructure.model.ResultMap;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ReserveSendingManageService {
	
	List<ResultMap> selectReserveSendingManageList(Map<String, Object> paramMap);
}

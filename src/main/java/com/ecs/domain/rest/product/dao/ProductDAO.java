package com.ecs.domain.rest.product.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

//@MapperScan("productDAO")
@Mapper
public interface ProductDAO {

	int insertCampSuccssCndtnPrdtWithSelect(Map<String, Object> paramMap);
	
}

package com.ecs.domain.rest.test.dao;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TestDAO {
    List<ResultMap> selectTest(Map<String,Object> paramMap);
    List<ResultMap> selectTest(Map<String,Object> paramMap, PageBounds pageBounds);
}

package com.ecs.domain.rest.test.service;

import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import java.util.List;
import java.util.Map;

public interface TestService {
    public List<ResultMap> selectTest(Map<String,Object> paramMap);
    public List<ResultMap> selectTest(Map<String,Object> paramMap, PageBounds pageBounds);
}

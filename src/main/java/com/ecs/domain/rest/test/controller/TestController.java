package com.ecs.domain.rest.test.controller;

import com.ecs.domain.rest.ecscontainer.campaign.model.CampListVO;
import com.ecs.domain.rest.test.model.api.TestGetResponse;
import com.ecs.domain.rest.test.service.TestService;
import com.ecs.global.util.CmmnUtil;
import com.ecs.global.util.PagingUtil;
import com.ecs.infrastructure.model.EcsResponse;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    static ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public TestService testService;

    // GET 요청 응답
    @GetMapping("/list")
    public ResponseEntity list(Model model, @RequestParam Map<String, Object> paramMap) throws Exception {
        List<ResultMap> testList = testService.selectTest(paramMap);
        model.addAttribute("data", testList);
        return ResponseEntity.badRequest().body(model);
    }

    // 페이징 처리를 포함한 GET 요청 응답
    //  - 요청시 파라미터
    //      page_size : 페이지에 표시할 갯수
    //      page_number : 현재 페이지
    @GetMapping("/list-page")
    public EcsResponse<List<TestGetResponse.ListDTO>> list(Model model, @RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

        PageBounds pageBounds = PagingUtil.getSimplePageBounds(paramMap);

        List<ResultMap> testList = testService.selectTest(paramMap,pageBounds);
        PageList pageList = (PageList) testList;

        List<TestGetResponse.ListDTO> list = CmmnUtil.deepCopyVoList(testList, TestGetResponse.ListDTO.class);

        return new EcsResponse(list, pageList, HttpStatus.OK);
    }

    // CSV 파일 다운로드
    @PostMapping("/csvDownload")
    public ModelAndView csvDownload(@RequestParam final Map<String, Object> paramMap) {

        ModelAndView modelAndView = new ModelAndView();
        List<CampListVO> list = CmmnUtil.convertResultMapList(testService.selectTest(paramMap), CampListVO.class);
        List<String> recNames = Arrays.asList(((String) paramMap.get("columnIds")).split(","));
        List<String> recTitles = Arrays.asList(((String) paramMap.get("columnNames")).split(","));
        String fileName = (String) paramMap.get("fileName");

        modelAndView.addObject("records",list);
        modelAndView.addObject("recNames",recNames);
        modelAndView.addObject("recTitles",recTitles);
        modelAndView.addObject("fileName",fileName);

        modelAndView.setViewName("csvDownloadView");
        return modelAndView;
    }
}

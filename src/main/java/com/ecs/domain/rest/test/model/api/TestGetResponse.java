package com.ecs.domain.rest.test.model.api;

import lombok.Data;

public class TestGetResponse {

    @Data
    public static class ListDTO {
        private String campId;
        private String campName;
        private String campSdate;
        private String campEdate;
    }

}

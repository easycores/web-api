package com.ecs.domain.rest.test.service.impl;

import com.ecs.domain.rest.test.dao.TestDAO;
import com.ecs.domain.rest.test.service.TestService;
import com.ecs.infrastructure.model.ResultMap;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    public TestDAO testDAO;

    public List<ResultMap> selectTest(Map<String,Object> paramMap) {
        return testDAO.selectTest(paramMap);
    }

    public List<ResultMap> selectTest(Map<String,Object> paramMap, PageBounds pageBounds) {
        return testDAO.selectTest(paramMap, pageBounds);
    }
}

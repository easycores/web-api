package com.ecs.domain.rest.cms.api.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ecs.domain.rest.cms.api.service.CmsApiService;
//import com.ecs.quadmax.cms.api.service.CmsApiService;
import com.ecs.global.constant.Const;
//import com.ecs.quadmax.common.Const;
import com.ecs.domain.rest.common.CustomClientHttpRequestFactory;
//import com.ecs.quadmax.common.CustomClientHttpRequestFactory;
import com.ecs.global.util.JsonUtil;
//import com.ecs.quadmax.common.util.JsonUtil;
import com.ecs.global.util.SessionUtil;
//import com.ecs.quadmax.common.util.SessionUtil;
import com.ecs.domain.rest.ecscontainer.scenario.service.EcsContainerService;
//import com.ecs.quadmax.ecscontainer.scenario.service.EcsContainerService;
import com.ecs.global.exception.QuadMaxException;
//import com.ecs.quadmax.exception.QuadMaxException;

//@Service
@Service("cmsApiService")
public class CmsApiServiceImpl implements CmsApiService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CmsApiServiceImpl.class);
	
	@Autowired
	private EcsContainerService ecsContainerService;
	
	@Value("${ebm.testsendserver.path}")
	private String testSendUrl;
	
	@Value("${ebm.rulecheck.path}")
	private String rulecheckUrl;
	
	@Value("${ebm.deploy.path}")
	private String ebmDeployUrl;
	
	@Value("${ebm.pause.path}")
	private String ebmPauseUrl;
	
	@Value("${ebm.resume.path}")
	private String ebmResumeUrl;
	
	@Value("${ebm.redisTarget.path}")
	private String redisTarget;
	
	@Value("#{application['application.cms.serverName']}")
	private String serverName;
	
	@Value("#{application['application.cms.serverIp']}")
	private String serverIp;
		
	@Override
	public ResponseEntity<Object> queryGetApi(HttpServletRequest request) {
		String url = getServerURL(request);
		RestTemplate restTemplate = new RestTemplate();
		
		return restTemplate.getForEntity(url, Object.class);
	}
	
	@Override
	public ResponseEntity<Object> queryPostApi(HttpServletRequest request, String body) {
		String url = getServerURL(request);
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate();

//		System.out.println("=======================================================");
//		System.out.println("request toString : " + request.toString());
//		System.out.println("url : " + url);
//		System.out.println("req body toString: " + req.getBody().toString());
//		System.out.println("req header location: " + req.getHeaders().getLocation());
//		System.out.println("=======================================================");
		return restTemplate.postForEntity(url, req, Object.class);
	}
	
	@Override
	public ResponseEntity<String> queryPostStringApi(HttpServletRequest request, String body) {
		String url = getServerURL(request);
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate();

		return restTemplate.postForEntity(url, req, String.class);
	}
	
	@Override
	public ResponseEntity<Object> commandApi(HttpServletRequest request, String body) {
		String url = getServerURL(request);
		LOGGER.debug("url : {}", url);
		HttpEntity<String> req = setHeader(StringEscapeUtils.unescapeHtml4(body));
		LOGGER.debug("req : {}", req);
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<String> res = restTemplate.postForEntity(url, req, String.class);
		return new ResponseEntity<>(res.getBody(), HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<Object> deleteTableApi(HttpServletRequest request, String body) {
		String url = getServerURL(request);
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate(new CustomClientHttpRequestFactory());
		return restTemplate.exchange(url, HttpMethod.DELETE, req, Object.class);
	}
	
	public String getServerURL(HttpServletRequest request) {
		String url = request.getRequestURL().toString();
		StringBuffer serverURL = new StringBuffer();

		int port = request.getServerPort();
		String local = null;
		String localIp = null;
		if (port == 80) {
			local = "http://" + serverName + "/";
			localIp = "http://" + serverIp + "/";
		} else if (port == 443) {
			local = "https://" + serverName + "/";
			localIp = "https://" + serverIp + "/";
		} else {
			local = "http://" + serverName + ":" + port + "/";
			localIp = "http://" + serverIp + ":" + port + "/";
		}
		
//		String serverURL = request.getHeader("serverURL") != null && !request.getHeader("serverURL").contains("N/A") ? request.getHeader("serverURL") : (String) SessionUtil.getAttribute(Const.DQS_URL);
		serverURL.append(ecsContainerService.getAliveQueryServerUrl());
		if (serverURL != null && StringUtils.isNotEmpty(serverURL.toString())) {
			serverURL.append(serverURL.toString().endsWith("/") ? "" : "/");
		}
		LOGGER.debug("serverURL : {}", serverURL);

//		System.out.println("=================================================================");
//		System.out.println("request.getHeader(\"serverURL\") : " + request.getHeader("serverURL"));
//		System.out.println("url : " + url);
//		System.out.println("Const.DQS_URL" + SessionUtil.getAttribute(Const.DQS_URL));
//		
//		System.out.println("port : " + port);
//		System.out.println("serverName : " + serverName);
//		System.out.println("serverIp : " + serverIp);
//		System.out.println("local : " + local);
//		System.out.println("serverURL : " + serverURL);
//		System.out.println("localIp : " + localIp);
//		System.out.println("=================================================================");
		
		if (!serverName.equals(request.getServerName())) {			
			String[] localarr = local.split("://");
			if (localarr != null && localarr.length > 1) {
				local = localarr[0] + "://" + localarr[1].replace(serverName, request.getServerName());
			}
		}
		url = url.replace("/cms/api", "");
		url = url.replace(local, serverURL);
		url = url.replace(localIp, serverURL);
		
//		System.out.println("url : " + url);
		try {
			url = URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("getServerURL Exception", e);
		}

		LOGGER.debug("url : {}", url);
		
		return url;
	}
	
	public HttpEntity<String> setHeader(String body) {
		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
		headers.setContentType(mediaType);
		return new HttpEntity<>(body, headers);
	}
	
	public ResponseEntity<Object> sendRequest(String url, HttpServletRequest request) throws IOException {
		return requestConnection(url, request, request.getMethod());
	}
	
	public ResponseEntity<Object> sendRequestWithMethod(String url, HttpServletRequest request, String method) throws IOException {
		return requestConnection(url, request, method);
	}
	
	private ResponseEntity<Object> requestConnection(String urlStr, HttpServletRequest request, String method) throws IOException {
		try {
			if (urlStr.startsWith("N/E") || urlStr.startsWith("N/A") || !urlStr.toLowerCase().startsWith("http")) {
				String serverUrl = getServerURL(request);
				urlStr = serverUrl
						+ (urlStr.startsWith("/") ? "" : "/")
						+ urlStr.replace("N/E", "").replace("N/A", "");
			}
			URL url = new URL(urlStr);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			con.setRequestMethod(method);
			con.setRequestProperty("accept", "application/json");
			if(method.equals("POST") || method.equals("PUT") || method.equals("DELETE")) {
				con.setRequestProperty("Content-Type", request.getHeader("content-type"));
				con.setDoOutput(true);
				try (OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(), StandardCharsets.UTF_8);) {
					String requestBody = IOUtils.toString(request.getReader());
					wr.write(requestBody);
					wr.flush();
				}
			}
	
			StringBuilder sb = new StringBuilder();
			String line;
			try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));) {
				line = br.readLine();
				while (line != null) {
					sb.append(line).append("\n");
					line = br.readLine();
				}
				
			}
			
			return new ResponseEntity<>(sb.toString(), HttpStatus.valueOf(con.getResponseCode()));
		} catch (ConnectException e) {
			LOGGER.error("fail to connect to URL: {}", urlStr);
			return null;
		}
	}

	@Override
	public ResponseEntity<Object> ebmPostApi(String type, String campId, String body) {
		String url = "";
		if ("rulecheck".equals(type)) {
			url = rulecheckUrl + campId;
		} else if ("deploy".equals(type)) {
			url = ebmDeployUrl + campId;
		} else if ("pause".equals(type)) {
			url = ebmPauseUrl + campId;
		} else if ("resume".equals(type)) {
			url = ebmResumeUrl + campId;
		}
		LOGGER.debug("ebmPostApi url : {}", url);
		LOGGER.debug("ebmPostApi body : {}", body);
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForEntity(url, req, Object.class);
	}
	
	@Override
	public ResponseEntity<Object> postApi(HttpServletRequest request, String body) {
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> res = restTemplate.postForEntity(testSendUrl, req, String.class);
		return new ResponseEntity<>(res.getBody(), HttpStatus.OK);	
	}

	@Override
	public ResponseEntity<Object> reqRedisApi(String campId, String body) throws QuadMaxException {
		HttpEntity<String> req = setHeader(body);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> res = restTemplate.postForEntity(redisTarget + campId, req, String.class);
		if (res.getStatusCode() != HttpStatus.OK) {
			throw new QuadMaxException("EBM Redis api 호출 실패 " + res.getBody());
		}
		Map<String, Object> reqRes = JsonUtil.getMap(res.getBody());
		LOGGER.debug("req redis api : {}", res);
		if (reqRes != null) {
			boolean status = (boolean) reqRes.get("status");
			if (!status) {
				throw new QuadMaxException((String) reqRes.get("msg"));
			}
		} else {
			throw new QuadMaxException("EBM Redis api 호출 결과값 없음");
		}
		return new ResponseEntity<>(res.getBody(), HttpStatus.OK);	
	}
}

package com.ecs.domain.rest.cms.api.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.ecs.global.exception.QuadMaxException;
//import com.ecs.quadmax.exception.QuadMaxException;

public interface CmsApiService {

	public ResponseEntity<Object> queryGetApi(HttpServletRequest request);
	public ResponseEntity<Object> queryPostApi(HttpServletRequest request, String body);
	public ResponseEntity<String> queryPostStringApi(HttpServletRequest request, String body);
	public ResponseEntity<Object> commandApi(HttpServletRequest request, String body);
	public ResponseEntity<Object> deleteTableApi(HttpServletRequest request, String body);
	public ResponseEntity<Object> sendRequest(String url, HttpServletRequest request) throws IOException;
	public ResponseEntity<Object> sendRequestWithMethod(String header, HttpServletRequest request, String string) throws IOException;
	public ResponseEntity<Object> ebmPostApi(String type, String campId, String body);
	public ResponseEntity<Object> postApi(HttpServletRequest request, String body);
	public ResponseEntity<Object> reqRedisApi(String campId, String body) throws QuadMaxException;
}

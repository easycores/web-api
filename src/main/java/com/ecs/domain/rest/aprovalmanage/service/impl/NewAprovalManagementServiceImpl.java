package com.ecs.domain.rest.aprovalmanage.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ecs.domain.rest.aprovalmanage.dao.NewAprovalManagementDAO;
import com.ecs.domain.rest.aprovalmanage.service.NewAprovalManagementService;
//import com.ecs.quadmax.common.Const;
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;
//승인/반려 처리 추가
import com.ecs.global.constant.Const;
//import com.ecs.quadmax.ecscontainer.campaign.model.CampaignVO;
import com.ecs.global.exception.ApproveException;
//import com.ecs.quadmax.exception.ApproveException;
import com.ecs.global.exception.ApproveTmsException;
//import com.ecs.quadmax.exception.ApproveTmsException;
import com.ecs.infrastructure.model.ResultMap;
import com.ecs.domain.rest.cms.api.service.CmsApiService;
//import com.ecs.quadmax.cms.api.service.CmsApiService;
import com.ecs.domain.rest.ecsmonitoring.dbpollinglist.model.SchedulerVO;
//import com.ecs.quadmax.ecsmonitoring.dbpollinglist.model.SchedulerVO;
import com.ecs.global.util.DateUtil;
//import com.ecs.quadmax.common.util.DateUtil;
import com.ecs.domain.rest.ecscontainer.scenario.service.EcsContainerService;
//import com.ecs.quadmax.ecscontainer.scenario.service.EcsContainerService;
import com.ecs.domain.rest.ecscontainer.campaign.dao.CampaignDAO;
//import com.ecs.quadmax.ecscontainer.campaign.dao.CampaignDAO;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
//import com.ecs.quadmax.ecscontainer.campaign.service.ContainerService;
import com.ecs.global.exception.QuadMaxException;
//import com.ecs.quadmax.exception.QuadMaxException;
import com.ecs.global.util.JsonUtil;
//import com.ecs.quadmax.common.util.JsonUtil;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


@Service("newAprovalManagementService")
public class NewAprovalManagementServiceImpl implements NewAprovalManagementService {

	public static final Logger LOGGER = LoggerFactory.getLogger(NewAprovalManagementServiceImpl.class);

	@Autowired
	private NewAprovalManagementDAO aprovalManagementDAO;
	
	@Autowired
	private ContainerService containerService;	

	@Autowired
	private CampaignDAO campaignDAO;	

	@Autowired
	private EcsContainerService ecsContainerService;

	@Autowired
	private CmsApiService cmsApiService;	
	
	private static final String CAMP_ID = "campId";
	private static final String APRV_FLAG = "aprvFlag";

	/**
	 * 내결제 대기리스트
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ResultMap> selectCampApproveWaitingList(Map<String, Object> paramMap) {
		return aprovalManagementDAO.selectCampApproveWaitingList(paramMap);
	}

	//승인요청/자가승인 추가
	@Override
	public String getCstsIdUntilApproval(String aprvStatCd) {
		String nextCstsId = null;
//		// 
//		if(aprvStatCd.equals("0") || aprvStatCd.equals("1")) {
//			nextCstsId = "002";
//		// 전자결제-승인완료, 자가승인
//		} else if(aprvStatCd.equals("2") || aprvStatCd.equals("5")) {
//			nextCstsId = "006";
//		// 전자결재-승인반송
//		} else if (aprvStatCd.equals("9")) {
//			nextCstsId = "069";
//		} 
		// 전자결제-승인요청 , 전자결제-승인완료, 자가승인
		if(aprvStatCd.equals("0") || aprvStatCd.equals("1") || aprvStatCd.equals("2") || aprvStatCd.equals("5")) {
			nextCstsId = "002";
		// 전자결재-승인반송
		} else if (aprvStatCd.equals("9")) {
			nextCstsId = "069";
		} 
		return nextCstsId; 
	}

	//승인/반려 처리 추가	
	/**
	 * 승인/반려 처리전 체크사항 체크 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public int checkAproval(Map<String, Object> paramMap) {
		int resultCnt =0;	// 7: 승인가능 | 1:요청캠페인체크 에러 | 2:결제라인체크에러 | 3:승인불가(에러)
		int reqCnt = aprovalManagementDAO.checkReqCampaign(paramMap);	//요청캠페인체크
		int aprvLineCnt = aprovalManagementDAO.checkAprvLine(paramMap);	//결제라인존재체크
		
//		String campId = (String) paramMap.get(CAMP_ID);
//		CampaignVO campInfo = containerService.readCampaign(campId);

		if(reqCnt>0 && aprvLineCnt>0){
			resultCnt = 7;	// 승인 가능
		}else if(reqCnt>0 && aprvLineCnt == 0){
			resultCnt = 1;	// 승인 불가
		}else if(reqCnt==0 && aprvLineCnt>0){
			resultCnt = 2;	// 승인 불가
		}else{
			resultCnt = 3;	// 승인불가
		}
		
		String dateCheck = aprovalManagementDAO.checkCampDate(paramMap);

		LOGGER.info("\n >> paramMap => {}", paramMap);
		//TODO paramMap.get("aprvFlag") 확인
		//boolean aprvFlag =  (boolean)(((String)paramMap.get(APRV_FLAG)).equals("true"));
		boolean aprvFlag =  false;
		aprvFlag = (boolean)("true".equals((String)paramMap.get(APRV_FLAG)));
		
		LOGGER.info("\n >> aprvFlag => {}", aprvFlag);
		
		if("N".equals(dateCheck) && aprvFlag) {
			resultCnt = 8;
		}

//		if ("NM_SQL".equals(campInfo.getCtypId()) && aprvFlag) {
//			String cellNodeId = getCellNodeIdForAprvSql(campId);
//			ResultMap res = cellDAO.selectCellNodeCntH(campId, cellNodeId);
//			if (res == null || CmmnUtil.toInt(res.get("countT")) <= 0) {
//				resultCnt = 4;
//			}
//		}

		return resultCnt;
	}

	//승인/반려 처리 추가
	/**
	 * 승인 / 반려 처리
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws ApproveException 
	 * @throws QuadMaxException 
	 * @throws Exception
	 */
	@Override
	public int aprvRsltLines(Map<String, Object> paramMap) throws IOException, SQLException, ApproveException, ApproveTmsException, QuadMaxException {
		int successCnt =0;
		//boolean aprvFlag =  (boolean)(((String)paramMap.get(APRV_FLAG)).equals("true"));
		boolean aprvFlag =  false;
		aprvFlag = (boolean)("true".equals((String)paramMap.get(APRV_FLAG)));

		String aprvStr = aprvFlag ? Const.APRV_STAT_APP : Const.APRV_STAT_RETURN;		// 승인처리 APRV_STAT_CD : 7(승인완료), 9(반려)
		String cstsStr = aprvFlag ? Const.CSTS_RUN_WAIT_K : "069";		// 캠페인상태 CSTS_ID : 006(승인완료), 069(승인반려)
		
		String campId = (String) paramMap.get(CAMP_ID);

		paramMap.put("oCd", "");
		paramMap.put("aprvStatCd", aprvStr);
		paramMap.put("cstsId", cstsStr);

		aprovalManagementDAO.aprvRsltLines(paramMap);

//		int lastSeq = aprovalManagementDAO.checkAprvLastSeq(paramMap);
		ResultMap nextLastSeqMap = aprovalManagementDAO.checkAprvNextLastSeq(paramMap);
		String lastSeq = (String) nextLastSeqMap.get("lastSeq");
		String nextSeq = (String) nextLastSeqMap.get("nextSeq");
		
		String seqStr = (String) paramMap.get("seq");

		if(!seqStr.equals(lastSeq) && aprvFlag) { // 다음 SEQ 상태코드 변경 (중간승인자의 승인일때만 변경)
			paramMap.put("nextSeq", nextSeq);
			aprovalManagementDAO.updateNextAprvCd(paramMap);
		}
		else { // 자가승인 or 최종승인시 스케줄 등록
//			// 앱푸시가 포함되어있을 경우 제작요청 상태로 변경
//			String apppushCampF = aprovalManagementDAO.isApppushCampaign(campId);
//			if ("006".equals(cstsStr) && "Y".equals(apppushCampF)) {
//				
//				paramMap.put("map1", "${map1}");
//				paramMap.put("map2", "${map2}");
//				paramMap.put("map3", "${map3}");
//				paramMap.put("map4", "${map4}");
//				List<ResultMap> appContList = cellContentsDAO.selectAppContentsList(paramMap);
//				
//				if (!apppushBytesCheck(appContList)) {
//					throw new ApproveException("앱푸시 제작요청 바이트수를 초과했습니다.");
//				}
//				
//				List<String> validTargetCustomIdList = new ArrayList<>(); 
//				
//				for (Map<String, Object> appCont : appContList) {
//					String nodeId = (String) appCont.get("cellNodeId");
//					String contId = (String) appCont.get("contId");
//					
//					LOGGER.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//					LOGGER.debug("{}", appCont);
//					LOGGER.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//					
//					for (Map.Entry<String, Object> entry : appCont.entrySet()) {
//						if (entry.getValue() instanceof String)
//							appCont.put(entry.getKey(), Util.changeStrOfEuckr((String) entry.getValue()));
//					}
//					
//					// 광고, 수신거부 문구 체크
//					String msgPreCd = (String) appCont.getOrDefault("msgPreCd", "");
//					String rejectCd = (String) appCont.getOrDefault("rejectCd", "");
//					String contName = (String) appCont.getOrDefault("pushTitle", "");
//					String pushMsgText = (String) appCont.getOrDefault("pushMsg", "");
//					String campTypeCd = (String) appCont.getOrDefault("campTypeCd", "");
//					if (campTypeCd.equals("PC")) {
//						if (!pushMsgText.contains(rejectCd)) {
//							throw new ApproveException("수신거부 문구가 포함되지 않았습니다.");
//						}
//						if (!pushMsgText.contains(pushMsgText.replace(rejectCd, ""))) {
//							throw new ApproveException("<PUSH 수신거부 문구 중복> PUSH 수신거부문구는 자동적용되므로 제외해주십시요.");
//						}
//						if (contName.indexOf(msgPreCd) != 0) {
//							throw new ApproveException("제목에 (광고) 문구가 포함되지 않았습니다.");
//						}
//					}
//					
//					// tb_crm_info 테이블에 적재
//					tmsDAO.insertTmsCrmInfo(appCont);
//					LOGGER.debug("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
//					LOGGER.debug("{}", appCont);
//					LOGGER.debug("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
//					
//					int seq = CmmnUtil.toInt(appCont.get("seq"));
//					String targetCustomId = campId + "_" + nodeId + "_" + contId + "_" + seq;
//					paramMap.put("targetCustomId", targetCustomId);
//					
//					validTargetCustomIdList.add(targetCustomId);
//					
//					paramMap.put("nodeId", nodeId);
//					paramMap.put("contId", contId);
//					List<ResultMap> testTargetList = cellContentsDAO.selectContTestTargetList(paramMap);
//					for (ResultMap testTarget : testTargetList) {
//						// tb_test_target 테이블에 적재
//						Map<String, Object> param = new HashMap<>();
//						param.put("custId", testTarget.get("custId"));
//						param.put("targetCustomId", targetCustomId);
//						tmsDAO.insertTestTarget(param);
//					}
//					
//					// t_camp_contents에 target_custom_id update
//					cellContentsDAO.updateTargetCustomId(paramMap);
//					
//					aprovalManagementDAO.updateArpvStatCd(CmmnUtil.toInt(paramMap.get("aprvReqNo")), "61", targetCustomId);
//					paramMap.put("nextCstsId", "061");
//					aprovalManagementDAO.updateCstsAprovalForCampaign(paramMap);
//					aprovalManagementDAO.updateCstsAprovalForContainer(paramMap);
//
//					// 검증1
//					int testCnt = tmsDAO.selectTestTargetCount(targetCustomId);
//					LOGGER.debug("테스트건수검증 : {}, {}", testCnt, testTargetList.size());
//					if (testCnt != testTargetList.size()) {
//						throw new ApproveTmsException("TMS푸시요청 테스트 건수가 맞지 않습니다.");
//					}
//				}
//				
//				// 검증2
//				Map<String, Object> validParamMap = new HashMap<>();
//				validParamMap.put("list", validTargetCustomIdList);
//				int crmInfoCnt = tmsDAO.selectTmsCrmInfoCount(validParamMap);
//				LOGGER.debug("푸시요청건수검증 : {}, {}", crmInfoCnt, appContList.size());
//				if (crmInfoCnt != appContList.size()) {
//					throw new ApproveTmsException("TMS푸시요청 건수가 맞지 않습니다.");
//				}
//			} else {
				CampaignVO campInfo = containerService.readCampaign((String)paramMap.get(CAMP_ID));
				
				if ("7".equals(aprvStr)) {
					// 실시간일경우 이력쌓아줌
					successCnt = setEbmHistory(paramMap);
					// 스케쥴 등록
					successCnt = setApproveSchedule(paramMap);
				}
				
				paramMap.put("aprvStatCd",  aprvStr);	// 결재상태
				// 승인정보 업데이트
				aprovalManagementDAO.updateAprvInfo(paramMap);
				
				// 다음 캠페인 상태 구한다.
				String nextCstsId = cstsStr;
				if (!"069".equals(cstsStr)) {
					nextCstsId = getCstsIdAfterApproval(campInfo);
				}
				if (nextCstsId == null) {
					nextCstsId = getCstsIdUntilApproval(aprvStr);
				}
				paramMap.put("nextCstsId", nextCstsId);	// 캠페인상태 
				// 캠페인 상태 업데이트
				aprovalManagementDAO.updateCstsAprovalForCampaign(paramMap);
				aprovalManagementDAO.updateCstsAprovalForContainer(paramMap);
				
				if ("7".equals(aprvStr)) {
					// EBM일 경우 Redis 적재
					if (!"EBNM".equals(campInfo.getCtypId()) && "EBM".equals(campInfo.getCampBaseType())) {
						ObjectMapper mapper = new ObjectMapper();
						String nodeCommand = aprovalManagementDAO.selectNodeCommand(paramMap);
						if (nodeCommand != null) {
							ObjectNode nodeCommandJson = mapper.readValue(nodeCommand, ObjectNode.class);
							String scheduleType = Optional.of(nodeCommandJson.get("scheduleType").textValue()).orElseThrow(NullPointerException::new);
							String retargetingYn = "1".equals(scheduleType) || "2".equals(scheduleType) || "3".equals(scheduleType) ? "Y" : "N";
							LOGGER.debug("레디스 적재 retargetingYN : {}", retargetingYn);
							if ("N".equals(retargetingYn)) {
								cmsApiService.reqRedisApi(campId, "");
								
							}
							// 지정일시일경우
							String appointDatetimeStatus = "1".equals(scheduleType) ? "Y" : "N";
							paramMap.put("appointDatetimeStatus", appointDatetimeStatus);
							campaignDAO.updateAppointDatetimeStatus(paramMap);
						} else {
							cmsApiService.reqRedisApi(campId, "");
							
							String appointDatetimeStatus = "N";
							paramMap.put("appointDatetimeStatus", appointDatetimeStatus);
							campaignDAO.updateAppointDatetimeStatus(paramMap);
						}
					}
				}
//			}
		}
		
		
		if(((String)paramMap.get("oCd")).equals("ERR")){
			successCnt = 4; // 메일 발송 실패시 에러 코드
		} else if (successCnt == 0){
			successCnt = 7;
		}
		
		return successCnt;
	}
		
	//승인/반려 처리 추가	
	@Override
	public void callSpEtlErrorAmsLog(String procName, String errorMessage) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("procName", procName);
		paramMap.put("errorMessage", errorMessage);
		aprovalManagementDAO.callSpEtlErrorAmsLog(paramMap);
	}	

	//승인/반려 처리 추가	
	/**
	 * 최종 승인시 EBM 실행이력 생성
	 * @throws IOException 
	 */
	@Override
	public int setEbmHistory(Map<String, Object> paramMap) throws IOException {
		int successCnt = 7;
		String campId = (String) paramMap.get(CAMP_ID);
		
		// T_CAMPAIGN 조회
		CampaignVO vo = campaignDAO.selectCampaign(campId);
		if ("EBM".equals(vo.getCampBaseType())) {
			String campMode = vo.getCampMode();
			
			String url = ecsContainerService.getMasterServerURL();
			if (url.equals("N/E")) {
				successCnt = 5;
				return successCnt;
			} else if (url.equals("N/A")) {
				successCnt = 6;
				return successCnt;
			} else {
				// 스케줄 등록여부 확인
				RestTemplate restTemplate = new RestTemplate();
				url += "/query/command";
				StringBuilder sb = new StringBuilder();
				sb.append("{")
						.append("   \"Command\": \"exec_HistoryEbm\",")
						.append("   \"Output\": \"csv\",")
						.append("   \"userid\": \"").append("admin").append("\",")
						.append("   \"parameter\": {")
						.append("       \"@CAMP_ID\": \"").append(campId).append("\",")
						.append("       \"@CAMP_MODE\": \"").append(campMode).append("\"")
						.append("   }")
						.append("}");
				
				HttpEntity<String> req = setHeader(sb.toString());
				restTemplate.postForEntity(url, req, String.class);
			}
		}
		
		return successCnt;
	}	
	
	//승인/반려 처리 추가
	/**
	 * PR_CMS_APRV_SCHEDULE
	 * 최종 승인시 스케줄 생성
	 * @throws IOException 
	 * @throws ApproveException 
	 */
	@Override
	public int setApproveSchedule(Map<String, Object> paramMap) throws IOException, ApproveException {
		int successCnt = 7;
		String schStartDate = null;
		String schEndDate = null;
		String dayString = null;
		String campId = (String) paramMap.get(CAMP_ID);
		
		// T_CAMP_SUBMITCOMMANDS SCHEDULING 노드 개수 조회
		int cnt = aprovalManagementDAO.selectScheduleExistCnt(paramMap);
		if (cnt < 1) {
			return successCnt;
		}
		
		String url = ecsContainerService.getMasterServerURL();
		
		LOGGER.info("\n >> url => {}", url);
		
		if (url.equals("N/E")) {
			throw new ApproveException("현재  등록된  캠페인 엔진 서버가 없습니다. 관리자에 확인바랍니다.");
		} else if (url.equals("N/A")) {
			throw new ApproveException("현재  가용한  캠페인 엔진 서버가 없습니다. 관리자에 확인바랍니다.");
		} else {
			// 스케줄 등록여부 확인
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> resGet = restTemplate.getForEntity(url + "/scheduler/schedule/" + campId, Object.class);
			
			ObjectMapper mapper = new ObjectMapper();
			String nodeCommand = aprovalManagementDAO.selectNodeCommand(paramMap);
			ObjectNode nodeCommandJson = mapper.readValue(nodeCommand, ObjectNode.class);
			JsonNode batchBody = nodeCommandJson.get("batch_body");
			HttpEntity<String> req = setHeader(batchBody.get(0).toString());
			StringBuilder sb = new StringBuilder()
					.append(url)
					.append("/scheduler/schedule/")
					.append(campId);
			String schUrl = sb.toString();
			
			// 스케줄 등록 안되어 있을 시
			if(resGet.getBody().toString().equals("{}") || resGet.getBody() == null) {	
				schStartDate = DateUtil.getCurrentYyyymmdd();
				schEndDate = DateUtil.getCurrentYyyymmdd();
				dayString = DateUtil.getToString(DateUtil.addSecond(new Date(), 10), "HHmmss");
				
				paramMap.put("schId", campId);
				paramMap.put("schStartDate", schStartDate);
				paramMap.put("schEndDate", schEndDate);
				paramMap.put("dayString", dayString);
				
				restTemplate.postForEntity(schUrl, req, String.class);
			} else {
				CampaignVO campaign = campaignDAO.selectCampaign(campId);
				Object schInfo = resGet.getBody();
				
				if (schInfo != null && campaign != null) {
					String cstsId = campaign.getCstsId();
										
					// 캠페인 상태가 006(승인), 007(실행중), 008(완료), 009(정지), 010(템플릿)이 아닐 경우에만 처리
					if(!(
							   Const.CSTS_RUN_WAIT_K.equals(cstsId) 	// 승인/실행대기
							|| Const.CSTS_RUN_K.equals(cstsId) 	// 실행중
							|| Const.CSTS_END_K.equals(cstsId) 	// 완료
							|| Const.CSTS_STOP_K.equals(cstsId) 	// 정지
							|| Const.CSTS_TEMP_K.equals(cstsId)	// 템플릿
						)) {
						
						restTemplate.exchange(schUrl, HttpMethod.PUT, req, String.class);						
					}
				}
			}
			
			return successCnt;
		}
	}

	//승인/반려 처리 추가
	@Override
	public String getCstsIdAfterApproval(CampaignVO campInfo) {
		// 007, 021, 022
		String nextCstsId = null;
		String now = LocalDate.now(ZoneId.of("Asia/Seoul")).format(DateTimeFormatter.ofPattern("yyyyMMdd"));

		if ("EBM".equals(campInfo.getCampBaseType())) {
			nextCstsId = "007";
		} else if ("TARGET".equals(campInfo.getCampBaseType())) {
			// 타게팅캠페인은 승인완료(
			SchedulerVO schVO = getScheduleInfo(campInfo.getCampId());
			LOGGER.debug("승인 스케쥴 정보 1 : {}", schVO);
			if (schVO.getStartDT()!=null && Integer.parseInt(schVO.getStartDT().substring(0, 8)) <= Integer.parseInt(now) ||
					Integer.parseInt(campInfo.getCampSdate()) <= Integer.parseInt(now)) {
				// 승인시점이 캠페인스케줄 시작일 이후인 경우
				nextCstsId = "006";
			}
		}
		
		return nextCstsId;
	}	

	//승인/반려 처리 추가
	public HttpEntity<String> setHeader(String body) {
		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
		headers.setContentType(mediaType);
		return new HttpEntity<>(body, headers);
	}	
	
	//승인/반려 처리 추가
	public SchedulerVO getScheduleInfo (String campId) {
		String url = null;
		SchedulerVO schVO = null;
		try {
			url = ecsContainerService.getMasterServerURL();
		
			if ("N/E".equals(url)) {
				LOGGER.error("Scheduler Server N/E");
			} else if ("N/A".equals(url)) {
				LOGGER.error("Scheduler Server N/A");
			} else {
				// 스케줄 등록여부 확인
				RestTemplate restTemplate = new RestTemplate();
				ResponseEntity<Object> resGet = restTemplate.getForEntity(url + "/scheduler/schedule/" + campId, Object.class);
				schVO = JsonUtil.fromJsonGson(JsonUtil.toJson(resGet.getBody()), SchedulerVO.class);
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
		}
		return schVO;
	}
	
	//승인요청취소 추가
	/**
	 * 결제순번 찾기
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Integer getAprvReqNo(Map<String, Object> paramMap) {
		return aprovalManagementDAO.getAprvReqNo(paramMap);
	}
	
	//승인요청취소 추가
	/**
	 * 승인요청취소 체크
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public int checkApproveReqCancle(Map<String, Object> paramMap) {
		return aprovalManagementDAO.checkApproveReqCancle(paramMap);
	}
	
	//승인요청취소 추가
	/**
	 * 승인요청취소 처리
	 * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws Exception
	 */
	@Override
	public int aprvRsltLinesCancle(Map<String, Object> paramMap) throws IOException, SQLException {
		int successCnt =0;
		
		paramMap.put("aprvStatCd", Const.APRV_STAT_EMI_CANCEL);	// 전자결재 취소 004
//		paramMap.put("aprvState", Const.APRV_STAT_CANCEL);	// ( 현재 사용처 없음 ) 
		paramMap.put("nextCstsId", Const.CSTS_CAN_K);	// 승인요청취소 005
		
		aprovalManagementDAO.aprvRsltLines(paramMap);
		aprovalManagementDAO.updateAprvInfo(paramMap);
		aprovalManagementDAO.updateCstsAprovalForCampaign(paramMap);
		aprovalManagementDAO.updateCstsAprovalForContainer(paramMap);
		
		successCnt = 7; // 정상 승인요청취소
		
		return successCnt;
	}

	/**
	 * 내 캠페인 승인 현황 리스트
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ResultMap> selectCampApproveHistoryList(Map<String, Object> paramMap) {
		//		String srchCampBaseType = (String) paramMap.get("srchCampBaseType");
//		if (srchCampBaseType != null && !"".equals(srchCampBaseType.trim())) {
//			String[] campBaseTypes = srchCampBaseType.split(",");
//			paramMap.put("campBaseTypes", campBaseTypes);
//		}
		return aprovalManagementDAO.selectCampApproveHistoryList(paramMap);

	}
}


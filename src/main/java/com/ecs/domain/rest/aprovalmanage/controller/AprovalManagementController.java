package com.ecs.domain.rest.aprovalmanage.controller;

//import com.ecs.quadmax.login.model.UserVO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ecs.domain.rest.aprovalmanage.model.ApprovalGetResponse;
import com.ecs.domain.rest.aprovalmanage.model.ApprovalGetRequest;
import com.ecs.domain.rest.aprovalmanage.service.NewAprovalManagementService;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
import com.ecs.global.i18n.QuadMaxMessageSource;
import com.ecs.global.util.CmmnUtil;
import com.ecs.infrastructure.model.EcsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.ecs.quadmax.exception.QuadMaxException;
import com.ecs.domain.common.login.model.UserVO;
import com.ecs.domain.rest.aprovalmanage.service.NewAprovalManagementService;
import com.ecs.domain.rest.ecscontainer.campaign.service.ContainerService;
import com.ecs.domain.rest.ecscontainer.scenario.model.api.EcsContainerGetRequest;
import com.ecs.domain.rest.ecscontainer.scenario.model.api.EcsContainerGetResponse;
import com.ecs.global.constant.Const;
//import com.ecs.quadmax.common.util.SessionUtil;
import com.ecs.global.exception.ApproveException;
//import com.ecs.quadmax.exception.ApproveException;
import com.ecs.global.exception.ApproveTmsException;
//import com.ecs.quadmax.exception.ApproveTmsException;
import com.ecs.global.exception.QuadMaxException;
import com.ecs.global.i18n.QuadMaxMessageSource;
//승인/반려 처리 추가
import com.ecs.global.util.SessionUtil;
import com.ecs.infrastructure.model.EcsResponse;
import com.ecs.infrastructure.model.ResultMap;

import com.ecs.domain.rest.aprovalmanage.model.api.AprovalManagementGetRequest;
import com.ecs.domain.rest.aprovalmanage.model.api.AprovalManagementGetResponse;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author jhjung
 *
 */
@RestController
public class AprovalManagementController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AprovalManagementController.class);
	@Autowired
	private ContainerService containerService;

	@Resource(name="quadMaxMessageSource")
	private QuadMaxMessageSource quadMaxMessageSource;

	@Autowired
	private NewAprovalManagementService newAprovalManagementService;
	/**
	 * 내결제 대기리스트
	 */
	@GetMapping(value="approvals")
	public EcsResponse<ApprovalGetResponse.selectCampApproveWaitingListDTO> selectCampApproveList(@RequestParam @Valid ApprovalGetRequest.SelectCampApproveWaitingListDTO selectCampApproveWaitingListDTO) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

		System.out.println("==================================DTO==================================");
		System.out.println(selectCampApproveWaitingListDTO);
		Map<String, Object> selectCampApproveWaitingListMap = CmmnUtil.convertToMap(selectCampApproveWaitingListDTO);
		System.out.println("===================================selectCampApproveWaitingListMap=================================");
		System.out.println(selectCampApproveWaitingListMap);

		List<ApprovalGetResponse.selectCampApproveWaitingListDTO> list= CmmnUtil.convertResultMapList(newAprovalManagementService.selectCampApproveWaitingList(selectCampApproveWaitingListMap), ApprovalGetResponse.selectCampApproveWaitingListDTO.class) ;




//		if(selectCampApproveWaitingListDTO.getType().equals("history")){
//			list = CmmnUtil.convertResultMapList(newAprovalManagementService.selectCampApproveHistoryList(selectCampApproveHistoryListDTO), ApprovalGetResponse.selectCampApproveHistoryListDTO.class);
//		}

		return new EcsResponse(HttpStatus.OK);
	}
	
	
	//승인/반려 처리 추가
	/**
	 * 승인/반려 처리
	 * @param model
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws QuadMaxException 
	 * @
	 */
	@PostMapping(value = "/campaigns/{campId}/approvals/{aprvReqNo}")
	public EcsResponse<List<AprovalManagementGetResponse.ResponseListDTO>>  approveAction(@PathVariable("campId") String campId, @PathVariable("aprvReqNo") long aprvReqNo, AprovalManagementGetRequest.RequestListDTO reqDto) throws IOException {

		String aprvReqSeq = "";

		Map<String, Object> paramMap = new HashMap<>();

		LOGGER.info(" >> reqDto => {}", reqDto);

		if  (reqDto.getSeq() == null || "".equals(reqDto.getSeq())) {
			aprvReqSeq = "1";
		} else {
			aprvReqSeq = reqDto.getSeq();
		}

		if ("approved".equals(reqDto.getStatus())) {
			LOGGER.info("\n >> 승인");
			paramMap.put("campId", campId);
			paramMap.put("aprvReqNo", aprvReqNo);
			paramMap.put("seq", aprvReqSeq);
			paramMap.put("aprvStatCd", "7");
			paramMap.put("aprvFlag", "true");
		} else if ("disapproved".equals(reqDto.getStatus())) {
			LOGGER.info("\n >> 반려");
			paramMap.put("campId", campId);
			paramMap.put("aprvReqNo", aprvReqNo);
			paramMap.put("seq", aprvReqSeq);
			paramMap.put("aprvStatCd", "9");
			paramMap.put("aprvFlag", "false");
		} else {
			LOGGER.info("\n >> 추후 재확인 필요");
			return new EcsResponse(HttpStatus.NO_CONTENT);
		}
		
		UserVO userInfo = (UserVO) SessionUtil.getAttribute(Const.SESSION_USER);
		//TODO user 코멘트 처리 주석 해제할것
		//paramMap.put("aprvEmpId", userInfo.getEmpId());
		paramMap.put("aprvEmpId", "admin");
		int successCnt=0;	//승인/반려처리
		String errorMsg="";	// 승인/반려 체크중 오류 발견
		
		//String campId = (String) paramMap.get("campId");

		//승인/반려전 체크사항 체크처리
		int checkCnt = newAprovalManagementService.checkAproval(paramMap);

		//checkCnt -> 7: 승인가능 | 1:요청캠페인체크 에러 | 2:결제라인체크에러 | 3:승인불가(에러)
		if(checkCnt == 7){
			//승인/반려 처리
			try {
					successCnt = newAprovalManagementService.aprvRsltLines(paramMap);

//				if (successCnt == 7) {
//					List<String> targetCustomIdList = newAprovalManagementService.selectTargetCustomIdList(campId);
//					for (String targetCustomId : targetCustomIdList) {
//						// 요청성공시 사내앱푸시 보낸다.
//						if (targetCustomId != null && !targetCustomId.equals(""))
//							tmsService.callSoap(targetCustomId, "001");
//					}
//				}
				
				//model.addAttribute(Const.ERR_MSG, errorMsg);
			} catch(IOException e) {
				LOGGER.error("AprovalManagementController IOException", e);	
			} catch(SQLException e) {	
				LOGGER.error("AprovalManagementController SQLException", e);
			} catch(ApproveException e) {
				LOGGER.error("AprovalManagementController ApproveException", e);
			} catch(ApproveTmsException e) {
				LOGGER.error("AprovalManagementController ApproveTmsException", e);
				// 인터페이스 실패시 프로시져 호출
				try {
					newAprovalManagementService.callSpEtlErrorAmsLog(null, "["+campId+"]" + e.getMessage());
				} catch(Exception ex) {
					LOGGER.error("callSpEtlErrorAmsLog 오류", ex);
				}
				//model.addAttribute("errMsg", e.getMessage());
			} catch(QuadMaxException e) {
				LOGGER.error("AprovalManagementController QuadMaxException", e);
			}
		} else {
			successCnt = checkCnt;

			if(successCnt == 1){
				errorMsg = quadMaxMessageSource.getMessage("MM0020"); // 선택한 캠페인은 승인/반려권한이 없습니다.
			}else if(successCnt ==2){
				errorMsg = quadMaxMessageSource.getMessage("MM0021"); // 선택한 캠페인은 승인요청한 캠페인이 아닙니다.
			}else if(successCnt ==3){
				errorMsg = quadMaxMessageSource.getMessage("MM0022"); // 선택한 캠페인은 승인/반려가 불가능합니다.
			}else if(successCnt ==4) {
				errorMsg = quadMaxMessageSource.getMessage("MC0197"); // 추출 고객군수를 확인해 주세요.
			}else if(successCnt ==8) {
				errorMsg = "발송일을 확인해주세요.";
			}
			//model.addAttribute("errMsg", errorMsg);
		}

		//model.addAttribute("aprvFlag", paramMap.get("aprvFlag"));
		//model.addAttribute("successCnt", successCnt);

		return new EcsResponse(HttpStatus.OK);
	}
	
}

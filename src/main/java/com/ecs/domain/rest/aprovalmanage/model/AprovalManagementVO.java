package com.ecs.domain.rest.aprovalmanage.model;

public class AprovalManagementVO {
	private String aprvReqNo;		//승인요청번호(APRV_REQ_NO_SEQ)
	private String campId;			//캠페인ID
	private String aprvStatCd;		//승인상태(1:승인요청,2:승인완료,3:반려,4:요청취소,5:자가승인)
	private String aprvDesc;		//비고
	private String aprvReqEmpId;	//승인요청자ID
	private String aprvReqDttm;		//승인요청일시
	private String aprvEmpId;		//최종승인자ID
	private String aprvDttm;		//최종승인일시
	private String aprvId;			//전자결재ID
	private String regEmpId;		//등록자ID
	private String regDate;			//등록일자
	private String regTime;			//등록시간
	private String updEmpId;		//수정자ID
	private String updDate;			//수정일자
	private String updTime;			//수정시간
	//----------------------------------------
	public String getAprvReqNo() {
		return aprvReqNo;
	}
	public void setAprvReqNo(String aprvReqNo) {
		this.aprvReqNo = aprvReqNo;
	}
	public String getCampId() {
		return campId;
	}
	public void setCampId(String campId) {
		this.campId = campId;
	}
	public String getAprvStatCd() {
		return aprvStatCd;
	}
	public void setAprvStatCd(String aprvStatCd) {
		this.aprvStatCd = aprvStatCd;
	}
	public String getAprvDesc() {
		return aprvDesc;
	}
	public void setAprvDesc(String aprvDesc) {
		this.aprvDesc = aprvDesc;
	}
	public String getAprvReqEmpId() {
		return aprvReqEmpId;
	}
	public void setAprvReqEmpId(String aprvReqEmpId) {
		this.aprvReqEmpId = aprvReqEmpId;
	}
	public String getAprvReqDttm() {
		return aprvReqDttm;
	}
	public void setAprvReqDttm(String aprvReqDttm) {
		this.aprvReqDttm = aprvReqDttm;
	}
	public String getAprvEmpId() {
		return aprvEmpId;
	}
	public void setAprvEmpId(String aprvEmpId) {
		this.aprvEmpId = aprvEmpId;
	}
	public String getAprvDttm() {
		return aprvDttm;
	}
	public void setAprvDttm(String aprvDttm) {
		this.aprvDttm = aprvDttm;
	}
	public String getAprvId() {
		return aprvId;
	}
	public void setAprvId(String aprvId) {
		this.aprvId = aprvId;
	}
	public String getRegEmpId() {
		return regEmpId;
	}
	public void setRegEmpId(String regEmpId) {
		this.regEmpId = regEmpId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getUpdEmpId() {
		return updEmpId;
	}
	public void setUpdEmpId(String updEmpId) {
		this.updEmpId = updEmpId;
	}
	public String getUpdDate() {
		return updDate;
	}
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	public String getUpdTime() {
		return updTime;
	}
	public void setUpdTime(String updTime) {
		this.updTime = updTime;
	}
	
}

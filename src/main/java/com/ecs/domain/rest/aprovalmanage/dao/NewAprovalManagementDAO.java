package com.ecs.domain.rest.aprovalmanage.dao;

import com.ecs.infrastructure.model.ResultMap;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface NewAprovalManagementDAO {
	/**
	 * 내결제 대기리스트
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectCampApproveWaitingList(Map<String, Object> paramMap);
	
	//승인요청/자가승인 추가
	/**
	 * 캠페인 상태 (캠페인상태 CSTS_ID : 승인완료(006), 승인반려(004) ) 변경
	 * @param paramMap
	 * @return
	 */
	int updateCstsAprovalForCampaign(Map<String, Object> paramMap);
	
	//승인요청/자가승인 추가
	/**
	 * 캠페인 상태 (캠페인상태 CSTS_ID : 승인완료(006), 승인반려(004) ) 변경
	 * @param paramMap
	 * @return
	 */
	int updateCstsAprovalForContainer(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 승인전 체크사항 체크 (요청된 캠페인 확인)
	 * @param paramMap
	 * @return
	 */
	int checkReqCampaign(Map<String, Object> paramMap);

	//승인/반려 처리 추가
	/**
	 * 승인전 체크사항 체크(결제라인존재유무확인)
	 * @param paramMap
	 * @return
	 */
	int checkAprvLine(Map<String, Object> paramMap);

	//승인/반려 처리 추가
	String checkCampDate(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 승인/반려 처리
	 * @param paramMap
	 * @return
	 */
	void aprvRsltLines(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 마지막/다음 승인자 체크
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	ResultMap checkAprvNextLastSeq(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 다음 승인자 상태코드 변경
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int updateNextAprvCd(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 승인정보 업데이트(승인처리 APRV_STAT_CD : 2(승인완료), 3(반려) )
	 * @param paramMap
	 * @return
	 */
	int updateAprvInfo(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	String selectNodeCommand(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	Map<String, Object> callSpEtlErrorAmsLog(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int selectScheduleExistCnt(Map<String, Object> paramMap);
	
	//승인요청취소 추가
	/**
	 * 승인요청번호
	 * @param paramMap
	 * @return
	 */
	Integer getAprvReqNo(Map<String, Object> paramMap);
	
	//승인요청취소 추가
	/**
	 * 승인요청취소 체크(승인취소전 체크)
	 * @param paramMap
	 * @return
	 */
	int checkApproveReqCancle(Map<String, Object> paramMap);

	List<ResultMap> selectCampApproveHistoryList(Map<String, Object> paramMap);

}

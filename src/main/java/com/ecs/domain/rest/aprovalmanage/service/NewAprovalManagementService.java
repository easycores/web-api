package com.ecs.domain.rest.aprovalmanage.service;

import com.ecs.infrastructure.model.ResultMap;

import java.util.List;
import java.util.Map;

//승인/반려 처리 추가
import com.ecs.domain.rest.ecscontainer.campaign.model.CampaignVO;
//import com.ecs.quadmax.ecscontainer.campaign.model.CampaignVO;
import com.ecs.global.exception.ApproveException;
import com.ecs.global.exception.ApproveTmsException;
import com.ecs.global.exception.QuadMaxException;

import java.io.IOException;
import java.sql.SQLException;



public interface NewAprovalManagementService {
	/**
	 * 내결제 대기리스트
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectCampApproveWaitingList(Map<String, Object> paramMap);
	
	//승인요청/자가승인 추가
	/**
	 * 다음 상태(CSTS_ID)값 추출
	 * 승인완료 이후의 상태 조건에 해당하지 않는 경우 사용.
	 */
	String getCstsIdUntilApproval(String aprvStatCd);
	
	//승인/반려 처리 추가
	/**
	 * 승인전 체크사항 (1.승인요청된 캠페인, 2.결제라인)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int checkAproval(Map<String, Object> paramMap);
	
	//승인/반려 처리 추가
	/**
	 * 승인 / 반려 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int aprvRsltLines(Map<String, Object> paramMap) throws IOException, SQLException, ApproveException, ApproveTmsException, QuadMaxException;
	
	//승인/반려 처리 추가
	void callSpEtlErrorAmsLog(String procName, String errorMessage);
	
	//승인/반려 처리 추가
	/**
	 * 다음 상태(CSTS_ID)값 추출
	 * 최종승인 및 자가승인일 경우에 사용.
	 * 승인완료 이후의 상태 조건에 해당하지 않는 경우 NULL 반환
	 * NULL 반환 시 getCstsIdUntilApproval 함수 사용하여 기타 조건으로 상태값 리턴 
	 */
	String getCstsIdAfterApproval(CampaignVO campInfo);
	
	//승인/반려 처리 추가
	/**
	 * EBM 실행이력 생성
	 * @param paramMap
	 * @throws Exception
	 */
	int setEbmHistory(Map<String, Object> paramMap) throws IOException;
	
	//승인/반려 처리 추가
	/**
	 * 스케줄 생성
	 * @param paramMap
	 * @throws Exception
	 */
	int setApproveSchedule(Map<String, Object> paramMap) throws IOException, ApproveException;
	
	//승인요청취소 추가
	/**
	 * 결제요청번호
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	Integer getAprvReqNo(Map<String, Object> paramMap);
	
	//승인요청취소 추가
	/**
	 * 승인요청취소 체크
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int checkApproveReqCancle(Map<String, Object> paramMap);
	
	//승인요청취소 추가
	/**
	 * 승인요청취소 처리
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	int aprvRsltLinesCancle(Map<String, Object> paramMap) throws IOException, SQLException;

	/**
	 * 승인/반려 이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	List<ResultMap> selectCampApproveHistoryList(Map<String, Object> paramMap);

}

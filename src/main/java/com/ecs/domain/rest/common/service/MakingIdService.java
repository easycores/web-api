package com.ecs.domain.rest.common.service;
/**
 *
 * @author h2y
 *
 */
public interface MakingIdService {

	/**
	 * 타입별ID생성.
	 * @param idType
	 * @param idShow
	 * @param nHex
	 * @param length
	 * @return String
	 */
	String getId(String idType, String idShow, int nHex, int length);
	
	/**
	 * 타입별캠페인ID생성.
	 * @param idType
	 * @param idShow
	 * @param nHex
	 * @param length
	 * @return String
	 */
	String getCampId(String idType, String idShow, int nHex, int length);

	/**
	 * 타입별ID생성.
	 * @param idType
	 * @param idShow
	 * @param length
	 * @return String
	 */
	String getId(String idType, String idShow, int numLength);

	/**
	 * 타입별ID생성.
	 * @param idType
	 * @return String
	 */
	String getId(String idType);

	/**
	 * 노드 ID생성.
	 *
	 * @param outId the out id
	 * @return String
	 */
	String getNodeId(String outId) throws InterruptedException;

	/**
	 * 스케줄 ID생성.
	 *
	 * @return String
	 */
	String getSchId();

	/**
	 * @param idType
	 * @param year
	 * @return
	 * @throws Exception
	 */
	int getSerialNumber(String idType, String year);

	/**
	 * @param idType
	 * @param year
	 * @return
	 * @throws Exception
	 */
	int insertSerialNumber(final String idType, final String year);

	/**
	 * @param idType
	 * @param year
	 * @param idx
	 * @return
	 * @throws Exception
	 */
	int updateSerialNumber(final String idType, final String year, final int idx);

	/**
	 * @param idType
	 * @return
	 * @throws Exception
	 */
	int deleteSerialNumber(final String idType);

	/**
	 *
	 * @param strText
	 * @param cSpace
	 * @param iTotalSize
	 * @return String
	 */
	String fixTextSize(String strText, char cSpace, int iTotalSize);

	/**
	 *
	 * @param nValue
	 * @param nHex
	 * @param nMax
	 * @return String
	 */
	String getHexString(int nValue, int nHex, int nMax);

	/**
	 * @param prefix
	 * @param seqName
	 * @param len
	 * @return
	 * 
	 */
	String getSeqId(String prefix, String seqName, int len);
}

package com.ecs.domain.rest.common.service.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ecs.domain.rest.common.dao.MakingIdDAO;
//import com.ecs.quadmax.common.dao.MakingIdDAO;
import com.ecs.domain.rest.common.service.MakingIdService;
//import com.ecs.quadmax.common.service.MakingIdService;
import com.ecs.global.util.DateUtil;
//import com.ecs.quadmax.common.util.DateUtil;
import com.ecs.global.util.StringUtil;
//import com.ecs.quadmax.common.util.StringUtil;

/**
 *
 * @author h2y
 *
 */
@Service("makingIdService")
public class MakingIdServiceImpl implements MakingIdService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MakingIdServiceImpl.class);

	@Autowired
	private MakingIdDAO makingIdDAO;

	@Override
	@Transactional(value="txManager", propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public synchronized String getId(
			final String idType,
			final String idShow,
			final int nHex,
			final int length) {

		String result = null;
		String year = null;
		int serialNumber = 0;

		year = DateUtil.getCurrentYear();

		serialNumber = getSerialNumber(idType, year);

		LOGGER.debug("serialNumber : {}", serialNumber);
		
		result = idShow.trim().toUpperCase()
				+ String.valueOf(year).substring(2, 4)	// YEAR_SUBSTR_MAX
				+ fixTextSize(getHexString(serialNumber, nHex, length), '0', length);

		LOGGER.debug("result : {} ", result);

		return result;
	}
	
	@Override
	@Transactional(value="txManager", propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public synchronized String getCampId(
			final String idType,
			final String idShow,
			final int nHex,
			final int length) {

		String result = null;
		String yearmonthday = null;
		int serialNumber = 0;

		yearmonthday = DateUtil.getCurrentYyyymmdd();

		serialNumber = getSerialNumber(idType, yearmonthday);

		LOGGER.debug("serialNumber : {}", serialNumber);

		result = idShow.trim().toUpperCase()
//				+ String.valueOf(year).substring(2, 4)	// YEAR_SUBSTR_MAX
				+ fixTextSize(getHexString(serialNumber, nHex, length), '0', length);

		LOGGER.debug("result : {} ", result);

		return result;
	}

	@Override
	@Transactional(value="txManager", propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public synchronized String getId(
			final String idType,
			final String idShow,
			final int numLength) {

		String result = null;
		String year = null;
		String month = null;
		String yearmonth = null;
		int serialNumber = 0;

		year = DateUtil.getCurrentYear();
		month = DateUtil.getCurrentMonth();
		yearmonth = year.substring(2) + month;

		serialNumber = getSerialNumber(idType, yearmonth);
		result = idShow.trim().toUpperCase()
				+ yearmonth
				+ fixTextSize(String.valueOf(serialNumber), '0', numLength);

		return result;
	}

	/**
	 * Gets the id.
	 *
	 * @param idType the id type
	 * @return the id
	 */
	@Override
	public synchronized String getId(String idType)  {
		int serialNumber = 0;
		String year = null;
		String result = null;
		year = DateUtil.getCurrentYear();

		serialNumber = getSerialNumber(idType, year);
		result = idType.trim().toUpperCase()+year+fixTextSize(String.valueOf(serialNumber),'0',8);

		return result;
	}

	/**
	 * Gets the node id.
	 *
	 * @param outId the out id
	 * @return the node id
	 * @throws InterruptedException 
	 * @throws Exception the exception
	 */
	@Override
	public synchronized String getNodeId(final String outId) throws InterruptedException {
		String id = "";

		int i = 0;
		boolean bMake = true;
		Calendar cal = null;
		while (bMake) {
			i++;
			bMake = false;
			cal = Calendar.getInstance();

			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DATE);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int minute = cal.get(Calendar.MINUTE);
			int second = cal.get(Calendar.SECOND);
			int millisecond = cal.get(Calendar.MILLISECOND);

			id = "N"
					+ StringUtil.fixTextSize(Integer.toString(year % 100), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(month + 1), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(day), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(hour), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(minute), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(second), '0', 2)
					+ StringUtil.fixTextSize(Integer.toString(millisecond), '0', 3);

			if (i > 1000) {
				break;
			}

			if (outId != null && outId.equals(id)) {
				bMake = true;
				id.wait(1000);
			}
		}

		return id;
	}

	/**
	 * Gets the sch id.
	 *
	 * @return the node id
	 * @throws Exception the exception
	 */
	@Override
	public synchronized String getSchId() {
		String id = "";

		Calendar cal = Calendar.getInstance();

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		int millisecond = cal.get(Calendar.MILLISECOND);

		String strYear = Integer.toString(year);
		if (strYear.length() > 0) {
			strYear = strYear.substring(strYear.length() - 1);
		}

		id = "SC"
				+ strYear
				+ StringUtil.fixTextSize(Integer.toString(month + 1), '0', 2)
				+ StringUtil.fixTextSize(Integer.toString(day), '0', 2)
				+ StringUtil.fixTextSize(Integer.toString(hour), '0', 2)
				+ StringUtil.fixTextSize(Integer.toString(minute), '0', 2)
				+ StringUtil.fixTextSize(Integer.toString(second), '0', 2)
				+ StringUtil.fixTextSize(Integer.toString(millisecond), '0', 3);


		return id;
	}

	@Override
	public synchronized int getSerialNumber(String idType, String year) {
		int resultValue = -1;
		List<Integer> resultList;
		Map<String, Object> paramMap = null;

		paramMap = new HashMap<>();
		paramMap.put("idType", idType);
		paramMap.put("year", year);

		resultList = makingIdDAO.getSerialNumber(paramMap);

		if (resultList != null && !resultList.isEmpty()) {
			resultValue = resultList.get(0);
			resultValue++;
			updateSerialNumber(idType, year, resultValue);
		} else {
			resultValue = 1;
			deleteSerialNumber(idType.toUpperCase());
			insertSerialNumber(idType.toUpperCase(), year);
		}

		return resultValue;
	}

	@Override
	public synchronized int insertSerialNumber(String idType, String year) {
		int returnValue = -1;
		Map<String, Object> paramMap = null;
		paramMap = new HashMap<>();
		paramMap.put("idType", idType.toUpperCase());
		paramMap.put("year", year);
		returnValue = makingIdDAO.insertSerialNumber(paramMap);
		return returnValue;
	}

	@Override
	public synchronized int updateSerialNumber(String idType, String year, int idx) {
		int returnValue = -1;
		Map<String, Object> paramMap = null;
		paramMap = new HashMap<>();
		paramMap.put("idType", idType.toUpperCase());
		paramMap.put("year", year);
		paramMap.put("idx", idx);
		returnValue = makingIdDAO.updateSerialNumber(paramMap);
		return returnValue;
	}

	@Override
	public synchronized int deleteSerialNumber(String idType) {
		int returnValue = -1;
		returnValue = makingIdDAO.deleteSerialNumber(idType);
		return returnValue;
	}

	@Override
	public String fixTextSize(
			final String strText, final char cSpace, final int iTotalSize) {
		String result = null;

		if (strText != null) {
			if (strText.length() < iTotalSize) {
				/**
				 * 문자열의 크기가 특정크기보다 작을 때는 특정문자로 채움
				 */
				char[] carraySpace = new char[iTotalSize - strText.length()];
				Arrays.fill(carraySpace, cSpace);

				String strSpace = new String(carraySpace);

				result = strSpace + strText;

			} else {
				/**
				 * 문자열의 크기가 특정크기보다 클때는 앞쪽의 문자열 잘라냄
				 */
				result =  strText.substring(strText.length() - iTotalSize, strText.length());
			}
		}
		return result;
	}

	@Override
	public String getHexString(final int nValue, final int nHex, final int nMax) {
		if (nHex < 2 || nHex > 36) {	// 36 : NHEX_MAX
			return "0";
		}

		StringBuilder strResultSb = new StringBuilder();
		int nTemp;
		int nSize = 0;
		int nVal = nValue;

		while (nVal > 0) {
			nTemp = nVal % nHex;
			nVal /= nHex;

			if (nTemp < 10) {	// 10 : NTEMP_MAX
				strResultSb.insert(0, (char) ('0' + nTemp));
			} else {
				strResultSb.insert(0, (char) ('A' + nTemp - 10));	// 10 : NTEMP_MAX
			}

			++nSize;
		}
		while (nSize < nMax) {
			++nSize;
			strResultSb.insert(0, '0');
		}
		return strResultSb.toString();
	}

	/**
	 * Modify serial number or alphabat.
	 *
	 * @param conn the conn
	 * @param idType the id type
	 * @param iYear the i year
	 * @param idx the idx
	 */
	public synchronized String getSerialAlphabet(String idType, int iYear, String idx2, int alpLength){
		StringBuilder sb = new StringBuilder();
		boolean lastId = false;
		boolean changeLower = false;	// 소문자 변경 여부(대문자로 우선 만들고 대문자가 끝나면 소문자로 change 해서 생성)

		if(idx2 == null || "".equals(idx2)){
			sb.append("");

		} else {

			int rePosition = idx2.length() - 1;	// 알파벳을 변경할 위치
			int lowerCh = 0;
			int zYn1 = 0;
			int zYn2 = 0;
			for (int i=idx2.length()-1; i>=0; i--) {
				char ch = idx2.charAt(i);

				/*
				 *  알파벳 대문자 : 65 ~ 90 (A ~ Z)
				 *  알파벳 소문자 : 97 ~ 122 (a ~ z)
				 */
				// 대문자 Z 이거나 소문자 a ~ z 일 경우 다음 id는 소문자로 변경되어야 하므로 lowerCh 를 플러스 함.
				if (ch == 90 || ch >= 97 && ch <= 122) {
					lowerCh++;
				}

				// 바꿔야 할 위치를 잡아야 하는데 대문자 Z 일 경우 소문자로 시작해야 하므로 대문자 Z이면 zYn 을 플러스 함.
				if (ch == 90) {
					zYn1++;
				}
				if (ch == 122) {
					zYn2++;
				}
			}


			// idx2 문자가 다음 id를 소문자로 변경해야 하는 것으로 체크되었다면
			if (idx2.length() == lowerCh) {
				changeLower = true;
			}

			// idx2 문자가 모두 대문자 Z로 되어 있다면 다음 id를 a로 시작해야 하므로 rePosition 값을 -1로 세팅
			if (idx2.length() == zYn1) {
				rePosition = -1;
			}

			// idx2 문자가 모두 소문자 z 라면 마지막 id 였으므로 더이상 id를 생성할 수 없음.
			if (idx2.length() == zYn2) {
				lastId = true;
			}

			if (!lastId) {
				for (int i=idx2.length()-1; i>=0; i--) {
					char ch = idx2.charAt(i);

					/*
					 *  알파벳 대문자 : 65 ~ 90 (a ~ z)
					 *  알파벳 소문자 : 97 ~ 122 (A ~ Z)
					 */
					// z와 Z는 바뀌어야 할 문자가 없으므로 빼고 체크
					if (ch >= 65 && ch < 90 || ch >= 97 && ch < 122) {
						rePosition = i;
						break;
					}
				}

				for (int i=0; i<idx2.length(); i++) {
					char ch = idx2.charAt(i);

					// replace 할 자리수이면
					if (i == rePosition) {
						char newCh = (char) (ch+1);
						if (newCh >= 65 && newCh <=90 || newCh >=97 && newCh <=122) {
							// 새로운 문자가 알파벳일 경우만 세팅
							sb.append(newCh); // char +1 을 해준다.
						}
					} else {
						if (i > rePosition) {
							// 앞자리의 문자가 replace 되었다면 뒤의 문자는 모두 a 로 세팅해 둔다.(뒤 문자는 초기로 시작)
							sb.append("a");

						} else {
							// replace 할 문자의 자리수가 아니라면 기존 문자열을 붙인다.
							sb.append(ch);
						}
					}
				}
			}
		}

		// idx2 값이 없을 경우(알파벳이 처음 시작일 때)
		if (sb.toString() == null || "".equals(sb.toString())) {
			// 모두 a로 세팅
			for (int i=0; i<alpLength; i++) {
				sb.append("a");
			}
		}

		if (changeLower) {
			sb.append(sb.toString().toLowerCase());
		} else {
			sb.append(sb.toString().toUpperCase());
		}

		return sb.toString();
	}

	/**
	 * Gets the sch id.
	 *
	 * @return the node id
	 * 
	 */
	@Override
	public String getSeqId(String prefix, String seqName, int len) {
		synchronized(this) {			
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("prefix", prefix);
			paramMap.put("seqName", seqName);
			paramMap.put("length", len);
			return makingIdDAO.getSeqId(paramMap);
		}
	}
}

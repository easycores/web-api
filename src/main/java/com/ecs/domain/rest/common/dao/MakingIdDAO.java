package com.ecs.domain.rest.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

/**
 *
 * @author h2y
 *
 */

//@MapperScan("makingIdDAO")
@Mapper
public interface MakingIdDAO {

	/**
	 *
	 * @param idType
	 * @param year
	 * @return int
	 */
	List<Integer> getSerialNumber(Map<String, Object> paramMap);

	/**
	 *
	 * @param idType
	 * @param year
	 * @return int
	 */
	int insertSerialNumber(Map<String, Object> paramMap);

	/**
	 *
	 * @param idType
	 * @param year
	 * @param idx
	 * @return int
	 */
	int updateSerialNumber(Map<String, Object> paramMap);

	/**
	 *
	 * @param idType
	 * @return int
	 */
	int deleteSerialNumber(String idType);

	/**
	 * @param paramMap
	 * @return
	 * 
	 */
	String getSeqId(Map<String, Object> paramMap) ;
}

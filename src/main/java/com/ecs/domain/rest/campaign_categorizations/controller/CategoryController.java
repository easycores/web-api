package com.ecs.domain.rest.campaign_categorizations.controller;

import com.ecs.domain.common.login.model.UserVO;
import com.ecs.domain.rest.campaign_categorizations.model.CategoryPostRequest;
import com.ecs.domain.rest.campaign_categorizations.model.CategoryVO;
import com.ecs.domain.rest.campaign_categorizations.service.CategoryService;
import com.ecs.global.constant.Const;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import com.ecs.domain.rest.offer.model.CampOfferVO;
import com.ecs.global.util.CmmnUtil;
import com.ecs.infrastructure.model.EcsResponse;
import com.ecs.infrastructure.model.ResultMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;


	/**
	 * 캠페인분류 : 처음화면 JSP이동
	 * @param model
	 * @return
	 * @
	 */
	@GetMapping(value="/campaign_categorizations")
	public EcsResponse<CategoryVO> SelectCategoryInfo() {

		List<CategoryVO> saveLocation = categoryService.sqlGetCategoryList();

		return new EcsResponse(saveLocation, HttpStatus.OK);
	}


	/**
	 * 캠페인분류 : 저장(신규등록)
	 * @param model
	 * @param dirId
	 * @param dirName
	 * @param parentId
	 * @param imgPath
	 * @return
	 */
	@PostMapping(value="/campaign_categorizations")
	public EcsResponse<CategoryVO> insertCategoryInfo(@RequestBody @Valid CategoryPostRequest.InsertCategoryInfoDTO insertCategoryInfoDTO) throws Exception {
		Long newDirId;
		System.out.println("=============================getDirId=============================");
		System.out.println(insertCategoryInfoDTO.getDirId());


		if (insertCategoryInfoDTO.getDirId() == null || insertCategoryInfoDTO.getDirId().equals("")){
			//신규ID값 : 등록되어있는 MAX(DIR_ID)+1값
			newDirId = categoryService.getCategoryId();
			System.out.println("=============================newDirId=============================");
			System.out.println(newDirId);

			if(newDirId != 0 && newDirId > 0){
				insertCategoryInfoDTO.setDirId(newDirId);

				Map<String, Object> insertCategoryInfoMap = CmmnUtil.convertToMap(insertCategoryInfoDTO);
				int result = categoryService.insertCategoryInfo(insertCategoryInfoMap); //그대로 인서트 하면됨
				System.out.println(result);
			}
		}

		//화면 새로고침용
		List<CategoryVO> saveLocation = categoryService.sqlGetCategoryList();

		return new EcsResponse(saveLocation, HttpStatus.OK);
	}
}

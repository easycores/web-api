package com.ecs.domain.rest.campaign_categorizations.service;

import com.ecs.domain.rest.campaign_categorizations.model.CategoryVO;
import com.ecs.infrastructure.model.ResultMap;

import java.util.List;
import java.util.Map;

/**
 * @author P068995
 *
 */
public interface CategoryService {
	/**
	 * 캠페인분류관리 : 카테고리리스트.
	 * @param dirCd
	 * @return List<CategoryVO>
	 */
	public List<CategoryVO> sqlGetCategoryList();


	/**
	 * 캠페인분류관리 : 캠페인분류 신규ID
	 * @return
	 */
	public Long getCategoryId();

	/**
	 * 캠페인분류관리 : 등록
	 * @param dirId
	 * @param dirName
	 * @param parentId
	 * @param imgPath
	 * @return
	 */
	public int insertCategoryInfo(Map<String, Object> paramMap);

}

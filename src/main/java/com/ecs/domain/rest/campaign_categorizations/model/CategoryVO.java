package com.ecs.domain.rest.campaign_categorizations.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.websocket.server.ServerEndpoint;

/**
 * @author P068995
 *
 */

@Getter
@Setter
public class CategoryVO {

	private long dirId;

	private String dirCd;

	private String dirName;

	private long parentId;

	private String savePath;

	private long lvl;

	private long sort_ord;
}

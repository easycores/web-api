package com.ecs.domain.rest.campaign_categorizations.service.impl;

import com.ecs.domain.rest.campaign_categorizations.dao.CategoryDAO;
import com.ecs.domain.rest.campaign_categorizations.model.CategoryVO;
import com.ecs.domain.rest.campaign_categorizations.service.CategoryService;
import com.ecs.infrastructure.model.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDAO categoryDAO;

	/**
	 * 캠페인분류관리 : 카테고리리스트.
	 *
	 * @param dirCd
	 * @return List<CategoryVO>
	 */
	public List<CategoryVO> sqlGetCategoryList() {
		System.out.println("HEELo");
		return categoryDAO.sqlGetCategoryList();
	}

	/**
	 * 캠페인분류관리 : 캠페인분류 신규ID
	 * @return
	 */
	public Long getCategoryId() {
		System.out.println("getCategoryId Service Impl 실행");
		return categoryDAO.getCategoryId();
	}

	public int insertCategoryInfo(Map<String, Object> paramMap) {
		System.out.println("serviceeeeeeeadfnj adsk");

		return categoryDAO.insertCategoryInfo(paramMap);
	}
	/**
	 * 캠페인분류관리 : 수정
	 * @param dirId
	 * @param dirName
	 * @param parentId
	 * @param imgPath
	 * @param empId
	 * @return
	 */




}

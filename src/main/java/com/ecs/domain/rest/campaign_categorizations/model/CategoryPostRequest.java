package com.ecs.domain.rest.campaign_categorizations.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

public class CategoryPostRequest {

    @Data
    @Getter
    @Setter
    public static class InsertCategoryInfoDTO{
        @NotNull
        private String dirCd;

        private Long dirId;
        @NotNull
        private String dirName;
        @NotNull
        private Long parentId;

    }
}

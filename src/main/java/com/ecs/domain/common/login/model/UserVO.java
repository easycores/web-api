package com.ecs.domain.common.login.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.ecs.infrastructure.model.ResultMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;

public class UserVO implements UserDetails {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 사용자아이디.
	 */
	private String empId;
	/**
	 * 사용자명.
	 */
	private String empName;
	/**
	 * 비밀번호.
	 */
	private String empPw;
	/**
	 * 부서그룹
	 */
	private String deptGpCd;
	/**
	 * 부서
	 */
	private String deptId;
	/**
	 * 부서명.
	 */
	private String deptName;
	/**
	 * 메뉴권한ID.
	 */
	private int mathId;
	/**
	 * 사용자타입아이디.
	 */
	private String typeId;
	/**
	 * 전화번호
	 */
	private String empTel;
	/**
	 * 이메일
	 */
	private String empEmail;
	/**
	 * 핸드폰번호.
	 */
	private String empMobile;
	/**
	 * IP
	 */
	private String ipAddr;
	/**
	 * MAC 
	 */
	private String macAddr;
	/**
	 * 직접접속 여부
	 */
	private String drctInstYn;
	/**
	 * 최종비밀번호수정일.
	 */
	private String pwModDate;
	/**
	 * 이전접속일시.
	 */
	private String bfrLogDttm;
	/**
	 * 삭제여부.
	 */
	private String delF;
	/**
	 * 비밀번호 reset 여부
	 */
	private String pwResetYn = "Y";
	
	
	private long userId;
	/**
	 * 사용자명
	 */
	private String username;
	/**
	 * 비밀번호
	 */
	private String password;
	/**
	 * 사용자 설정 언어
	 */
	private String lang;
	/**
	 *
	 */
	private List<Role> authorities;
	/**
	 *
	 */
	private boolean accountNonExpired = true;
	/**
	 *
	 */
	private boolean accountNonLocked = true;
	/**
	 *
	 */
	private boolean credentialsNonExpired = true;
	/**
	 *
	 */
	private boolean enabled = true;
	
	private List<ResultMap> authDeptList;
	
	private List<UserAuthVO> authList;

	// 캠페인 전체 조회 권한
	private boolean isAllCampReadable;

	//매장사용자여부
	private boolean isStoreUser;
	
	//탬플릿관리자
	private boolean isTemplateAdmin;
	
	//대시보드관리자
	private boolean isDashbordAdmin;
	
	//컨텐츠문구승인권한
	private boolean isContAuthAdmin;
	
	private int pwErrCnt;
	private String pwErrYn = "Y";
	private String pwInitYn = "N";
	
	private String syncYn;
	
	@Value("#{application['application.system.isPasswordEncrypt']}")
	private boolean passwordEncrypt;
	
	
	// 업태코드
	private String biztpCd;
	
	// 업태명
	private String biztpNm;
	
	// 점포
	private String strCd;
		
	// 점포명
	private String strNm;
	
	// 본사/점포 구분
	private String hqStrDivCd;
			

	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpPw() {
		return empPw;
	}
	public void setEmpPw(String empPw) {
		this.empPw = empPw;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public int getMathId() {
		return mathId;
	}
	public void setMathId(int mathId) {
		this.mathId = mathId;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getEmpTel() {
		return empTel;
	}
	public void setEmpTel(String empTel) {
		this.empTel = empTel;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	public String getEmpMobile() {
		return empMobile;
	}
	public void setEmpMobile(String empMobile) {
		this.empMobile = empMobile;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getMacAddr() {
		return macAddr;
	}
	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}
	public String getDrctInstYn() {
		return drctInstYn;
	}
	public void setDrctInstYn(String drctInstYn) {
		this.drctInstYn = drctInstYn;
	}
	public String getPwModDate() {
		return pwModDate;
	}
	public void setPwModDate(String pwModDate) {
		this.pwModDate = pwModDate;
	}
	public String getBfrLogDttm() {
		return bfrLogDttm;
	}
	public void setBfrLogDttm(String bfrLogDttm) {
		this.bfrLogDttm = bfrLogDttm;
	}
	public String getDelF() {
		return delF;
	}
	public void setDelF(String delF) {
		this.delF = delF;
	}
	public String getPwResetYn() {
		return pwResetYn;
	}
	public void setPwResetYn(String pwResetYn) {
		this.pwResetYn = pwResetYn;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public List<Role> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<Role> authorities) {
		this.authorities = authorities;
	}
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}
	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean getIsAllCampReadable() {
		return isAllCampReadable;
	}
	public void setIsAllCampReadable(boolean isAllCampReadable) {
		this.isAllCampReadable = isAllCampReadable;
	}
	public boolean isStoreUser() {
		return isStoreUser;
	}
	public void setStoreUser(boolean isStoreUser) {
		this.isStoreUser = isStoreUser;
	}
	public boolean isTemplateAdmin() {
		return isTemplateAdmin;
	}
	public void setTemplateAdmin(boolean isTemplateAdmin) {
		this.isTemplateAdmin = isTemplateAdmin;
	}
	public boolean isDashbordAdmin() {
		return isDashbordAdmin;
	}
	public void setDashbordAdmin(boolean isDashbordAdmin) {
		this.isDashbordAdmin = isDashbordAdmin;
	}
	public boolean isContAuthAdmin() {
		return isContAuthAdmin;
	}
	public void setContAuthAdmin(boolean isContAuthAdmin) {
		this.isContAuthAdmin = isContAuthAdmin;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDeptGpCd() {
		return deptGpCd;
	}
	public void setDeptGpCd(String deptGpCd) {
		setStoreUser("ST".equals(deptGpCd));
		this.deptGpCd = deptGpCd;
	}

	public List<ResultMap> getAuthDeptList() {
		List<ResultMap> authDeptListCopy = new ArrayList<>();
		authDeptListCopy.addAll(authDeptList);
		return authDeptListCopy;
	}
	public void setAuthDeptList(List<ResultMap> authDeptList) {
		this.authDeptList = new ArrayList<>();
		this.authDeptList.addAll(authDeptList);
	}
	
	public List<UserAuthVO> getAuthList() {
		List<UserAuthVO> authListCopy = new ArrayList<>();
		authListCopy.addAll(authList);
		return authListCopy;
	}
	//45 private 배열에 public 데이터 할당
	public void setAuthList(List<UserAuthVO> authList) {
		this.authList= new ArrayList<>();
		if (authList != null) this.authList.addAll(authList);

		if(null == authList || authList.isEmpty()) {
			setDashbordAdmin(false);	//대시보드관리권한
			setTemplateAdmin(false);	//탬플릿관리권한
			setContAuthAdmin(false);
		} else {
			for(UserAuthVO auth : authList) {				
				if("Y".equals(auth.getAllCampSelectAuthYn())) {
					setIsAllCampReadable(true);
				}
				if("Y".equals(auth.getDashAuthYn())){
					setDashbordAdmin(true);
				}
				if("Y".equals(auth.getTmplAuthYn())){
					setTemplateAdmin(true);
				}
				//컨텐츠문구승인권한
				if("Y".equals(auth.getContAuthYn())){
					setTemplateAdmin(true);
				}
			}
		}

	}
	
	public String getBiztpCd() {
		return biztpCd;
	}
	public void setBiztpCd(String biztpCd) {
		this.biztpCd = biztpCd;
	}
	public String getBiztpNm() {
		return biztpNm;
	}
	public void setBiztpNm(String biztpNm) {
		this.biztpNm = biztpNm;
	}
	public String getStrCd() {
		return strCd;
	}
	public void setStrCd(String strCd) {
		this.strCd = strCd;
	}
	public String getStrNm() {
		return strNm;
	}
	public void setStrNm(String strNm) {
		this.strNm = strNm;
	}
	
	public int getPwErrCnt() {
		return pwErrCnt;
	}
	public void setPwErrCnt(int pwErrCnt) {
		this.pwErrCnt = pwErrCnt;
	}
	public String getPwErrYn() {
		return pwErrYn;
	}
	public void setPwErrYn(String pwErrYn) {
		this.pwErrYn = pwErrYn;
	}
	public String getPwInitYn() {
		return pwInitYn;
	}
	public void setPwInitYn(String pwInitYn) {
		this.pwInitYn = pwInitYn;
	}

	public boolean isPasswordEncrypt() {
		return passwordEncrypt 
				|| this.empPw.length() >= 24;
	}
	
	public boolean isPasswordEmartEncrypt() {
		return passwordEncrypt 
				|| this.empPw.length() >= 100;
	}
	
	public void setPasswordEncrypt(boolean passwordEncrypt) {
		this.passwordEncrypt = passwordEncrypt;
	}
	public String getHqStrDivCd() {
		return hqStrDivCd;
	}
	public void setHqStrDivCd(String hqStrDivCd) {
		this.hqStrDivCd = hqStrDivCd;
	}
	public String getSyncYn() {
		return syncYn;
	}
	public void setSyncYn(String syncYn) {
		this.syncYn = syncYn;
	}
	
	public boolean isPasswordExpired() {
		LocalDate now = LocalDate.now();
		LocalDate modDate = LocalDate.of(
				Integer.parseInt(pwModDate.substring(0, 4)), 
				Integer.parseInt(pwModDate.substring(4, 6)), 
				Integer.parseInt(pwModDate.substring(6, 8)));
		
		return now.minusDays(90).isAfter(modDate);
	}
	

}

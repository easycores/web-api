package com.ecs.domain.common.login.model;

import java.io.Serializable;

public class UserAuthVO implements Serializable {

	private static final long serialVersionUID = 2562698874517792090L;

	private String empId;
	private int seq;
	private int mathId;
	private String majorMathYn;
	private String delF;
	
	
//	private String adminAuthYn;		//관리자권한여부
	private String allCampSelectAuthYn;		//전캠페인조회권한
	
	private String sysAuthYn;		//시스템권한여부
	private String dataAuthYn;		//데이터권한여부
	
	private String menuAuthYn;		//메뉴권한여부
	private String dashAuthYn;		//대시보드권한여부
	private String tmplAuthYn;		//탬플릿권한여부
	
	private String contAuthYn;		//컨텐츠문구승인권한
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public int getMathId() {
		return mathId;
	}
	public void setMathId(int mathId) {
		this.mathId = mathId;
	}
	public String getMajorMathYn() {
		return majorMathYn;
	}
	public void setMajorMathYn(String majorMathYn) {
		this.majorMathYn = majorMathYn;
	}
	public String getDelF() {
		return delF;
	}
	public void setDelF(String delF) {
		this.delF = delF;
	}
	
	public String getSysAuthYn() {
		return sysAuthYn;
	}
	public String getAllCampSelectAuthYn() {
		return allCampSelectAuthYn;
	}
	public void setAllCampSelectAuthYn(String allCampSelectAuthYn) {
		this.allCampSelectAuthYn = allCampSelectAuthYn;
	}
	public void setSysAuthYn(String sysAuthYn) {
		this.sysAuthYn = sysAuthYn;
	}
	public String getDataAuthYn() {
		return dataAuthYn;
	}
	public void setDataAuthYn(String dataAuthYn) {
		this.dataAuthYn = dataAuthYn;
	}
	public String getMenuAuthYn() {
		return menuAuthYn;
	}
	public void setMenuAuthYn(String menuAuthYn) {
		this.menuAuthYn = menuAuthYn;
	}
	public String getDashAuthYn() {
		return dashAuthYn;
	}
	public void setDashAuthYn(String dashAuthYn) {
		this.dashAuthYn = dashAuthYn;
	}
	public String getTmplAuthYn() {
		return tmplAuthYn;
	}
	public void setTmplAuthYn(String tmplAuthYn) {
		this.tmplAuthYn = tmplAuthYn;
	}
	@Override
	public String toString() {
		return "UserAuthVO [empId=" + empId + ", seq=" + seq + ", mathId=" + mathId + ", majorMathYn=" + majorMathYn
				+ ", delF=" + delF + ", sysAuthYn=" + sysAuthYn + ", dataAuthYn="
				+ dataAuthYn + ", menuAuthYn=" + menuAuthYn + ", dashAuthYn=" + dashAuthYn + ", tmplAuthYn="
				+ tmplAuthYn + ", allCampSelectAuthYn=" + allCampSelectAuthYn + ", contAuthYn=" + contAuthYn + "]";
	}
	public String getContAuthYn() {
		return contAuthYn;
	}
	public void setContAuthYn(String contAuthYn) {
		this.contAuthYn = contAuthYn;
	}


}

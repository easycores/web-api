package com.ecs.domain.common.login.model;

import java.io.Serializable;

/**
 * 
 * @author h2y
 *
 */
public class Privilege implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9202914314306964595L;
	/**
	 * 
	 */
	private String name;

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param value the name to set
	 */
	public final void setName(final String value) {
		this.name = value;
	}
}

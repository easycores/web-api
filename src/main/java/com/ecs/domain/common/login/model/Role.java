package com.ecs.domain.common.login.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
/**
 * 
 * @author h2y
 *
 */
public class Role implements GrantedAuthority, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4541907208632182466L;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private List<Privilege> privileges;

	@Override
	public final String getAuthority() {
		return this.name;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param value the name to set
	 */
	public final void setName(final String value) {
		this.name = value;
	}

	/**
	 * @return the privileges
	 */
	public final List<Privilege> getPrivileges() {
		List<Privilege> privilegescopy = new ArrayList<>();
		if(this.privileges!=null) {
			privilegescopy= this.privileges;
		}
		return privilegescopy;
	}

	/**
	 * @param list the privileges to set
	 */
	public final void setPrivileges(final List<Privilege> list) {
		this.privileges = new ArrayList<>();
		this.privileges.addAll(list);
	}

}

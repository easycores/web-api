package com.ecs.domain;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

// API 를 테스트하기 위한 임시 화면
@Controller
public class HomeController {

    @GetMapping(value = {"/",""})
    public String home() {
        return "index.html";
    }
}
